#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:           MASAS Tools Portal v0.2 Setup
Author:         Jacob Westfall
Copyright:      Copyright (c) 2011-2014 MASAS Contributors.  Published under the
                Clear BSD license.  See License.txt for the full text of the license.
Created:        Nov 01, 2011
Updated:        Feb 06, 2014
Description:    Setup for the tools portal server.
"""

#NOTE: if a built installation is ever needed, ie setup.py bdist, then it will
#      use the ./build directory to do this and will need to rename the
#      current one to something that doesn't conflict

from distutils.core import setup

setup(name="portal",
    version="0.222",
    description="MASAS-X Portal",
    author="Jacob Westfall",
    license="install/Licence.txt",
    url="http://www.masas-x.ca",
    py_modules=["portal", "status", "report"],
    packages = ["libs", "utils", "utils/ua_parser"],
)

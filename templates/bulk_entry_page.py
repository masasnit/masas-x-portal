#!/usr/bin/env python




##################################################
## DEPENDENCIES
import sys
import os
import os.path
try:
    import builtins as builtin
except ImportError:
    import __builtin__ as builtin
from os.path import getmtime, exists
import time
import types
from Cheetah.Version import MinCompatibleVersion as RequiredCheetahVersion
from Cheetah.Version import MinCompatibleVersionTuple as RequiredCheetahVersionTuple
from Cheetah.Template import Template
from Cheetah.DummyTransaction import *
from Cheetah.NameMapper import NotFound, valueForName, valueFromSearchList, valueFromFrameOrSearchList
from Cheetah.CacheRegion import CacheRegion
import Cheetah.Filters as Filters
import Cheetah.ErrorCatchers as ErrorCatchers

##################################################
## MODULE CONSTANTS
VFFSL=valueFromFrameOrSearchList
VFSL=valueFromSearchList
VFN=valueForName
currentTime=time.time
__CHEETAH_version__ = '2.4.4'
__CHEETAH_versionTuple__ = (2, 4, 4, 'development', 0)
__CHEETAH_genTime__ = 1391711257.8355761
__CHEETAH_genTimestamp__ = 'Thu Feb  6 13:27:37 2014'
__CHEETAH_src__ = 'bulk_entry_page.tmpl'
__CHEETAH_srcLastModified__ = 'Thu Feb  6 13:23:51 2014'
__CHEETAH_docstring__ = 'Autogenerated by Cheetah: The Python-Powered Template Engine'

if __CHEETAH_versionTuple__ < RequiredCheetahVersionTuple:
    raise AssertionError(
      'This template was compiled with Cheetah version'
      ' %s. Templates compiled before version %s must be recompiled.'%(
         __CHEETAH_version__, RequiredCheetahVersion))

##################################################
## CLASSES

class bulk_entry_page(Template):

    ##################################################
    ## CHEETAH GENERATED METHODS


    def __init__(self, *args, **KWs):

        super(bulk_entry_page, self).__init__(*args, **KWs)
        if not self._CHEETAH__instanceInitialized:
            cheetahKWArgs = {}
            allowedKWs = 'searchList namespaces filter filtersLib errorCatcher'.split()
            for k,v in KWs.items():
                if k in allowedKWs: cheetahKWArgs[k] = v
            self._initCheetahInstance(**cheetahKWArgs)
        

    def respond(self, trans=None):



        ## CHEETAH: main method generated for this template
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        #  MASAS Portal Cheetah Template - Bulk Entry Page
        #  Last modified Jan 13, 2014
        write(u'''<!DOCTYPE html>
<html>
<head>
  <title>MASAS Bulk Entry Tool</title>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">

''')
        # TODO: use debug CSS versions too?
        if VFFSL(SL,"current_mode",True) == "Operational": # generated from line 10, col 1
            if self.varExists('use_source'): # generated from line 11, col 5
                write(u'''  <link href="/src/portal/themes/css/ext-all-notheme.css" rel="stylesheet" type="text/css"></link>
  <link href="/src/portal/themes/css/xtheme-redWine.css" rel="stylesheet" type="text/css"></link>
''')
            else: # generated from line 14, col 5
                write(u'''  <link href="''')
                _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 15, col 15
                if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 15, col 15.
                write(u'''/libs/ExtJS-3.4.2/css/ext-all-notheme-min.css" rel="stylesheet" type="text/css"></link>
  <link href="''')
                _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 16, col 15
                if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 16, col 15.
                write(u'''/libs/ExtJS-3.4.2/css/ext-operational-min.css" rel="stylesheet" type="text/css"></link>
''')
        elif VFFSL(SL,"current_mode",True) == "Exercise": # generated from line 18, col 1
            if self.varExists('use_source'): # generated from line 19, col 5
                write(u'''  <link href="/src/portal/themes/css/ext-all-notheme.css" rel="stylesheet" type="text/css"></link>
  <link href="/src/portal/themes/css/xtheme-vodkaO.css" rel="stylesheet" type="text/css"></link>
''')
            else: # generated from line 22, col 5
                write(u'''  <link href="''')
                _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 23, col 15
                if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 23, col 15.
                write(u'''/libs/ExtJS-3.4.2/css/ext-all-notheme-min.css" rel="stylesheet" type="text/css"></link>
  <link href="''')
                _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 24, col 15
                if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 24, col 15.
                write(u'''/libs/ExtJS-3.4.2/css/ext-exercise-min.css" rel="stylesheet" type="text/css"></link>
''')
        else: # generated from line 26, col 1
            write(u'''  <link href="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 27, col 15
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 27, col 15.
            write(u'''/libs/ExtJS-3.4.2/css/ext-all-min.css" type="text/css" rel="stylesheet">
''')
        write(u'''  <link href="''')
        _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 29, col 15
        if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 29, col 15.
        write(u'''/libs/ExtJS-3.4.2/css/post-plugins-min.css" type="text/css" rel="stylesheet">
  <link href="''')
        _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 30, col 15
        if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 30, col 15.
        write(u'''/libs/OpenLayers-2.13.1/theme/default/style.css" type="text/css" rel="stylesheet">
  <link href="''')
        _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 31, col 15
        if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 31, col 15.
        write(u'''/libs/GeoExt-1.2rc/css/geoext-all-min.css" type="text/css" rel="stylesheet">
''')
        if self.varExists('use_source'): # generated from line 32, col 1
            write(u'''  <link href="/src/post/css/post.css" type="text/css" rel="stylesheet">
''')
        else: # generated from line 34, col 1
            write(u'''  <link href="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 35, col 15
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 35, col 15.
            write(u'''/css/post-min.css" type="text/css" rel="stylesheet">
''')
        write(u'''
  <script type="text/javascript">
// error handling for javascript, setup first
var ERR = {};
''')
        if self.varExists('use_debug_tools'): # generated from line 41, col 1
            write(u'''ERR.DEBUG = true;
''')
        else: # generated from line 43, col 1
            write(u'''ERR.DEBUG = false;
''')
        write(u"""ERR.REPORT_URL = 'ajax/error_report';
  </script>
""")
        if self.varExists('use_source'): # generated from line 48, col 1
            write(u'''  <script src="/src/portal/js/err-console.js" type="text/javascript"></script>
''')
        else: # generated from line 50, col 1
            if self.varExists('use_debug_tools'): # generated from line 51, col 5
                write(u'''  <script src="''')
                _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 52, col 16
                if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 52, col 16.
                write(u'''/js/err-console-debug.js" type="text/javascript"></script>
''')
            else: # generated from line 53, col 5
                write(u'''  <script src="''')
                _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 54, col 16
                if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 54, col 16.
                write(u'''/js/err-console-min.js" type="text/javascript"></script>
''')
        write(u'''
''')
        if self.varExists('use_debug_all'): # generated from line 58, col 1
            write(u'''  <script src="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 59, col 16
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 59, col 16.
            write(u'''/libs/ExtJS-3.4.2/ext-base-debug.js" type="text/javascript"></script>
  <script src="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 60, col 16
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 60, col 16.
            write(u'''/libs/ExtJS-3.4.2/ext-all-debug.js" type="text/javascript"></script>
  <script src="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 61, col 16
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 61, col 16.
            write(u'''/libs/ExtJS-3.4.2/post-plugins-debug.js" type="text/javascript"></script>
  <script src="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 62, col 16
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 62, col 16.
            write(u'''/libs/OpenLayers-2.13.1/OpenLayers-debug.js" type="text/javascript"></script>
  <script src="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 63, col 16
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 63, col 16.
            write(u'''/libs/GeoExt-1.2rc/GeoExt-debug.js" type="text/javascript"></script>
  <script src="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 64, col 16
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 64, col 16.
            write(u'''/libs/xml-json-debug.js" type="text/javascript"></script>
''')
        else: # generated from line 65, col 1
            write(u'''  <script src="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 66, col 16
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 66, col 16.
            write(u'''/libs/ExtJS-3.4.2/ext-base.js" type="text/javascript"></script>
  <script src="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 67, col 16
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 67, col 16.
            write(u'''/libs/ExtJS-3.4.2/ext-all.js" type="text/javascript"></script>
  <script src="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 68, col 16
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 68, col 16.
            write(u'''/libs/ExtJS-3.4.2/post-plugins-min.js" type="text/javascript"></script>
  <script src="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 69, col 16
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 69, col 16.
            write(u'''/libs/OpenLayers-2.13.1/OpenLayers.js" type="text/javascript"></script>
  <script src="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 70, col 16
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 70, col 16.
            write(u'''/libs/GeoExt-1.2rc/GeoExt-min.js" type="text/javascript"></script>
  <script src="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 71, col 16
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 71, col 16.
            write(u'''/libs/xml-json-min.js" type="text/javascript"></script>
''')
        write(u'''
  <script src="''')
        _v = VFFSL(SL,"google_map_url",True) # u'$google_map_url' on line 74, col 16
        if _v is not None: write(_filter(_v, rawExpr=u'$google_map_url')) # from line 74, col 16.
        write(u'''" type="text/javascript"></script>

  <script type="text/javascript">

// Path to the blank image should point to a valid location on your server
Ext.BLANK_IMAGE_URL = \'''')
        _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 79, col 24
        if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 79, col 24.
        write(u"""/libs/ExtJS-3.4.2/images/default/s.gif';
Ext.SSL_SECURE_URL = '""")
        _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 80, col 23
        if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 80, col 23.
        write(u"""/libs/ExtJS-3.4.2/images/default/s.gif';
// Path for OpenLayers images
OpenLayers.ImgPath = '""")
        _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 82, col 23
        if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 82, col 23.
        write(u"""/libs/OpenLayers-2.13.1/img/';
// Customize the MASAS Icon location
OpenLayers.Format.MASASFeed.IconURL = '""")
        _v = VFFSL(SL,"icon_service_url",True) # u'$icon_service_url' on line 84, col 40
        if _v is not None: write(_filter(_v, rawExpr=u'$icon_service_url')) # from line 84, col 40.
        write(u"""';
// Adding retries because default is 0
OpenLayers.IMAGE_RELOAD_ATTEMPTS = 3;
// use the browser's console for logging from OpenLayers
OpenLayers.Console = console;

// global object for shared values
Ext.namespace('MASAS');
// global object for this app
Ext.namespace('POST');

// posting values
POST.USER_URI = '""")
        _v = VFFSL(SL,"masas_user_uri",True) # u'$masas_user_uri' on line 96, col 18
        if _v is not None: write(_filter(_v, rawExpr=u'$masas_user_uri')) # from line 96, col 18.
        write(u"""';
POST.FEED_URL = '""")
        _v = VFFSL(SL,"masas_hub_url",True) # u'$masas_hub_url' on line 97, col 18
        if _v is not None: write(_filter(_v, rawExpr=u'$masas_hub_url')) # from line 97, col 18.
        write(u"""';
POST.USER_SECRET = '""")
        _v = VFFSL(SL,"masas_user_secret",True) # u'$masas_user_secret' on line 98, col 21
        if _v is not None: write(_filter(_v, rawExpr=u'$masas_user_secret')) # from line 98, col 21.
        write(u"""';
""")
        if self.varExists('feed_list'): # generated from line 99, col 1
            write(u'''POST.FEED_SETTINGS = ''')
            _v = VFFSL(SL,"feed_list",True) # u'$feed_list' on line 100, col 22
            if _v is not None: write(_filter(_v, rawExpr=u'$feed_list')) # from line 100, col 22.
            write(u''';
''')
        write(u"""POST.AJAX_PROXY_URL = 'ajax/go?url=';
POST.SETUP_ATTACH_URL = 'ajax/setup_attach';
POST.IMPORT_ATTACH_URL = 'ajax/import_attach';
POST.ATTACH_PROXY_URL = 'ajax/post_attach?url=';

""")
        if self.varExists('use_touch'): # generated from line 107, col 1
            write(u'''POST.TOUCH_ENABLE = true;
''')
        write(u"""
// header box values
Ext.namespace('MASAS.Portal');
MASAS.Portal.SITE_NAME = '""")
        _v = VFFSL(SL,"site_name",True) # u'$site_name' on line 113, col 27
        if _v is not None: write(_filter(_v, rawExpr=u'$site_name')) # from line 113, col 27.
        write(u"""';
MASAS.Portal.CURRENT_MODE = '""")
        _v = VFFSL(SL,"current_mode",True) # u'$current_mode' on line 114, col 30
        if _v is not None: write(_filter(_v, rawExpr=u'$current_mode')) # from line 114, col 30.
        write(u"""';
MASAS.Portal.ACCOUNT_NAME = '""")
        _v = VFFSL(SL,"account_name",True) # u'$account_name' on line 115, col 30
        if _v is not None: write(_filter(_v, rawExpr=u'$account_name')) # from line 115, col 30.
        write(u'''\';

  </script>
</head>
<body>
  <div id="north" class="x-hide-display">
    <p>MASAS Bulk Entry Tool - if you can see this, your web browser
does not support this tool.</p>
  </div>

''')
        if self.varExists('use_source'): # generated from line 125, col 1
            write(u'''  <script src="/src/post/js/src/common.js" type="text/javascript"></script>
  <script src="/src/post/js/postMap.js" type="text/javascript"></script>
  <script src="/src/post/js/bulk-entry.js" type="text/javascript"></script>
''')
        elif self.varExists('use_debug_tools'): # generated from line 129, col 1
            write(u'''  <script src="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 130, col 16
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 130, col 16.
            write(u'''/js/bulk-entry-debug.js" type="text/javascript"></script>
''')
        else: # generated from line 131, col 1
            write(u'''  <script src="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 132, col 16
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 132, col 16.
            write(u'''/js/bulk-entry-min.js" type="text/javascript"></script>
''')
        write(u'''
</body>
</html>''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        
    ##################################################
    ## CHEETAH GENERATED ATTRIBUTES


    _CHEETAH__instanceInitialized = False

    _CHEETAH_version = __CHEETAH_version__

    _CHEETAH_versionTuple = __CHEETAH_versionTuple__

    _CHEETAH_genTime = __CHEETAH_genTime__

    _CHEETAH_genTimestamp = __CHEETAH_genTimestamp__

    _CHEETAH_src = __CHEETAH_src__

    _CHEETAH_srcLastModified = __CHEETAH_srcLastModified__

    _mainCheetahMethod_for_bulk_entry_page= 'respond'

## END CLASS DEFINITION

if not hasattr(bulk_entry_page, '_initCheetahAttributes'):
    templateAPIClass = getattr(bulk_entry_page, '_CHEETAH_templateClass', Template)
    templateAPIClass._addCheetahPlumbingCodeToClass(bulk_entry_page)


# CHEETAH was developed by Tavis Rudd and Mike Orr
# with code, advice and input from many other volunteers.
# For more information visit http://www.CheetahTemplate.org/

##################################################
## if run from command line:
if __name__ == '__main__':
    from Cheetah.TemplateCmdLineIface import CmdLineIface
    CmdLineIface(templateObj=bulk_entry_page()).run()



#!/usr/bin/env python




##################################################
## DEPENDENCIES
import sys
import os
import os.path
try:
    import builtins as builtin
except ImportError:
    import __builtin__ as builtin
from os.path import getmtime, exists
import time
import types
from Cheetah.Version import MinCompatibleVersion as RequiredCheetahVersion
from Cheetah.Version import MinCompatibleVersionTuple as RequiredCheetahVersionTuple
from Cheetah.Template import Template
from Cheetah.DummyTransaction import *
from Cheetah.NameMapper import NotFound, valueForName, valueFromSearchList, valueFromFrameOrSearchList
from Cheetah.CacheRegion import CacheRegion
import Cheetah.Filters as Filters
import Cheetah.ErrorCatchers as ErrorCatchers

##################################################
## MODULE CONSTANTS
VFFSL=valueFromFrameOrSearchList
VFSL=valueFromSearchList
VFN=valueForName
currentTime=time.time
__CHEETAH_version__ = '2.4.4'
__CHEETAH_versionTuple__ = (2, 4, 4, 'development', 0)
__CHEETAH_genTime__ = 1389656993.0196061
__CHEETAH_genTimestamp__ = 'Mon Jan 13 18:49:53 2014'
__CHEETAH_src__ = 'login_page.tmpl'
__CHEETAH_srcLastModified__ = 'Fri May 17 14:51:15 2013'
__CHEETAH_docstring__ = 'Autogenerated by Cheetah: The Python-Powered Template Engine'

if __CHEETAH_versionTuple__ < RequiredCheetahVersionTuple:
    raise AssertionError(
      'This template was compiled with Cheetah version'
      ' %s. Templates compiled before version %s must be recompiled.'%(
         __CHEETAH_version__, RequiredCheetahVersion))

##################################################
## CLASSES

class login_page(Template):

    ##################################################
    ## CHEETAH GENERATED METHODS


    def __init__(self, *args, **KWs):

        super(login_page, self).__init__(*args, **KWs)
        if not self._CHEETAH__instanceInitialized:
            cheetahKWArgs = {}
            allowedKWs = 'searchList namespaces filter filtersLib errorCatcher'.split()
            for k,v in KWs.items():
                if k in allowedKWs: cheetahKWArgs[k] = v
            self._initCheetahInstance(**cheetahKWArgs)
        

    def respond(self, trans=None):



        ## CHEETAH: main method generated for this template
        if (not trans and not self._CHEETAH__isBuffering and not callable(self.transaction)):
            trans = self.transaction # is None unless self.awake() was called
        if not trans:
            trans = DummyTransaction()
            _dummyTrans = True
        else: _dummyTrans = False
        write = trans.response().write
        SL = self._CHEETAH__searchList
        _filter = self._CHEETAH__currentFilter
        
        ########################################
        ## START - generated method body
        
        #  MASAS Portal Cheetah Template - Login Page
        #  Last modified May 17, 2013
        write(u'''<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
 "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
  <meta http-equiv="content-type" content="text/html; charset=utf-8">
  <meta http-equiv="Cache-control" content="no-store; no-cache; must-revalidate">
  <meta http-equiv="Pragma" content="no-cache">
  <meta http-equiv="Expires" content="-1">
  <title>''')
        _v = VFFSL(SL,"site_name",True) # u'$site_name' on line 11, col 10
        if _v is not None: write(_filter(_v, rawExpr=u'$site_name')) # from line 11, col 10.
        write(u''' - Login</title>
''')
        if self.varExists('use_source'): # generated from line 12, col 1
            write(u'''  <link href="/src/portal/css/portal.css" type="text/css" rel="stylesheet">
''')
        else: # generated from line 14, col 1
            write(u'''  <link href="''')
            _v = VFFSL(SL,"resource_url",True) # u'$resource_url' on line 15, col 15
            if _v is not None: write(_filter(_v, rawExpr=u'$resource_url')) # from line 15, col 15.
            write(u'''/css/portal-min.css" type="text/css" rel="stylesheet">
''')
        write(u'''</head>
<body onLoad="document.forms[0].secret.focus();">

<div class="loginHeader">
  <div class="loginHeaderBanner">
    <p class="loginHeaderTitle"> ''')
        _v = VFFSL(SL,"site_name",True) # u'$site_name' on line 22, col 34
        if _v is not None: write(_filter(_v, rawExpr=u'$site_name')) # from line 22, col 34.
        write(u''' </p>
    <p class="loginHeaderSlogan"> ''')
        _v = VFFSL(SL,"site_slogan",True) # u'$site_slogan' on line 23, col 35
        if _v is not None: write(_filter(_v, rawExpr=u'$site_slogan')) # from line 23, col 35.
        write(u''' </p>
  </div>
</div>

<hr>

<form method="post" action="do_login" name="login">

  <div class="loginLanguageSwitch">
    Language 
    <select name="language" disabled="disabled">
      <option value="en">English</option>
    </select>
  </div>
  
  <div class="loginToolsTitle"> Tools Portal Login </div>
  
  <div class="loginStatusIndicator">
    <a href="''')
        _v = VFFSL(SL,"status_site_url",True) # u'$status_site_url' on line 41, col 14
        if _v is not None: write(_filter(_v, rawExpr=u'$status_site_url')) # from line 41, col 14.
        write(u'''" target="_blank" title="Open Statusboard">System Status:</a>
    &nbsp;<img src="''')
        _v = VFFSL(SL,"status_icon_url",True) # u'$status_icon_url' on line 42, col 21
        if _v is not None: write(_filter(_v, rawExpr=u'$status_icon_url')) # from line 42, col 21.
        write(u'''" id="statusIcon" alt="N/A" height="16" width="16">
  </div>
  
  <table border="0" cellpadding="2" cellspacing="2" width="100%">
    <tr>
      <td align="right" valign="middle" width="40%">
        <strong> Access Code: </strong>
      </td>
      <td align="left" valign="top">
''')
        if self.varExists('show_text_field'): # generated from line 51, col 1
            write(u'''        <input maxlength="25" size="25" type="text" name="secret">
''')
        else: # generated from line 53, col 1
            write(u'''        <input maxlength="25" size="25" type="password" name="secret">
''')
        write(u'''      </td>
      <td align="left" valign="top">
''')
        if self.varExists('secret_error'): # generated from line 58, col 1
            write(u'''        <span style="color: red;"> ''')
            _v = VFFSL(SL,"secret_error",True) # u'$secret_error' on line 59, col 36
            if _v is not None: write(_filter(_v, rawExpr=u'$secret_error')) # from line 59, col 36.
            write(u''' </span>
''')
        write(u'''      </td>
    </tr>
    <tr>
      <td colspan="3" height="5">&nbsp;</td>
    </tr>
    <tr>
      <td></td>
      <td align="left"><input type="submit" value="-  Login  -"></td>
      <td></td>
    </tr>
  </table>
  
  <div class="loginDeviceType">
    <input type="radio" name="device-type" value="desktop" id="deviceDesktop" ''')
        if VFFSL(SL,"user_agent",True) == "desktop": # generated from line 75, col 1
            write(u'''checked="checked" ''')
        write(u'''>
    <label for="deviceDesktop">Desktop</label>
    <input type="radio" name="device-type" value="tablet" id="deviceTablet" ''')
        if VFFSL(SL,"user_agent",True) == "tablet": # generated from line 81, col 1
            write(u'''checked="checked" ''')
        write(u'''>
    <label for="deviceTablet">Tablet</label>
    <input type="radio" name="device-type" value="mobile" id="deviceMobile" ''')
        if VFFSL(SL,"user_agent",True) == "mobile": # generated from line 87, col 1
            write(u'''checked="checked" ''')
        write(u'''>
    <label for="deviceMobile">Mobile</label>
  </div>
  
  <div class="loginVersionSwitch">
    <select name="version">
      <option value="normal">Normal</option>
      <option value="debug_tools">Debug Tools</option>
      <option value="debug_all">Debug All</option>
    </select>
  </div>

</form>

<hr>

<div class="loginFooter">
  <a href="''')
        _v = VFFSL(SL,"site_name_url",True) # u'$site_name_url' on line 107, col 12
        if _v is not None: write(_filter(_v, rawExpr=u'$site_name_url')) # from line 107, col 12.
        write(u'''">''')
        _v = VFFSL(SL,"site_name",True) # u'$site_name' on line 107, col 28
        if _v is not None: write(_filter(_v, rawExpr=u'$site_name')) # from line 107, col 28.
        write(u'''</a>
</div>

</body>
</html>''')
        
        ########################################
        ## END - generated method body
        
        return _dummyTrans and trans.response().getvalue() or ""
        
    ##################################################
    ## CHEETAH GENERATED ATTRIBUTES


    _CHEETAH__instanceInitialized = False

    _CHEETAH_version = __CHEETAH_version__

    _CHEETAH_versionTuple = __CHEETAH_versionTuple__

    _CHEETAH_genTime = __CHEETAH_genTime__

    _CHEETAH_genTimestamp = __CHEETAH_genTimestamp__

    _CHEETAH_src = __CHEETAH_src__

    _CHEETAH_srcLastModified = __CHEETAH_srcLastModified__

    _mainCheetahMethod_for_login_page= 'respond'

## END CLASS DEFINITION

if not hasattr(login_page, '_initCheetahAttributes'):
    templateAPIClass = getattr(login_page, '_CHEETAH_templateClass', Template)
    templateAPIClass._addCheetahPlumbingCodeToClass(login_page)


# CHEETAH was developed by Tavis Rudd and Mike Orr
# with code, advice and input from many other volunteers.
# For more information visit http://www.CheetahTemplate.org/

##################################################
## if run from command line:
if __name__ == '__main__':
    from Cheetah.TemplateCmdLineIface import CmdLineIface
    CmdLineIface(templateObj=login_page()).run()



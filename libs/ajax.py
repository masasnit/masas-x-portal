#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:           MASAS Tools Portal v0.2 - Ajax
Author:         Jacob Westfall
Created:        Nov 01, 2011
Updated:        Jan 13, 2014
Copyright:      Copyright (c) 2011-2014 MASAS Contributors.  Published under the
                Clear BSD license.  See license.txt for the full text of the license.
Description:	Methods to serve Ajax responses.
"""

import os, sys
import urllib2
try:
    import json
except ImportError:
    import simplejson as json
from cStringIO import StringIO
import re
import ast
import logging
import datetime
import zipfile
import xml.dom.minidom as minidom
import mimetypes
import uuid

# see requirements.txt
import cherrypy
import geopy
from geopy import distance
import redis

# custom modules
from utils import dbutils
from utils import shapefile
import common

# JSON data files
import event_list
import icon_list
import category_list
import template_list

# cheetah templates
from templates.entry_forward_msg import entry_forward_msg as entry_forward_msg


# python 2.6 or higher required
if sys.version < "2.6":
    print "Python 2.6 or higher is required"
    sys.exit(1)


class Ajax(object):
    """ Ajax response methods """
    
    def __init__(self, settings={}):
        """ Setup common root attributes """
        
        self.portal_settings = settings
        # mimetypes is used for saving entries
        mimetypes.init()
        # redis connection setup for temporary storage
        if "temporary_redis_config" in settings:
            self.temp_redis_conn = redis.Redis(
                host=settings["temporary_redis_config"]["host"],
                port=settings["temporary_redis_config"]["port"],
                db=settings["temporary_redis_config"]["db"],
                password=settings["temporary_redis_config"]["password"])
        else:
            self.temp_redis_conn = None
    
    
    @cherrypy.expose
    def default(self, *args, **kwargs):
        
        # setting the severity here to ensure it will be posted to email/sentry
        # instead of just logged to a file like normal
        cherrypy.log.error("%s attempted to access %s" %(cherrypy.request.remote.ip,
            cherrypy.request.path_info), context="APP", severity=logging.WARNING,
            headers=True)
        raise cherrypy.HTTPError(404)
    
## Portal
    
    @cherrypy.expose
    def check_login_status(self, *args, **kwargs):
        """ Periodically queried by JS windows to see if the user is still
        logged in or not """
        
        # only allow access from currently logged in users, otherwise 401
        self.check_auth_ajax()
    
    
    @cherrypy.expose
    def get_modes(self, *args, **kwargs):
        """ Return a JSON listing of available modes.
        
        mode_list = [{"name": "Training", "post": True, "selected": False},
            {"name": "Exercise", "post": False, "selected": True},
            {"name": "Operational", "post": False, "selected": False},
        ]
        """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        self.check_auth_ajax()
        hub_data = cherrypy.session.get("hubs", [])[:]
        user_mode = cherrypy.session.get("mode",
            self.portal_settings["masas_modes_order"][0])
        user_hubs = dict([(x["url"], x["post"]) for x in hub_data])
        mode_list = []
        # should be in a particular order
        for mode in self.portal_settings["masas_modes_order"]:
            found_mode = False
            mode_temp = {"name": mode, "post": False, "selected": False}
            if user_mode == mode:
                mode_temp["selected"] = True
            mode_hubs = [ x["url"] for x in self.portal_settings["masas_modes"]\
                [mode]["hubs"] ]
            for hub in mode_hubs:
                if hub in user_hubs:
                    # give user access to this mode
                    found_mode = True
                    if user_hubs[hub] == "Y":
                        # posting access to at least one hub in this mode
                        mode_temp["post"] = True
            if found_mode:
                mode_list.append(mode_temp)
        
        return json.dumps({"modes": mode_list})
    
    
    @cherrypy.expose
    def set_mode(self, *args, **kwargs):
        """ Set the tools mode """
        
        self.check_auth_ajax()
        if "mode" not in kwargs:
            raise cherrypy.HTTPError(400)
        if kwargs["mode"] not in self.portal_settings["masas_modes"]:
            raise cherrypy.HTTPError(400)
        cherrypy.session["mode"] = kwargs["mode"]
    
    
    @cherrypy.expose
    def error_report(self, *args, **kwargs):
        """ Error testing page """
        
        self.check_auth_ajax()
        account_uri = cherrypy.session.get("uri", None)[:]
        account_email = cherrypy.session.get("email", None)
        account_mode = cherrypy.session.get("mode", None)
        
        report = {
            "msg": kwargs.get("msg", "JS Error"),
            "line": kwargs.get("line", "undefined"),
            "file": kwargs.get("file", "undefined"),
            "history": kwargs.get("history", None),
            "stack": kwargs.get("stack", None),
            "ip": cherrypy.request.remote.ip,
            "browser": cherrypy.request.headers.get("User-Agent", None),
            "referer": cherrypy.request.headers.get("Referer", None),
            "uri": account_uri,
            "email": account_email,
            "mode": account_mode,
        }
        self.send_error_report(report)
        
        # setting the proper mimetype, even if nothing is returned, to prevent
        # warnings by some browsers
        cherrypy.response.headers["Content-Type"] = "image/gif"
        
        return
    
## Shared
    
    @cherrypy.expose
    def go(self, *args, **kwargs):
        """ Proxy for cross-domain javascript """
        
        self.check_auth_ajax()
        # the proxy for OpenLayers puts the url to be proxied as a url
        # encoded string in &url=...
        if "url" not in kwargs:
            cherrypy.response.status = 400
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nIllegal Request, Error: 1"
        url = kwargs["url"]
        if not url.startswith("http://") and not url.startswith("https://"):
            cherrypy.response.status = 400
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nIllegal Request, Error: 2"
        # Since all hosts are allowed by default, check for any specific
        # hostname matches that are denied because of security/abuse concerns
        for deny_check in self.portal_settings["proxy_denied_urls"]:
            if url.startswith("http://%s" %deny_check) or \
            url.startswith("https://%s" %deny_check):
                # accessing via logging instead of Root class
                auth_logger = logging.getLogger("authentication")
                auth_logger.warning("warning proxy url %s denied to %s" \
                    %(cherrypy.session["uri"], cherrypy.request.remote.ip))
                cherrypy.response.status = 400
                cherrypy.response.headers["Content-Type"] = "text/plain"
                return "\nIllegal Request, Error: 3"
        
        # default is 30 seconds which should be long enough to fetch a Feed
        # or an Entry, any longer and the user will wonder what is happening.
        # For POST or GET of other content, supplying a longer timeout
        # is possible because those operations will take longer with large
        # files, ie 3 minutes for 10 MB on most DSL service
        request_timeout = 30
        if "timeout" in kwargs:
            try:
                request_timeout = int(kwargs["timeout"])
                # cherrypy default is 5 mins max for a request
                if request_timeout > 300:
                    raise ValueError
            except:
                cherrypy.response.headers["Content-Type"] = "text/plain"
                return "\nIllegal Request, Error: 5"
        
        new_connection = None
        try:
            new_headers = {"User-Agent": "MASAS Portal Proxy"}
            if "Authorization" in cherrypy.request.headers:
                new_headers["Authorization"] = cherrypy.request.headers["Authorization"]
            if cherrypy.request.method == "POST":
                original_length = int(cherrypy.request.headers["Content-Length"])
                new_headers["Content-Type"] = cherrypy.request.headers["Content-Type"]
                original_body = cherrypy.request.body.read()
                new_request = urllib2.Request(url, original_body, new_headers)
                # There is a bug in older python versions where only 200
                # is an acceptable response vs 201
            elif cherrypy.request.method == "PUT":
                original_length = int(cherrypy.request.headers["Content-Length"])
                new_headers["Content-Type"] = cherrypy.request.headers["Content-Type"]
                original_body = cherrypy.request.body.read()
                new_request = urllib2.Request(url, original_body, new_headers)
                # consider using httplib2 which has builtin PUT method instead
                new_request.get_method = lambda: 'PUT'
            else:
                new_request = urllib2.Request(url, headers=new_headers)
            new_connection = urllib2.urlopen(new_request, timeout=request_timeout)
            new_info = new_connection.info()
            cherrypy.response.headers["Content-Type"] = \
                new_info.get("Content-Type", "text/plain")
            cherrypy.response.headers["Location"] = new_info.get("Location", None)
            # 10 MB max size on most Hubs
            new_data = new_connection.read(10485760)
            return new_data
        except Exception, err:
            #TODO: capture these errors in the log as well? there could be a
            #      lot of them that may not be of interest
            cherrypy.response.status = 500
            if hasattr(err, "reason"):
                if "timed out" in err.reason:
                    cherrypy.response.status = 408
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nProxy Error:\n\n%s" %err
        finally:
            if new_connection:
                new_connection.close()
    
    
    @cherrypy.expose
    def address_search(self, *args, **kwargs):
        """ Return a JSON listing of geocoded addresses """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        self.check_auth_ajax()
        search_data = {"results": []}
        # check for a good query val
        if "query" not in kwargs:
            return json.dumps(search_data)
        if len(kwargs["query"]) < 5:
            return json.dumps(search_data)
        if len(kwargs["query"]) > 150:
            return json.dumps(search_data)
        # need to make sure the query value is in unicode so can be encoded as
        # utf-8 to ensure google will search for an address with french characters
        try:
            kwargs["query"] = common.unicode_convert(kwargs["query"])
        except:
            cherrypy.log.error("Address query decoding error", context="APP",
                severity=logging.WARNING, traceback=True)
            return json.dumps(search_data)
        try:
            #TODO: add timeout to the geocoder attempt
            geocode = geopy.geocoders.GoogleV3()
            result = geocode.geocode(kwargs["query"].encode("utf-8"),
                exactly_one=False)
            for address, (lat, lon) in result:
                # ensure address result is in unicode for json -> utf-8
                try:
                    address = common.unicode_convert(address)
                except:
                    cherrypy.log.error("Address result decoding error",
                        context="APP", severity=logging.WARNING, traceback=True)
                    continue
                search_data["results"].append({"address": address,
                    "lat": "%.8f" %lat, "lon": "%.8f" %lon})
        except ValueError:
            # should be raised when there is no address found
            pass
        except:
            cherrypy.log.error("Geocode error", context="APP",
                severity=logging.WARNING, traceback=True)
        
        return json.dumps(search_data)
    
    
    @cherrypy.expose
    def forward_email(self, *args, **kwargs):
        """ Forwards an Entry or CAP message via email """
        
        self.check_auth_ajax()
        if cherrypy.request.method != "POST":
            raise cherrypy.HTTPError(400)
#TODO: validation of the "to" address
        if "to" not in kwargs and not kwargs["to"]:
            raise cherrypy.HTTPError(400)
        if "subject" not in kwargs and not kwargs["subject"]:
            raise cherrypy.HTTPError(400)
        if "message" not in kwargs and not kwargs["message"]:
            raise cherrypy.HTTPError(400)
        try:
            msg_data = {"email_address": kwargs["to"].encode("utf-8"),
                "subject": kwargs["subject"].encode("utf-8"),
                "message": kwargs["message"].encode("utf-8"),
                "return_address": self.portal_settings["email"]["return_address"],
                "site_name": self.portal_settings["site_name"],
            }
            new_msg = str(entry_forward_msg(searchList=[msg_data]))
            common.send_message(self.portal_settings["email"]["smtp_server"],
                kwargs["to"].encode("utf-8"),
                self.portal_settings["email"]["return_address"],
                new_msg)
        except:
            cherrypy.log.error("Email forward error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            raise cherrypy.HTTPError(400)
        
        return "Success"
    
## CAP
    
    @cherrypy.expose
    def cap_template(self, *args, **kwargs):
        """ Return either a JSON listing of all available templates or
        if a particular template is specified, return its JSON data.
        """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        self.check_auth_ajax()
        
        # default is first group name, unless specified
        group = template_list.cap_groups[0]
        if "group" in kwargs and kwargs["group"] in template_list.cap_groups:
            group = kwargs["group"]
        
        if "template" in kwargs and kwargs["template"]:
            return json.dumps(template_list.cap[group][int(kwargs["template"])])
        else:
            temps = []
            for temp_num, each_temp in enumerate(template_list.cap[group]):
                temps.append({"name": each_temp["name"], "num": temp_num})
            return json.dumps({"templates": temps})
    
    
    @cherrypy.expose
    def get_events(self, *args, **kwargs):
        """ Return a JSON listing of CAP events """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        self.check_auth_ajax()
        
        return json.dumps(event_list.event_list_data)
    
    
    @cherrypy.expose
    def get_areas(self, *args, **kwargs):
        """ Return a GeoJSON listing of applicable SGC areas given a point and radius """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        self.check_auth_ajax()
        if "point" not in kwargs:
            raise cherrypy.HTTPError("400", "Missing point value")
        if "radius" not in kwargs:
            raise cherrypy.HTTPError("400", "Missing radius value")
        # default value for optional type: csd, cd, and other
        sgc_type = kwargs.get("type", "csd")
        # optional param for GeoJSON versus JSON geocode array
        if "geocode" in kwargs and kwargs["geocode"] == "yes":
            geocodes_only = True
        else :
            geocodes_only = False
        try:
            search_result = self.sgc_search(point=kwargs["point"],
                radius=kwargs["radius"], type=sgc_type, geocodes=geocodes_only)
        except:
            cherrypy.log.error("SGC search error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            raise cherrypy.HTTPError("400", "Area search error")
        
        return json.dumps(search_result)
    
## Entry
    
    @cherrypy.expose
    def entry_template(self, *args, **kwargs):
        """ Return either a JSON listing of all available templates or
        if a particular template is specified, return its JSON data.
        """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        self.check_auth_ajax()
        
        # default is first group name, unless specified
        group = template_list.entry_groups[0]
        if "group" in kwargs and kwargs["group"] in template_list.entry_groups:
            group = kwargs["group"]
        
        if "template" in kwargs and kwargs["template"]:
            return json.dumps(template_list.entry[group][int(kwargs["template"])])
        else:
            temps = []
            for temp_num, each_temp in enumerate(template_list.entry[group]):
                temps.append({"name": each_temp["name"], "num": temp_num})
            return json.dumps({"templates": temps})
    
    
    @cherrypy.expose
    def get_categories(self, *args, **kwargs):
        """ Return a JSON listing of CAP based categories for an entry """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        self.check_auth_ajax()
        
        return json.dumps({"categories": category_list.category_list_data})
    
    
    @cherrypy.expose
    def get_icons(self, *args, **kwargs):
        """ Return a JSON listing/tree of EMS icons """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        self.check_auth_ajax()
        
        return json.dumps(icon_list.icon_list_tree)
    
    
    @cherrypy.expose
    def get_colour_data(self, *args, **kwargs):
        """ Return a JSON listing of a set of colour data for a matching
        category label in templates """
        
        cherrypy.response.headers["Content-Type"] = "application/json"
        self.check_auth_ajax()
        
        if "label" in kwargs and kwargs["label"]:
            for each_dataset in template_list.colour_datasets:
                if kwargs["label"].lower() == each_dataset["label"].lower():
                    return json.dumps({"data": each_dataset["data"]})
        else:
            raise cherrypy.HTTPError(400)
        
        return "[]"
    
    
    @cherrypy.expose
    def setup_attach(self, *args, **kwargs):
        """ Clear out past attachments and configure settings for selected Hub.
        Called after page loads as an async request. """
        
        self.check_auth_ajax()
        new_connection = None
        try:
            sess_data = {
                "attachment_types": {"application/atom+xml;type=entry": 1048576}
            }
            if "reset" in kwargs and kwargs["reset"] == "yes":
                # reset any previously saved values, should only happen at the
                # beginning of a session, later changes to a Hub should only need
                # the attachment types changed
                sess_data["attached_files"] = []
                sess_data["attached_size"] = 0
                sess_data["original_files"] = []
            if "feed" not in kwargs:
                raise cherrypy.HTTPError(400)
            if "secret" not in kwargs:
                raise cherrypy.HTTPError(400)
            hub_url = kwargs["feed"].strip()
            # remove the feed path to get the Hub root
            if hub_url.endswith("/"):
                hub_url = hub_url.replace("/feed/", "/")
            else:
                hub_url = hub_url.replace("/feed", "/")
            # load the attachment config from the hub's service document
            new_headers = {"User-Agent": "MASAS Portal Proxy"}
            new_request = urllib2.Request("%s?secret=%s" %(hub_url,
                kwargs["secret"].strip()), headers=new_headers)
            # max 15 seconds to download service doc
            new_connection = urllib2.urlopen(new_request, timeout=15)
            # max 1 MB for a service doc
            new_data = new_connection.read(1048576)
            sess_data["attachment_types"] = common.find_acceptable_types(new_data)
            #Andy's attempt at adding new file types
            sess_data["attachment_types"]["application/vnd.ms-excel"] = 1048576
            sess_data["attachment_types"]["application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"] = 1048576
            cherrypy.session.update(sess_data)
        except:
            cherrypy.log.error("Hub servicedoc error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            raise cherrypy.HTTPError(400)
        finally:
            if new_connection:
                new_connection.close()
    
    
    @cherrypy.expose
    def add_attach(self, *args, **kwargs):
        """ Helper method for attachments, stores new ones """
        
        # even though return values are JSON-like to work with ExtJS form
        # submission, the Content-Type has to remain text/html for this to work
        try:
            self.check_auth_ajax()
        except cherrypy.HTTPError:
            # instead of 401, use JSON return values to work with ExtJS 
            # form submission
            return "{ success: false, message: 'Not logged in.' }"
        if cherrypy.request.method != "POST":
            return "{ success: false, message: 'Only POST allowed.' }"
        
        if "kml" in kwargs and kwargs["kml"] == "yes":
            # KML layer type handling
            new_file_name = "attach.kml"
            try:
                new_file_data = kwargs["attachment-file"]
            except:
                cherrypy.log.error("KML attachment error", context="APP",
                    severity=logging.WARNING, traceback=True, request=False)
                return "{ success: false, message: 'Attachment file invalid.' }"
            new_file_type = "application/vnd.google-earth.kml+xml"
        else:
            # all other attachment types
            try:
                # iso-8859-1 used later for these values
                new_file_name = common.unicode_convert(kwargs["attachment-file"].filename)
            except:
                cherrypy.log.error("Attachment filename decoding error", context="APP",
                    severity=logging.WARNING, traceback=True, request=False)
                return "{ success: false, message: 'Attachment filename invalid.' }"
            new_file_data = kwargs["attachment-file"].file.read()
            new_file_type = str(kwargs["attachment-file"].content_type)
        new_file_length = len(new_file_data)
        try:
            new_file_title = common.unicode_convert(kwargs["attachment-title"])
        except:
            cherrypy.log.error("Attachment title decoding error", context="APP",
                severity=logging.WARNING, traceback=True, request=False)
            return "{ success: false, message: 'Attachment title invalid.' }"
        
        # convert some non-standard file types that Internet Explorer uses
        if new_file_type == "image/pjpeg" or new_file_type == "image/x-citrix-jpeg" \
        or new_file_type == "image/x-citrix-pjpeg":
            new_file_type = "image/jpeg"
        if new_file_type == "image/x-png" or new_file_type == "image/x-citrix-png":
            new_file_type = "image/png"
        if new_file_type == "image/x-citrix-gif":
            new_file_type = "image/gif"
        # in addition, this supports determing the mimeType based on file
        # extension, could use python mimetypes module but would still need to
        # update it for some of these newer values
        mime_mapping = {
            "doc": "application/msword",
            "docx": "application/vnd.openxmlformats-officedocument.wordprocessingml.document",
            "ppt": "application/vnd.ms-powerpoint",
            "pptx": "application/vnd.openxmlformats-officedocument.presentationml.presentation",
            "pdf": "application/pdf",
            "atom": "application/atom+xml",
            "cap": "application/common-alerting-protocol+xml",
            "xls": "application/vnd.ms-excel",
            "xlsx": "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        }
        
        try:
            # allowable attachment types are defined by the selected Hub
            if new_file_type not in cherrypy.session["attachment_types"]:
                new_file_extension = os.path.splitext(new_file_name)[1][1:].lower()
                if new_file_extension in mime_mapping:
                    new_file_type = mime_mapping[new_file_extension]
            # last try
            if new_file_type not in cherrypy.session["attachment_types"]:
                return "{ success: false, message: 'File type not supported. %s' }" \
                    %new_file_type
            # size limits may be different for each type
            if new_file_length > cherrypy.session["attachment_types"][new_file_type]:
                return "{ success: false, message: 'File size exceeded.' }"
            # the overall max size for an entire entry, including attachments,
            # is set in the atom entry type.  Need to also leave some room for
            # encoding and to add the Entry XML itself, 512 KB for now
            overall_max_size = cherrypy.session["attachment_types"]\
                ["application/atom+xml;type=entry"] - 524288
            if new_file_length + cherrypy.session["attached_size"] > overall_max_size:
                return "{ success: false, message: 'Overall file size exceeded.' }"
            # new attachments are always added to the end to use for len - 1 to
            # get the index location.  Removals have None substituted to keep
            # ordering in place.
            cherrypy.session["attached_files"].append(\
                (new_file_name, new_file_data, new_file_type, new_file_title,
                new_file_length))
            cherrypy.session["attached_size"] += new_file_length
            return "{ success: true, num: %s }" \
                %(len(cherrypy.session["attached_files"]) - 1)
        except:
            cherrypy.log.error("Add Attachment Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            return "{ success: false, message: 'Unable to attach file.' }"
    
    
    @cherrypy.expose
    def import_attach(self, *args, **kwargs):
        """ Helper method for attachments, imports past attachments """
        
        self.check_auth_ajax()
        if cherrypy.request.method != "POST":
            raise cherrypy.HTTPError(400)
        if "url" not in kwargs:
            raise cherrypy.HTTPError(400)
        url = kwargs["url"]
        if not url.startswith("http://") and not url.startswith("https://"):
            raise cherrypy.HTTPError(400)
        if "type" not in kwargs and not kwargs["type"]:
            raise cherrypy.HTTPError(400)
        if "title" not in kwargs and not kwargs["title"]:
            raise cherrypy.HTTPError(400)
        # iso-8859-1 used later for the title
        try:
            kwargs["title"] = common.unicode_convert(kwargs["title"])
        except:
            cherrypy.log.error("Attachment title decoding error", context="APP",
                severity=logging.WARNING, traceback=True, request=False)
            raise cherrypy.HTTPError(400)
        
        new_connection = None
        try:
            new_headers = {"User-Agent": "MASAS Portal Proxy"}
            new_request = urllib2.Request(url, headers=new_headers)
            # 1 minute should be enough for portal server with large bandwidth
            new_connection = urllib2.urlopen(new_request, timeout=60)
            # 10 MB max size on most hubs
            new_data = new_connection.read(10485760)
            new_data_size = len(new_data)
        except:
            raise cherrypy.HTTPError(400)
        finally:
            if new_connection:
                new_connection.close()
        
        try:
            # the overall max size for an entire entry, including attachments,
            # is set in the atom entry type.  Need to also leave some room for
            # encoding and to add the Entry XML itself, 512 KB for now
            overall_max_size = cherrypy.session["attachment_types"]\
                ["application/atom+xml;type=entry"] - 524288
            if new_data_size + cherrypy.session["attached_size"] > overall_max_size:
                raise ValueError("Overall attach size too big")
            cherrypy.session["attached_files"].append(\
                ("new.txt", new_data, kwargs["type"], kwargs["title"],
                new_data_size))
            cherrypy.session["original_files"].append(\
                ("new.txt", new_data, kwargs["type"], kwargs["title"],
                new_data_size))
            cherrypy.session["attached_size"] += new_data_size
            # index used for reference, normally all that's need to be returned
            # for attachment handling
            cherrypy.response.headers["Attach-Num"] = \
                len(cherrypy.session["attached_files"]) - 1
            if kwargs["type"] == "application/vnd.google-earth.kml+xml":
                # return KML so it can be added to the location map
                return new_data
        except:
            cherrypy.log.error("Import Attach Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            raise cherrypy.HTTPError(500)
    
    
    @cherrypy.expose
    def remove_attach(self, *args, **kwargs):
        """ Helper method for attachments, removes previous ones """
        
        self.check_auth_ajax()
        if cherrypy.request.method != "DELETE":
            raise cherrypy.HTTPError(400)
        if "num" not in kwargs and not kwargs["num"]:
            raise cherrypy.HTTPError(400)
        
        attach_num = int(kwargs["num"])
        if not cherrypy.session["attached_files"]:
            # nothing to remove
            raise cherrypy.HTTPError(400)
        try:
            if cherrypy.session["attached_files"][attach_num] == None:
                # already removed
                raise cherrypy.HTTPError(400)
            cherrypy.session["attached_size"] -= \
                cherrypy.session["attached_files"][attach_num][4]
            cherrypy.session["attached_files"][attach_num] = None
        except IndexError:
            raise cherrypy.HTTPError(400)
        
        return "Attachment %s removed" %attach_num
    
    
    @cherrypy.expose
    def post_attach(self, *args, **kwargs):
        """ Helper method for attachments, post as a multi-part entry """
        
        #NOTE: not checking, but on rare occasion multipart/related isn't an
        #      allowable type in the Hub service document, this won't work
        
        self.check_auth_ajax()
        if cherrypy.request.method != "POST":
            raise cherrypy.HTTPError(400)
        if "url" not in kwargs:
            raise cherrypy.HTTPError(400)
        url = kwargs["url"]
        if not url.startswith("http://") and not url.startswith("https://"):
            raise cherrypy.HTTPError(400)
        
        attach_files = []
        # copy and clean up any None placeholders where attachments were removed
        attach_files = [x for x in cherrypy.session["attached_files"][:] if x != None]
        
        new_connection = None
        try:
            entry_body = cherrypy.request.body.read()
            if attach_files:
                attach_files.insert(0, ("entry.xml", entry_body,
                    "application/atom+xml", "Entry XML", len(entry_body)) )
                boundary,multipart_body = common.multipart_encode(attach_files)
                new_headers = {"Content-Type": "multipart/related; boundary=%s" \
                    %boundary, "User-Agent": "MASAS Portal Proxy"}
                new_request = urllib2.Request(url, multipart_body, new_headers)
            else:
                # treat as normal Entry post to handle cases where files were
                # attached, then all removed, but the proxy url changed anyway
                new_headers = {"Content-Type": "application/atom+xml",
                    "User-Agent": "MASAS Portal Proxy"}
                new_request = urllib2.Request(url, entry_body, new_headers)
            # 2 minutes should be enough for portal server with large bandwidth
            new_connection = urllib2.urlopen(new_request, timeout=120)
            new_info = new_connection.info()
            cherrypy.response.headers["Content-Type"] = \
                new_info.get("Content-Type", "text/plain")
            cherrypy.response.headers["Location"] = new_info.get("Location", None)
            # 10 MB max size on most Hubs
            new_data = new_connection.read(10485760)
            # upon success, reset session temporary storage
            cherrypy.session["attached_files"] = []
            cherrypy.session["attached_size"] = 0
            cherrypy.session["original_files"] = []
            
            return new_data
        except Exception, err:
            cherrypy.log.error("Post Attach Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            cherrypy.response.status = 500
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nPosting Error:\n\n%s" %err
        finally:
            if new_connection:
                new_connection.close()
    
    
    @cherrypy.expose
    def put_attach(self, *args, **kwargs):
        """ Helper method for attachments, put as a multi-part entry """
        
        #NOTE: not checking, but on rare occasion multipart/related isn't an
        #      allowable type in the Hub service document, this won't work
        
        self.check_auth_ajax()
        if cherrypy.request.method != "PUT":
            raise cherrypy.HTTPError(400)
        if "url" not in kwargs:
            raise cherrypy.HTTPError(400)
        url = kwargs["url"]
        if not url.startswith("http://") and not url.startswith("https://"):
            raise cherrypy.HTTPError(400)
        
        attach_files = []
        # copy and clean up any None placeholders where attachments were removed
        attach_files = [x for x in cherrypy.session["attached_files"][:] if x != None]
        # no change, no need to update attachments
        if attach_files == cherrypy.session["original_files"]:
            return "No Change"
        
        new_connection = None
        try:
            if len(attach_files) == 0:
                # assuming that all previous attachments are supposed to be
                # deleted from this Entry as part of the Update.
                new_headers = {"User-Agent": "MASAS Portal Proxy"}
                new_request = urllib2.Request(url, headers=new_headers)
                # consider using httplib2 which has builtin DELETE method instead
                new_request.get_method = lambda: 'DELETE'
            elif len(attach_files) == 1:
                # a single attachment update
                new_headers = {"Content-Type": attach_files[0][2],
                    "Slug": attach_files[0][3],
                    "User-Agent": "MASAS Portal Proxy"}
                new_request = urllib2.Request(url, attach_files[0][1], new_headers)
                new_request.get_method = lambda: 'PUT'
            else:
                boundary,multipart_body = common.multipart_encode(attach_files)
                new_headers = {"Content-Type": "multipart/related; boundary=%s" \
                    %boundary, "User-Agent": "MASAS Portal Proxy"}
                new_request = urllib2.Request(url, multipart_body, new_headers)
                new_request.get_method = lambda: 'PUT'
            # 2 minutes should be enough for portal server with large bandwidth
            new_connection = urllib2.urlopen(new_request, timeout=120)
            # 10 MB max size on most Hubs
            new_data = new_connection.read(10485760)
            # upon success, reset session temporary storage
            cherrypy.session["attached_files"] = []
            cherrypy.session["attached_size"] = 0
            cherrypy.session["original_files"] = []
            
            return new_data
        except Exception, err:
            cherrypy.log.error("Put Attach Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            cherrypy.response.status = 500
            cherrypy.response.headers["Content-Type"] = "text/plain"
            return "\nPosting Error:\n\n%s" %err
        finally:
            if new_connection:
                new_connection.close()
    
    
    @cherrypy.expose
    def export_entry(self, *args, **kwargs):
        """ Save the Entry and return a ZIP.  This can take a long time to
        download not only the Entry but any attachments as well. """
        
        self.check_auth_ajax()
        if "url" not in kwargs:
            raise cherrypy.HTTPError("400", "URL is missing")
        if not kwargs["url"]:
            raise cherrypy.HTTPError("400", "URL is missing")
        if "secret" not in kwargs:
            raise cherrypy.HTTPError("400", "Secret is missing")
        if not kwargs["secret"]:
            raise cherrypy.HTTPError("400", "Secret is missing")
        try:
            # 30 seconds to download this Entry
            entry = common.url_download("%s?secret=%s" %(kwargs["url"],
                kwargs["secret"]), 30)
        except:
            cherrypy.log.error("Eport Download Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            raise cherrypy.HTTPError("400", "Unable to download entry URL")
        try:
            new_entry,attachments = self.find_entry_attachments(entry)
        except:
            cherrypy.log.error("Export Parsing Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            raise cherrypy.HTTPError("400", "Unable to parse entry XML")
        # zipfile needs a file like object to work with
        output = StringIO()
        archive = zipfile.ZipFile(output, 'w', zipfile.ZIP_DEFLATED)
        try:
            archive.writestr("entry.xml", new_entry)
            if attachments:
                for a_url,a_name in attachments:
                    # 3 minutes allotted for attachment download since it
                    # should be enough time for a 10 MB file on DSL
                    a_data = common.url_download("%s?secret=%s" %(a_url,
                        kwargs["secret"]), 180)
                    archive.writestr(a_name, a_data)
        except:
            cherrypy.log.error("Export Add Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            archive.close()
            output.close()
            raise cherrypy.HTTPError("400", "Unable to add Entry or Attachments")
        try:
            # need to close the archive first before contents can be read
            archive.close()
            contents = output.getvalue()
            output.close()
        except:
            cherrypy.log.error("Export ZIP Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            archive.close()
            output.close()
            raise cherrypy.HTTPError("400", "Unable to build ZIP file")
        
        cherrypy.response.headers["Content-Type"] = "application/zip"
        cherrypy.response.headers["Content-Disposition"] = 'attachment; filename="entry.zip"'
        
        return contents
    
    
    @cherrypy.expose
    def import_entry(self, *args, **kwargs):
        """ Import a zip file containing an Entry and any associated
        attachments, adding to the user's import feed """
        
        self.check_auth_ajax()
        # even though return values are JSON-like to work with ExtJS form
        # submission, the Content-Type has to remain text/html for this to work
        if cherrypy.request.method != "POST":
            return "{ success: false, message: 'Only POST allowed.' }"
        # really only care about the file upload data right now, ignoring
        # details like the filename, etc
        try:
            entry_zip = zipfile.ZipFile(kwargs["attachment-file"].file, "r")
        except:
            cherrypy.log.error("Import File Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            return "{ success: false, message: 'Not a ZIP file.' }"
        try:
            entry_xml = entry_zip.read("entry.xml")
        except:
            cherrypy.log.error("Import XML Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            entry_zip.close()
            return "{ success: false, message: 'Entry is missing.' }"
        try:
            doc = minidom.parseString(entry_xml)
            entry_element = doc.getElementsByTagNameNS("http://www.w3.org/2005/Atom",
                "entry")[0]
            if not entry_element:
                raise ValueError("Entry element missing")
            # get a placeholder location
            cherrypy.session["import_entries"].append(None)
            entry_pos = len(cherrypy.session["import_entries"]) - 1
            for link in doc.getElementsByTagName("link"):
                link_rel = link.getAttribute("rel")
                link_href = link.getAttribute("href")
                if link_rel == "edit":
                    # save the original link, assumably to a Hub entry
                    old_link = doc.createElement("link")
                    old_link.setAttribute("rel", "related")
                    old_link.setAttribute("title", "Original Entry Link")
                    old_link.setAttribute("href", link_href)
                    link.parentNode.appendChild(old_link)
                    # replace with the local relative link
                    link.setAttribute("href", "ajax/import_feed?entry=%s" %entry_pos)
                elif link_rel == "enclosure":
                    # use either redis or the session for temporary storage
                    if self.temp_redis_conn:
                        new_id = uuid.uuid4()
                        # expires after 6 hours
                        self.temp_redis_conn.setex(new_id, entry_zip.read(link_href),
                            21600)
                        # storing either an ID to get the data or the data itself
                        attach_data = new_id
                    else:
                        # attachment links should be relative filenames that
                        # are available in the zip file
                        attach_data = entry_zip.read(link_href)
                    cherrypy.session["import_attachments"].append(\
                        (link.getAttribute("type"), attach_data))
                    attach_pos = len(cherrypy.session["import_attachments"]) - 1
                    # replace with local relative links too
                    link.setAttribute("href", "ajax/import_feed?attachment=%s" %attach_pos)
                elif link_rel == "related":
                    pass
                else:
                    # other link such as history or edit-media, which won't be
                    # valid and need to be removed
                    link.parentNode.removeChild(link)
        except:
            cherrypy.log.error("Import Entry Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            return "{ success: false, message: 'Invalid Entry.' }"
        finally:
            entry_zip.close()
        try:
            # saving the modified version of the entry as a standalone entry
            # with declaration and namespaces
            if self.temp_redis_conn:
                new_id = uuid.uuid4()
                self.temp_redis_conn.setex(new_id, doc.toxml(encoding="utf-8"),
                    21600)
                entry_data = new_id
            else:
                entry_data = doc.toxml(encoding="utf-8")
            cherrypy.session["import_entries"][entry_pos] = entry_data
        except:
            cherrypy.log.error("Import Save Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            return "{ success: false, message: 'Import not Saved.' }"
        
        return "{ success: true }"
    
    
    @cherrypy.expose
    def import_feed(self, *args, **kwargs):
        """ A user's import feed containing any Entries they have uploaded """
        
        self.check_auth_ajax()
        if "entry" in kwargs:
            # direct access to an Entry
            try:
                entry_location = int(kwargs["entry"])
                cherrypy.response.headers["Content-Type"] = \
                    'application/atom+xml;type=entry;charset="utf-8"'
                if self.temp_redis_conn:
                    # either use the key stored at the entry location
                    return self.temp_redis_conn.get(
                        cherrypy.session["import_entries"][entry_location])
                else:
                    # or the data itself
                    return cherrypy.session["import_entries"][entry_location]
            except:
                cherrypy.log.error("Import Feed Entry Error", context="APP",
                    severity=logging.ERROR, traceback=True, headers=True,
                    user={"uri": cherrypy.session["uri"],
                        "email": cherrypy.session["email"]},
                    extra={"Mode": cherrypy.session["mode"]})
                raise cherrypy.HTTPError("400", "Entry Error")
        elif "attachment" in kwargs:
            # direct access to an Attachment
            try:
                attach_location = int(kwargs["attachment"])
                cherrypy.response.headers["Content-Type"] = \
                    cherrypy.session["import_attachments"][attach_location][0]
                if self.temp_redis_conn:
                    return self.temp_redis_conn.get(
                        cherrypy.session["import_attachments"][attach_location][1])
                else:
                    return cherrypy.session["import_attachments"][attach_location][1]
            except:
                cherrypy.log.error("Import Feed Attachment Error", context="APP",
                    severity=logging.ERROR, traceback=True, headers=True,
                    user={"uri": cherrypy.session["uri"],
                        "email": cherrypy.session["email"]},
                    extra={"Mode": cherrypy.session["mode"]})
                raise cherrypy.HTTPError("400", "Attachment Error")
        else:
            # generate a feed of any imported Entries
            try:
                feed = []
                declaration_match = re.compile(r"<\?xml.*\?>")
                entry_match = re.compile(r"<entry.*>")
                # start feed with minimum values
                feed.append("""<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom"
  xmlns:georss="http://www.georss.org/georss"
  xmlns:app="http://www.w3.org/2007/app"
  xmlns:age="http://purl.org/atompub/age/1.0"
  xmlns:mec="masas:extension:control"
  xmlns:met="masas:experimental:time"
  xmlns:mea="masas:experimental:attribute">
    <author>
        <name>Import Feed</name>
    </author>
    <id>import-feed</id>
    <link href="import_feed" rel="self" />
    <title type="text">Import Feed</title>
    <updated>%s</updated>\n""" %datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ"))
                # add revised entries suitable for a feed
                for entry in cherrypy.session["import_entries"]:
                    if self.temp_redis_conn:
                        # either use the stored key
                        feed_entry = self.temp_redis_conn.get(entry)
                    else:
                        # or the data itself
                        feed_entry = entry
                    feed_entry = declaration_match.sub("", feed_entry)
                    feed_entry = entry_match.sub("<entry>", feed_entry)
                    feed.append(feed_entry)
                # close feed
                feed.append("\n</feed>")
                cherrypy.response.headers["Content-Type"] = \
                    'application/atom+xml;type=feed;charset="utf-8"'
                
                return "\n".join(feed)
            except:
                cherrypy.log.error("Import Feed Error", context="APP",
                    severity=logging.ERROR, traceback=True, headers=True,
                    user={"uri": cherrypy.session["uri"],
                        "email": cherrypy.session["email"]},
                    extra={"Mode": cherrypy.session["mode"]})
                raise cherrypy.HTTPError("400", "Feed Error")
    
    
    @cherrypy.expose
    def favorite_entry(self, *args, **kwargs):
        """ Add this Entry to the favorites list """
        
        self.check_auth_ajax()
        if "url" not in kwargs:
            raise cherrypy.HTTPError("400", "URL is missing")
        if not kwargs["url"]:
            raise cherrypy.HTTPError("400", "URL is missing")
        if "secret" not in kwargs:
            raise cherrypy.HTTPError("400", "Secret is missing")
        if not kwargs["secret"]:
            raise cherrypy.HTTPError("400", "Secret is missing")
        try:
            # 30 seconds to download this Entry
            entry = common.url_download("%s?secret=%s" %(kwargs["url"],
                kwargs["secret"]), 30)
        except:
            cherrypy.log.error("Favorite Download Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            raise cherrypy.HTTPError("400", "Unable to download entry URL")
        try:
            doc = minidom.parseString(entry)
            entry_element = doc.getElementsByTagNameNS("http://www.w3.org/2005/Atom",
                "entry")[0]
            if not entry_element:
                raise ValueError("Entry element missing")
            # save XML again to ensure uniformity for re matches
            entry_xml = doc.toxml(encoding="utf-8")
        except:
            cherrypy.log.error("Favorite XML Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            raise cherrypy.HTTPError("400", "Unable to parse entry XML")
        try:
            declaration_match = re.compile(r"<\?xml.*\?>")
            entry_match = re.compile(r"<entry.*>")
            feed_entry = declaration_match.sub("", entry_xml)
            feed_entry = entry_match.sub("<entry>", feed_entry)
            # use either redis or the session for temporary storage
            if self.temp_redis_conn:
                new_id = uuid.uuid4()
                # expires after 6 hours
                self.temp_redis_conn.setex(new_id, feed_entry, 21600)
                # storing either an ID to get the data or the data itself
                entry_data = new_id
            else:
                entry_data = feed_entry
            cherrypy.session["favorite_entries"].append(entry_data)
        except:
            cherrypy.log.error("Favorite Save Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            raise cherrypy.HTTPError("400", "Unable to add to favorites")
    
    
    @cherrypy.expose
    def favorite_feed(self, *args, **kwargs):
        """ A user's favorite feed containing any Entries they have selected """
        
        self.check_auth_ajax()
        if "reset" in kwargs and kwargs["reset"] == "yes":
            cherrypy.session["favorite_entries"] = []
            return
        
        try:
            feed = []
            feed.append("""<?xml version="1.0" encoding="UTF-8"?>
<feed xmlns="http://www.w3.org/2005/Atom"
  xmlns:georss="http://www.georss.org/georss"
  xmlns:app="http://www.w3.org/2007/app"
  xmlns:age="http://purl.org/atompub/age/1.0"
  xmlns:mec="masas:extension:control"
  xmlns:met="masas:experimental:time"
  xmlns:mea="masas:experimental:attribute">
    <author>
        <name>Favorite Feed</name>
    </author>
    <id>favorite-feed</id>
    <link href="favorite_feed" rel="self" />
    <title type="text">Favorite Feed</title>
    <updated>%s</updated>\n""" %datetime.datetime.utcnow().strftime("%Y-%m-%dT%H:%M:%SZ"))
            for entry in cherrypy.session["favorite_entries"]:
                if self.temp_redis_conn:
                    # either use the stored key
                    feed_entry = self.temp_redis_conn.get(entry)
                else:
                    # or the data itself
                    feed_entry = entry
                feed.append(feed_entry)
            feed.append("\n</feed>")
            cherrypy.response.headers["Content-Type"] = \
                'application/atom+xml;type=feed;charset="utf-8"'
            
            return "\n".join(feed)
        except:
            cherrypy.log.error("Favorite Feed Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]},
                extra={"Mode": cherrypy.session["mode"]})
            raise cherrypy.HTTPError("400", "Feed Error")
    
    
    @cherrypy.expose
    def import_geometry(self, *args, **kwargs):
        """ Import a shapefile returning GeoJSON """
        
        self.check_auth_ajax()
        # even though return values are JSON-like to work with ExtJS form
        # submission, the Content-Type has to remain text/html for this to work
        if cherrypy.request.method != "POST":
            return "{ success: false, message: 'Only POST allowed.' }"
        features = []
        try:
            s_file = shapefile.Reader(shp=kwargs["attachment-file"].file)
            shapes = s_file.shapes()
            if len(shapes) < 1:
                raise ValueError("No geometry values found")
            if len(shapes) > 25:
                raise ValueError("Too many geometry values")
            for each_shape in shapes:
                # geo_interface returns proper GeoJSON
                features.append({"type": "Feature",
                    "geometry": each_shape.__geo_interface__})
        except:
            cherrypy.log.error("Import Geometry Error", context="APP",
                severity=logging.ERROR, traceback=True, headers=True,
                user={"uri": cherrypy.session["uri"],
                    "email": cherrypy.session["email"]})
            return "{ success: false, message: 'File invalid.' }"
        geojson = {"type": "FeatureCollection", "features": features}
        
        return "{ success: true, data: %s }" %json.dumps(geojson)


## Private Methods


    def check_auth_ajax(self):
        """ Checks to see if the user is already logged in for AJAX requests,
        no redirect, just a 401 error """
        
        if "logged_in" not in cherrypy.session or \
        not cherrypy.session["logged_in"]:
            #TODO: log this access attempt
            raise cherrypy.HTTPError(401)
    
    
    def sgc_search(self, point, radius, type, geocodes):
        """ Search for any SGC zones given a lat,lon and radius """
        
        # validate the input values
        lat,lon = point.split(",")
        center_lat = float(lat)
        if center_lat > 90 or center_lat < -90:
            raise ValueError("Latitude incorrect")
        center_lon = float(lon)
        if center_lon > 180 or center_lon < -180:
            raise ValueError("Longitude incorrect")
        # being provided in KM
        radius_km = float(radius)
        if radius_km < 0:
            raise ValueError("Radius incorrect")
        if type not in ["cd", "csd", "other"]:
            raise ValueError("Invalid type")
        
        if radius_km == 0:
            # 0 is from a custom area and is a simple search
            sql_string = "SELECT SGC,Name,AsText(Geo) FROM %s WHERE \
Intersects(GeomFromText('Point(%s %s)'), Geo) LIMIT 50" \
                %(self.portal_settings["db_info"]["%s_table" %type],
                    center_lon, center_lat)
        else:
            # otherwise create a polygon with 8 points in a circle around the
            # point to use for search the database
            circle_points = []
            bearing = 0
            vincenty_calc = distance.VincentyDistance()
            while bearing < 360:
                new_point = vincenty_calc.destination((center_lat,
                    center_lon), bearing, radius_km)
                # WKT polygon points
                point_string = "%.4f %.4f" %(new_point[1], new_point[0])
                circle_points.append(point_string)
                bearing += 45
            # close the polygon
            circle_points.append(circle_points[0])
            sql_string = "SELECT SGC,Name,AsText(Geo) FROM %s WHERE \
Intersects(GeomFromText('Polygon((%s))'), Geo) LIMIT 50" \
                %(self.portal_settings["db_info"]["%s_table" %type],
                    ",".join(circle_points))
        #print sql_string
        db = dbutils.DBAccess(self.portal_settings["db_info"])
        db.db_cursor.execute(sql_string)
        result = db.db_cursor.fetchall()
        
        if geocodes:
            # return just geocodes for the custom area search, at a minimum
            # should return "none" as the geocode value to be used
            if not result:
                return ["none"]
            else:
                return [x["SGC"] for x in result]
        else:
            # return GeoJSON for location search and display, at a minimum
            # should return proper empty GeoJSON
            features = []
            for each_sgc in result:
                # convert brackets
                each_geom = each_sgc["AsText(Geo)"].replace("(", "[")
                each_geom = each_geom.replace(")", "]")
                # convert all points from "1.0 -2" to "[1.0, -2]"
                each_geom = re.sub(r'(-?\d+\.?\d*) (-?\d+\.?\d*)', r'[\1, \2]',
                    each_geom)
                # normally the areas are loaded into the database as iso-8859-1
                # string values.  need unicode to support json -> utf-8
                try:
                    each_sgc["Name"] = common.unicode_convert(each_sgc["Name"])
                except:
                    cherrypy.log.error("SGC %s decoding error" %each_sgc["SGC"],
                        context="APP", severity=logging.WARNING, traceback=True,
                        request=False)
                    continue
                new_feature = {"type": "Feature", "properties": {\
                    "e_name": each_sgc["Name"], "f_name": each_sgc["Name"],
                    "sgc": each_sgc["SGC"]} }
                # using the ast version of eval to change the each_geom string into
                # an actual list structure
                if "MULTI" in each_geom:
                    new_feature["geometry"] = {"type": "MultiPolygon",
                        "coordinates": ast.literal_eval(\
                            each_geom.replace("MULTIPOLYGON", ""))}
                else:
                    new_feature["geometry"] = {"type": "Polygon",
                        "coordinates": ast.literal_eval(\
                            each_geom.replace("POLYGON", ""))}
                features.append(new_feature)
            
            return {"type": "FeatureCollection", "features": features}
    
    
    def send_error_report(self, report):
        """ Send a JS error report to Sentry """
        
        user_data = {"uri": report["uri"], "email": report["email"]}
        # stacktraces are not supported by all browsers and there are different
        # formats, optional use
        frames = None
        #TODO: test an error parsing the stack and what comes out in the log
        try:
            if "stack" in report and report["stack"]:
                frames = common.parse_js_stacktrace(report["stack"])
        except:
            cherrypy.log.error("JS Stack Error", context="APP",
                severity=logging.WARNING, traceback=True, request=False)
        extra_data = {"File": report["file"], "Line": report["line"],
            "IP": report["ip"]}
        if "browser" in report and report["browser"]:
            extra_data["Browser"] = report["browser"]
        if "referer" in report and report["referer"]:
            extra_data["Referer"] = report["referer"]
        if "mode" in report and report["mode"]:
            extra_data["Mode"] = report["mode"]
        if "history" in report and report["history"]:
            history_vals = report["history"].split("|")
            # has newest events at the back, change for display
            history_vals.reverse()
            for h_pos,h_val in enumerate(history_vals):
                extra_data["History %s" %h_pos] = h_val
        
        # additional data being logged for this error, except the cherrypy
        # request info since it will always be the same for a JS error report
        cherrypy.log.error(report["msg"], context="JAVASCRIPT",
            severity=logging.ERROR, traceback=frames, request=False,
            user=user_data, extra=extra_data)
    
    
    def find_entry_attachments(self, entry):
        """ Parse the Entry and return a modified version with attachments """
        
        # common types allowed on a hub
        attachment_extensions = {
            "image/jpeg": ".jpg",
            "image/png": ".png",
            "image/gif": ".gif",
            "image/svg+xml": ".svg",
            "text/plain": ".txt",
            "text/rtf": ".rtf",
            "application/pdf": ".pdf",
            "application/vnd.ms-powerpoint": ".ppt",
            "application/vnd.openxmlformats-officedocument.presentationml.presentation": ".pptx",
            "application/msword": ".doc",
            "application/vnd.openxmlformats-officedocument.wordprocessingml.document": ".docx",
            "application/vnd.oasis.opendocument.text": ".odt",
            "application/vnd.oasis.opendocument.presentation": ".odp",
            "application/vnd.google-earth.kml+xml": ".kml",
            "application/vnd.google-earth.kmz": ".kmz",
            "audio/wav": ".wav",
            "audio/amr": ".amr",
        }
        
        attachments = []
        doc = minidom.parseString(entry)
        entry_element = doc.getElementsByTagNameNS("http://www.w3.org/2005/Atom",
            "entry")[0]
        if not entry_element:
            raise ValueError("Not an Entry")
        for link in doc.getElementsByTagName("link"):
            if link.getAttribute("rel") == "enclosure":
                link_url = link.getAttribute("href")
                link_type = link.getAttribute("type")
                # common Hub types
                extension = attachment_extensions.get(link_type, None)
                if not extension:
                    # fallback to mimetypes or a default
                    extension = mimetypes.guess_extension(link_type)
                    if not extension:
                        extension = ".xml"
                # should result in "content/0.jpg"
                new_link = "content/" + link_url.split("/content/")[1] + extension
                # saving original for download and new for zip filename
                attachments.append((link_url, new_link))
                # update in the entry XML so that its now a relative URL
                link.setAttribute("href", new_link)
        new_entry = doc.toxml(encoding="utf-8")
        
        return new_entry, attachments

#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:           MASAS Tools Portal v0.2 - Configuration
Author:         Jacob Westfall
Created:        Nov 01, 2011
Updated:        Jan 13, 2014
Copyright:      Copyright (c) 2011-2014 MASAS Contributors.  Published under the
                Clear BSD license.  See license.txt for the full text of the license.
Description:    Config settings for the tools portal server.
"""

cherrypy_server = {
    # use default production environment settings such as no auto reloading,
    # hiding tracebacks from errors sent to users, and no stdout logging
#    "environment": "production",
    # default is to serve on localhost only, this opens up to other hosts,
    # comment out for production
    "server.socket_host": "0.0.0.0",
    # always set the listening port
    "server.socket_port": 8080,
    # cherrypy default is 100MB, set to 15MB, the expected Hub limit is 10MB
    # but allow for some extra data in an upload to the portal
    "server.max_request_body_size": 15728640,
    # normally behind a proxy that is already access logging, so this turns it
    # off
    "log.access_file": "",
    # don't want to use normal error logging as we add our own handler with
    # better support so this should turn off
    "log.error_file": "",
    # use with debugging to stop autoreloading
#    "engine.autoreload_on": False,
    # fine tune this setting when multiple shared instances are used
    # the number of worker threads that are started and available, default is 10
#    "server.thread_pool": 25,
}

cherrypy_app = {"/": {
    # set default header values for responses
    "tools.response_headers.on": True,
    "tools.response_headers.headers": [("Server", "MASAS Portal")],
    "tools.sessions.on": True,
    # either 'custom' for single server RAM based session storage or 'redis'
    # for multi-server Redis based storage
    "tools.sessions.storage_type": "custom",
    # Redis config
    #"tools.sessions.host": "",
    #"tools.sessions.port": 6379,
    #"tools.sessions.db": 0,
    #"tools.sessions.password": None,
    #NOTE: for redis the expires timeout value is advanced each time a session
    #      value is "saved", so its X hours since that last operation, whereas
    #      for custom its X hours in total regardless of saves
    "tools.sessions.timeout": 60,
    # use with HTTPS only to ensure cookies aren't sent via HTTP
#    "tools.sessions.secure": True,
    # JS scripts can't try and read your session cookies
    "tools.sessions.httponly": True,
    # limit the cookies to this path only to prevent sending them for any
    # static file requests
    "tools.sessions.path": "/",
    # encoding for pages returned to users
    "tools.encode.on": True,
    "tools.encode.encoding": "utf-8",
    # don't save the headers for an error separately, already being done
    "tools.log_headers.on": False,
    # used for development only, serve static files directly for production
    #NOTE: staticdir sends/receives cookies with each request and if sessions
    #      are turned on, this means the sessions will be locked/unlocked too
    "tools.staticdir.root": "",
    "tools.staticdir.on": True, "tools.staticdir.dir": ".",
    # use a custom tool for IP address rewriting when behind a proxy
#    "tools.xff.on": True,
#    "tools.xff.debug": True,
    },
}

portal = {
    "site_name": "MASAS-Dev",
    # appears on login page only, can include <br>
    "site_slogan": "CANADA'S MULTI-AGENCY SITUATIONAL AWARENESS SYSTEMS",
    # should like to an "about" site rather than this tools portal
    "site_name_url": "http://www.masas-sics.ca",
    # use the /src folder with development versions, comment out for production
#    "use_source": True,
    # force tool debugging to allow for better testing of new deployments,
    # comment out to disable
    "force_debug_tools": True,
    # SGC zone database
    "db_info": {"host": "",
        "username": "",
        "password": "",
        "database": "",
        "cd_table": "",
        "csd_table": "",
        "other_table": "",
    },
    # access control remote API
    "auth_service_info": {"secret": "", "admin": "",
        "url": "https://access.masas-sics.ca/api/check_access/",
        "search": "https://access.masas-sics.ca/api/search/",
    },
    # Override auth for development and testing purposes, comment out to
    # disable this for normal operation
    #"override_user_secret": "abc",
    #"override_user_info": {"id": 1,
    #    "name": "",
    #    "phone": "123-123-1234", "email": "test@test.com",
    #    #"uri": "http://localhost/user/1", "groups": [],
    #    #"hubs": [{"url": "http://localhost", "post": "Y"}],
    #},
    # hub urls must match access directory values exactly, make sure the
    # first hub listed is the primary one for that mode as the others will
    # start as disabled until the user enables them
    "masas_modes": {\
        "Developer": {\
            "hubs": [
                {"title": "Sandbox2", "url": "https://sandbox2.masas-sics.ca/hub"},
            ]
        },
    },
    # display order for modes, names must match, first name is default
    #TODO: replace with ordered dict
    "masas_modes_order": ["Developer"],
    # http maps
    "google_map_url": "http://maps.googleapis.com/maps/api/js?v=3.14&sensor=false",
    "google_static_map_url": "http://maps.googleapis.com/maps/api/staticmap",
    # https maps
    #"google_map_url": "https://maps.googleapis.com/maps/api/js?v=3.14&sensor=false",
    #"google_static_map_url": "https://maps.googleapis.com/maps/api/staticmap",
    # the prefix that will be used to build CAP identifiers
    "cap_ident": "MASAS-Dev",
    # urls that cannot be used in proxy requests due to security concerns
    # don't comment out if not using, just make blank
    "proxy_denied_urls": ("localhost", "127.0.0."),
    # URLs to locations for resource files so they can be loaded by
    # apache or whatever server may be proxying this application, can load
    # locally if static file server is turned on
    "resource_url": "/static",
    # EMS icon service url, add trailing slash
    "icon_service_url": "http://icon.masas-sics.ca/",
    # url for the welcome page to load in the iframe after login
    # add ?site=dev for MASAS-Dev
    "welcome_url": "welcome",
    # url to use for Flex Tool, comment out to disable
    "flex_tool_url": "https://masas3.masas-sics.ca/MASAS-NIT/MASAS-X-NG-33/index.html?config=config-masas-x.xml",
    # url to use for Mobile Tool, should be same domain as the portal for
    # security reasons.  Comment out to disable
    "mobile_tool_url": "/src/mobile/index.html",
    # comment out to disable
    "bulk_tool_enable": True,
    "activity_tool_enable": True,
    # the absolute URL for the root of this portal site, used for proxying for
    # Flex/Mobile, no trailing slash
    "proxy_root_url": "/ajax",
    # url to redirect to when logging out, default is index
    "logout_url": "index",
    # email settings
    "email": {
        "smtp_server": "",
        # used for sending error messages, can be a list of addresses
        "admin_email": "",
        "subject": "Portal Error",
        # used for email forwarding, comment out to disable forwarding
        "return_address": "",
    },
    # statusboard sites for this portal
    "statusboard_site": "http://masas-dev-status.appspot.com",
    "data_statusboard_site": "http://masas-dev-data.appspot.com",
    # code used to protect administrative stats methods, comment out to disable
    "admin_stats_code": "",
    # settings for the status checking app
    "status_app": {
        # absolute path for this logfile
        "logfile": "./status.log",
        # URL for all services listing on statusboard
        "services_url": "http://masas-dev-status.appspot.com/api/v1/services/",
        # absolute file path to save status check state between runs
        "state_file": "./status-state.txt",
        # absolute file path for the up icon
        "up_icon": "./src/portal/img/status-green.png",
        # absolute file path for the down icon
        "down_icon": "./src/portal/img/status-red.png",
        # absolute file path for the status icon location used by portal
        "status_icon": "./src/portal/img/current-status.png",
    },
    # absolute filename for the new format error log
    "error_log_filename": "./portal.log",
    # absolute filename for the new format authentication log
    "auth_log_filename": "./auth.log",
    # settings for the error reporting app
    "report_app": {
        # errors sent to a sentry server with email as a backup or comment out to
        # use email only
        "sentry_dsn": "",
        # the server this report will be coming from, different server names for
        # different portal instances
        "server": "Portal",
        # absolute file path to save error report state between runs
        "state_file": "./report-state.txt",
    },
    # use redis to store temporary data more efficiently than in a session, as
    # each session access, such as using the go Proxy, would need to load any
    # temporary data again and again.  Setup a separate db for this instead.
    # Comment out to disable
    #"temporary_redis_config": {
    #    "host": "",
    #    "port": 6379,
    #    "db": 0,
    #    "password": None,
    #},
    # authors that will populate the saved authors grid
    "saved_authors_list": [\
        # uri, name, organization name
        ["https://access.masas-sics.ca/accounts/user/4/", "Canadian Weather",
            "Environment Canada"],
        ["https://access.masas-sics.ca/accounts/user/3/", "US Weather",
            "National Weather Service"],
        ["https://access.masas-sics.ca/accounts/user/2/", "Canadian Earthquakes",
            "Natural Resources Canada"],
        ["https://access.masas-sics.ca/accounts/user/64/", "BC Hydro",
            "BC Hydro"],
        ["https://access.masas-sics.ca/accounts/user/65/", "BC Highways",
            "BC Highways"],
        ["https://access.masas-sics.ca/accounts/user/66/", "Manitoba Highways",
            "Manitoba Highways"],
        ["https://access.masas-sics.ca/accounts/user/67/", "Ontario Fires",
            "Ontario Fires"],
        ["https://access.masas-sics.ca/accounts/user/68/", "Ontario Highways",
            "Ontario Highways"],
        ["https://access.masas-sics.ca/accounts/user/92/", "Hydro One",
            "Hydro One"],
        ["https://access.masas-sics.ca/accounts/user/93/", "Fortis Alberta",
            "Fortis Alberta"],
    ],
}

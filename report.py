#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:           MASAS Tools Portal v0.2 Server - Error Reporting
Author:         Jacob Westfall
Copyright:      Copyright (c) 2011-2013 MASAS Contributors.  Published under the
                Clear BSD license.  See license.txt for the full text of the license.
Created:        Nov 01, 2011
Updated:        Oct 21, 2013
Description:	This app parses logs for an errors to report.
"""

#TODO: consider exporting the log data as JSON instead of python repr
#TODO: consider comparing each log entry for uniqueness.  If the Content and
#      the message is the same, then just increment a count for the number
#      of times that same message has been seen.  Will help with flooding Sentry
#      with a lot of error messages, but care is needed to ensure nothing of
#      value will be dropped.


import os, sys
import optparse
import logging
from logging.handlers import SMTPHandler
import ast
import time

from raven import Client

# python 2.6 or higher required
if sys.version < "2.6":
    print "Python 2.6 or higher is required"
    sys.exit(1)


class LogReport(object):
    """ Reviews logfiles to find any errors to report """
    
    def __init__(self, debug=False, settings={}):
        """ Setup LogReport """
        
        self.debug = debug
        self.portal_settings = settings
        self.setup_logging()
    
    
    def setup_logging(self):
        """ use the python logging module to setup logging """
        
        self.logger = logging.getLogger("report")
        self.logger.setLevel(logging.DEBUG)
        # debugs go to the terminal screen
        screen = logging.StreamHandler()
        screen.setLevel(logging.DEBUG)
        # email errors
        email = SMTPHandler(self.portal_settings["email"]["smtp_server"],
            self.portal_settings["email"]["admin_email"],
            self.portal_settings["email"]["admin_email"],
            "%s - Report" %self.portal_settings["email"]["subject"])
        email.setLevel(logging.ERROR)
        # new format that adds function names and removes milliseconds
        format1 = logging.Formatter("%(asctime)s - %(name)s.%(funcName)s \
- %(message)s", datefmt='%Y-%m-%d %H:%M:%S')
        email.setFormatter(format1)
        format2 = logging.Formatter("%(levelname)s - %(message)s")
        screen.setFormatter(format2)
        # use screen debugging if turned on
        if self.debug:
            self.logger.addHandler(screen)
        self.logger.addHandler(email)
    
    
    def run(self):
        """ Runs the report """
        
        previous_state = self.read_state()
        if not previous_state:
            # start out with an old default
            old_date = "2013-01-01"
            old_time = "00:00:00,000"
        else:
            old_date, old_time = previous_state.split(" ")
        self.logger.debug("Using previous run date %s and time %s" %(old_date,
            old_time))
        
        try:
            new_logs, new_date, new_time = self.collect_logs(\
                self.portal_settings["error_log_filename"], old_date, old_time)
            new_logs = filter(log_filter, new_logs)
        except:
            self.logger.error("Collect Logs Error: ", exc_info=True)
            return
        self.logger.debug("%s new log entries found" %len(new_logs))
        
        # used to track error report attempts so that if Sentry server is down
        # its just not going to try over and over again
        report_attempt = 0
        for each_log in new_logs:
            # don't want to flood Sentry server such that it may appear its
            # down when its not
            time.sleep(0.5)
            if report_attempt > 3:
                self.logger.error("Error report attempt limit reached for %s %s" \
                    %(old_date, old_time))
                break
            try:
                if "sentry_dsn" in self.portal_settings["report_app"] and \
                self.portal_settings["report_app"]["sentry_dsn"]:
                    self.error_report(each_log)
                else:
                    # email backup, should only be used on a limited basis
                    self.logger.error("Error Report: %s" %each_log)
            except:
                report_attempt += 1
                self.logger.error("Sentry Error: ", exc_info=True)
        
        self.write_state("%s %s" %(new_date, new_time))
    
    
    def read_state(self):
        """ Read the saved state from a previous run """
        
        try:
            if os.path.exists(self.portal_settings["report_app"]["state_file"]):
                s_file = open(self.portal_settings["report_app"]["state_file"], "r")
                state = s_file.read().strip()
                s_file.close()
                self.logger.debug("Loaded state file")
                return state
            else:
                self.logger.warning("State file missing")
                return None
        except:
            self.logger.error("State File Read Error: ", exc_info=True)
            return None
    
    
    def write_state(self, state):
        """ Write the state to a file for next run to use """
        
        try:
            s_file = open(self.portal_settings["report_app"]["state_file"], "w")
            s_file.write(state)
            s_file.close()
            self.logger.debug("Wrote state file")
        except:
            self.logger.error("State File Write Error: ", exc_info=True)
    
    
    def collect_logs(self, filename, old_date, old_time):
        """ Review logfiles to find new log entries """
        
        logs = []
        # start out with the original filename
        result = self.new_file_input(filename, old_date, old_time)
        # the old_date/time wasn't found so that means its in an older file
        if not result[3]:
            self.logger.debug("Reviewing suffix logfiles")
            # should be descending order so oldest gets put in logs first
            for each_suffix in [".1"]:
                suffix_filename = filename + each_suffix
                if os.path.exists(suffix_filename):
                    suffix_result = self.new_file_input(suffix_filename,
                        old_date, old_time)
                    if suffix_result[0]:
                        logs.extend(suffix_result[0])
        logs.extend(result[0])
        
        return logs, result[1], result[2]
    
    
    def new_file_input(self, filename, p_date, p_time):
        """ Reads log entries from a file, provided a date/time to start from """
        
        self.logger.debug("Reading logfile %s" %filename)
        logs = []
        #TODO: ignore microseconds by modifying log file format?
        last_date = p_date
        last_time = p_time
        found_old = False
        
        with open(filename, "r") as f:
            for each_line in f:
                # first 4 values are space separated, rest are repr strings
                line_vals = each_line.split(" ", 4)
                if compare_datetime(p_date, p_time, line_vals[0], line_vals[1]):
                    logs.append(line_vals)
                    last_date = line_vals[0]
                    last_time = line_vals[1]
                else:
                    found_old = True
        
        #TODO: determine if when a new log entry is being written at the same time as
        #      its being read, whether an incomplete entry might have just the
        #      first parts and not the remainder?  If so, then it may be needed
        #      that the last log entry is normally dropped, going back to the 2nd
        #      last instead, and on the next run the last log entry "should" be
        #      complete and would be read then.
        
        return logs, last_date, last_time, found_old
    
    
    def error_report(self, log):
        """ Create a Sentry error report """
        
        data = {}
        extra = {}
        date, time, level, context, fields = log
        
        #NOTE: Sentry expects UTC only for this time right now
        # removing the milliseconds as Sentry doesn't like
        datetime = "%sT%s" %(date, time.split(",")[0])
        # defaults to ERROR, other levels are currently filtered out
        if level.lower() == "warning":
            data["level"] = logging.WARNING
        # defaults to "root"
        if context.lower() == "javascript":
            data["logger"] = "Javascript Error Console"
        elif context.lower() == "http":
            data["logger"] = "HTTP Framework"
        elif context.lower() == "app":
            data["logger"] = "Application"
        
        # convert from the string repr back to python
        p_fields = ast.literal_eval(fields)
        # not using list expansion as additional values may be added to the end
        # and so keeps flexiblity by not raising an index out of range error
        msg = p_fields.pop(0)
        traceback = p_fields.pop(0)
        request = p_fields.pop(0)
        headers = p_fields.pop(0)
        user = p_fields.pop(0)
        extra_info = p_fields.pop(0)
        
        # the culprit or file/module where the error took place can attempt to
        # be determined using the stack trace by default, however in some cases
        # it can be more precisely specified
        if context.lower() == "javascript":
            if "File" in extra_info and extra_info["File"] and \
            extra_info["File"] != "undefined":
                data["culprit"] = extra_info["File"]
        if traceback:
            data["sentry.interfaces.Stacktrace"] = \
                {"frames": format_stack_frames(traceback)}
        if request:
            data["sentry.interfaces.Http"] = {
                # url and method are required
                "url": request[0],
                "method": request[1],
            }
            if request[2]:
                data["sentry.interfaces.Http"]["query_string"] = \
                    format_http_query(request[2])
            if headers:
                data["sentry.interfaces.Http"]["headers"] = \
                    format_http_headers(headers)
        if user:
            data["sentry.interfaces.User"] = {
                # id is required, should the user's URI
                "id": user[0],
                # is_authenticated is required although not in newer sentry versions
                "is_authenticated": True,
                "email": user[1],
            }
        if extra_info:
            extra.update(extra_info)
        
        self.send_report(msg, datetime, data, extra)
    
    
    def send_report(self, message, date, data=None, extra=None):
        """ Send the report to Sentry """
        
        self.logger.debug("Sending report to Sentry")
        s_client = Client(dsn=self.portal_settings["report_app"]["sentry_dsn"],
            site=self.portal_settings["site_name"],
            name=self.portal_settings["report_app"]["server"])
        s_client.capture("Message", message=message, date=date, data=data,
            extra=extra)



def compare_datetime(p_date, p_time, n_date, n_time):
    """ Compare the new date/time to previous date/time to find newer values.
    Relies on simple string comparison of values """
    
    if n_date > p_date:
        return True
    elif n_date == p_date:
        if n_time > p_time:
            return True
    
    return False


def log_filter(log):
    """ Used to filter the log entries """
    
    # severity value is 3 position
    severity = log[2].lower()
    if severity == "info":
        return False
    elif severity == "debug":
        return False
    #TODO: filter by app Context as well?
    
    return True


def format_stack_frames(frames):
    """ Changes a list of tuples with stacktrace values into a list of dicts
    that is supported by Raven/Sentry """
    
    result = []
    for each_stack in frames:
        new_stack = {}
        filename, lineno, function, context = each_stack
        # filename and lineno are required
        if filename:
            new_stack["filename"] = filename
        else:
            new_stack["filename"] = "unknown"
        if lineno:
            new_stack["lineno"] = lineno
        else:
            new_stack["lineno"] = 1
        if function:
            new_stack["function"] = function
        if context:
            new_stack["context_line"] = context
        result.append(new_stack)
    
    return result


def format_http_query(query):
    """ Format the original query string, removing secrets and other vals """
    
    try:
        s_start = query.index("secret=")
    except ValueError:
        # no secret, no other formatting options
        return query
    # account for secret= characters
    s_start += 7
    try:
        s_end = query.index("&", s_start)
    except ValueError:
        # the secret is at the end of the query string
        s_end = len(query)
    s_text = query[s_start:s_end]
    
    return query.replace(s_text, "xxx")


def format_http_headers(headers):
    """ Changes a list of tuples with header values into a list of dicts
    and other formatting changes """
    
    result = {}
    ignore_headers = ["accept", "accept-encoding", "accept-language", "connection"]
    for h_key, h_val in headers:
        if h_key.lower() in ignore_headers:
            continue
        # hiding secret values
        if h_key.lower() == "authorization":
            if "masas-secret" in h_val.lower():
                h_val = "MASAS-Secret xxx"
        result[h_key] = h_val
    
    return result



def run():
    """ run method for Portal log report """
    
    usage = "usage: report.py [-d] [-c] config"
    parser = optparse.OptionParser(usage)
    parser.add_option("-d", action="store_true", dest="debug",
        help="print debugging messages")
    parser.add_option("-c", action="append", type="string", dest="config_file",
        help="read config from this python module")
    (options, args) = parser.parse_args()
    
    # don't catch config exceptions, let them show on commandline
    app_config = {}
    if options.config_file:
        # currently uses the first string provided by options and can
        # be relative or absolute url to .py file
        execfile(options.config_file[0], {}, app_config)
    else:
        # try a default config
        execfile("config.py", {}, app_config)
    
    # run once to start
    report = LogReport(debug=options.debug, settings=app_config["portal"])
    report.run()


if __name__ == '__main__':
    run()
#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:         Example Template List
Author:       Jacob Westfall
Copyright:    Independent Joint Copyright (c) 2011 MASAS Contributors.
              Released under the Modified BSD license.  See license.txt for
              the full text of the license.
Created:      Feb 01, 2011
Updated:      Aug 05, 2013
Description:  An example listing of template values for Entry and CAP.
"""

# template group names should be a single word only and the default group should
# be listed first

entry_groups = ["Desktop", "Tablet"]

entry = {
    "Desktop": [\
        {"name": "Road Obstruction",
         "category": "Transport",
         "icon": "ems/incident/roadway/roadwayDelay",
         "en_title": "_ is blocking access to _ Street",
         "en_content": "_ is blocking access to _ Street between _ and _ \
A detour has been setup from _ to _",
         "severity": "Minor",
         "expires": 24,
        },
        {"name": "Emergency Road Block",
         "category": "Transport",
         "icon": "ems/incident/roadway/roadwayClosure",
         "en_title": "Emergency road block setup by _ at _",
         "en_content": "_ has established an emergency road block at _ \
A detour has been setup from _ to _",
         "severity": "Moderate",
         "expires": 24,
        },
        {"name": "Evacuation Route",
         "category": "Transport",
         "icon": "ems/operations/emergency/emergencyEvacuationPoint",
         "en_title": "Evacuation Route from _ to _",
         "en_content": "An emergency evacuation route has been setup from _ \
to _  Traffic direction is being provided by _",
         "severity": "Severe",
         "expires": 48,
        },
        {"name": "EOC Activation",
         "category": "Safety",
         "icon": "ems/operations/emergency/emergencyOperationsCenter",
         "en_title": "Emergency Operations Centre Activated",
         "en_content": "The Emergency Operations Centre located at _ has been \
activated.  Contact _ for more information.",
         "severity": "Moderate",
         "expires": 48,
        },
        {"name": "Incident Command Centre",
         "category": "Safety",
         "icon": "ems/operations/emergency/incidentCommandCenter",
         "en_title": "Incident Command Centre setup at _",
         "en_content": "An Incident Command Centre has been setup at _ \
Access the Centre via _ Street.  Contact _ for more information.",
         "severity": "Severe",
         "expires": 48,
        },
        {"name": "Emergency Staging Site",
         "category": "Safety",
         "icon": "ems/operations/emergency/emergencyStagingArea",
         "en_title": "Emergency Staging Site setup at _",
         "en_content": "An Emergency Staging Site has been setup at _ \
Safe approach to the Site is via _ Street.",
         "severity": "Severe",
         "expires": 48,
        },
        {"name": "Medical Triage Site",
         "category": "Health",
         "icon": "ems/operations/emergencyMedical/triage",
         "en_title": "Medial Triage Site setup at _",
         "en_content": "A Medical Triage Site has been setup at _  Access the \
site via _ Street.",
         "severity": "Moderate",
         "expires": 48,
        },
        {"name": "Emergency Shelter",
         "category": "Safety",
         "icon": "ems/operations/emergency/emergencyShelter",
         "en_title": "Emergency Shelter setup at _",
         "en_content": "An Emergency Shelter has been setup at _ Contact _ \
for more information.",
         "severity": "Moderate",
         "expires": 48,
        },
        {"name": "Public Report",
         "category": "Other",
         "icon": "ems/incident/civil",
         "en_title": "Public Report regarding _",
         "en_content": "A Public Report was received by _ regarding _ \
located at _  Contact _ for more information.",
         "expires": 24,
        },
    ],
    "Tablet": [\
        {"name": "Road Obstruction",
         "category": "Transport",
         "icon": "ems/incident/roadway/roadwayDelay",
         "en_title": "Road Obstruction",
         "en_content": "A road obstruction has been found at this location.",
         "severity": "Minor",
         "expires": 24,
        },
        {"name": "Poor Road Conditions",
         "category": "Transport",
         "icon": "ems/incident/roadway/hazardousRoadConditions",
         "en_title": "Poor Road Conditions",
         "en_content": "Poor road conditions have been found at this location.",
         "severity": "Minor",
         "expires": 24,
        },
        {"name": "Weather Report",
         "category": "Met",
         "icon": "ems/incident/meteorological",
         "en_title": "Weather Report",
         "en_content": "Weather report from this location.",
         "severity": "Minor",
         "expires": 24,
        },
        {"name": "Fire Report",
         "category": "Fire",
         "icon": "ems/incident/fire",
         "en_title": "Fire Report",
         "en_content": "Fire report from this location.",
         "severity": "Minor",
         "expires": 24,
        },
        {"name": "Flood Report",
         "category": "Met",
         "icon": "ems/incident/flood",
         "en_title": "Flood Report",
         "en_content": "Flood report from this location.",
         "severity": "Minor",
         "expires": 24,
        },
        {"name": "Other Incident Report",
         "category": "Other",
         "icon": "ems/operations/emergency",
         "en_title": "Incident Report",
         "en_content": "An on scene incident report from this location.",
         "severity": "Minor",
         "expires": 24,
        },
    ]
}

cap_groups = ["Desktop"]

cap = {
    "Desktop": [\
        {"name": "Road Closure",
         "event": "roadClose",
         "urgency": "Immediate",
         "severity": "Moderate",
         "certainty": "Observed",
         "en_headline": "_ Street is closed between _ and _",
         "en_description": "_ Street is closed between _ and _ due to _ \
A detour has been setup from _ to _  The street is expected to reopen _",
         "expires": 24,
        },
        {"name": "Fire",
         "event": "fire",
         "urgency": "Immediate",
         "severity": "Moderate",
         "certainty": "Observed",
         "en_headline": "Fire located at _",
         "en_description": "A Fire alert has been issued by _ regarding a \
_ fire located at _  Responders from _ are on scene.",
         "expires": 24,
        },
        {"name": "Flooding",
         "event": "flood",
         "urgency": "Immediate",
         "severity": "Moderate",
         "certainty": "Observed",
         "en_headline": "Flood (Watch/Warning) for _",
         "en_description": "A Flood (Watch/Warning) for _ has been issued by _",
         "expires": 24,
        },
        {"name": "Hazmat Incident",
         "event": "hazmat",
         "urgency": "Immediate",
         "severity": "Moderate",
         "certainty": "Observed",
         "en_headline": "Hazmat Incident located at _",
         "en_description": "A Hazmat Alert has been issued by _ regarding a \
(type of incident) at _  Responders from _ are on scene.",
         "expires": 24,
        },
        {"name": "Electrical Outage",
         "event": "electric",
         "urgency": "Immediate",
         "severity": "Minor",
         "certainty": "Observed",
         "en_headline": "Electrical Outage at _",
         "en_description": "An Electrical Outage has been reported at _ \
Repair crews from _ are on scene.  Restoration is expected by _",
         "expires": 24,
        },
    ],
}
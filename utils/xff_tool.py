#!/usr/bin/env python
"""
Name:           MASAS Tools Portal v0.2 - Custom X-Forwarded-For Tool
Author:         Jacob Westfall
Created:        Nov 27, 2011
Updated:        Oct 25, 2012
Copyright:      Copyright (c) 2011-2012 Jacob Westfall
License:        Internal Use Only, No Redistribution Permitted
Description:    This tool provides the IP address when behind a proxy.  Used
                instead of the included proxy tool because we don't want the
                base url rewriting that it normally does.
"""

# see requirements.txt
import cherrypy

def xff_rewrite(remote="X-Forwarded-For", debug=False):
    """ Derived from cherrypy.tools.proxy
    
    cherrypy.request.remote.ip (the IP address of the client) will be
    rewritten. By default is using 'X-Forwarded-For'. """
    
    request = cherrypy.serving.request
    xff = request.headers.get(remote)
    if debug:
        cherrypy.log("Testing remote %r:%r" %(remote, xff), context="XFF")
    if xff:
        if remote == "X-Forwarded-For":
            # See http://bob.pythonmac.org/archives/2005/09/23/apache-x-forwarded-for-caveat/
            xff = xff.split(',')[-1].strip()
        request.remote.ip = xff

cherrypy.tools.xff = cherrypy.Tool("before_request_body", xff_rewrite, priority=30)

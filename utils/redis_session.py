#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:           MASAS Tools Portal v0.2 Server - Redis Sessions
Author:         Jacob Westfall
Created:        Apr 28, 2013
Updated:        Jan 13, 2014
Copyright:      Copyright (c) 2013-2014 Jacob Westfall
License:        Internal Use Only, No Redistribution Permitted
Description:	Provides a Redis backend for CherryPy sessions.  Based on
                https://github.com/3kwa/cherrys   Redis server must support
                SETEX http://redis.io/commands/setex
"""

import time
import datetime
import threading
try:
  import cPickle as pickle
except ImportError:
  import pickle

# see requirements.txt
import cherrypy
import redis


class LockTimeout(BaseException):
    """ Raised in the event a timeout occurs while waiting for a lock """


class RedisSession(cherrypy.lib.sessions.Session):
    """ Redis session storage """
    
    # the default settings
    host = "127.0.0.1"
    port = 6379
    db = 0
    password = None
    # lock acquire attempts will block for x seconds before timing out
    lock_timeout = 30
    
    
    @classmethod
    def setup(cls, **kwargs):
        """ implementation specifc method to set up the storage system for
        redis-based sessions. Called once when the built-in tool calls
        sessions.init. """
        
        # overwritting default settings with the config dictionary values
        for k, v in kwargs.items():
            setattr(cls, k, v)
        
        cls.cache = redis.Redis(
            host=cls.host,
            port=cls.port,
            db=cls.db,
            password=cls.password)
    
    
    def _exists(self):
        """ implementation specific method to check for a key """
        
        return bool(self.cache.exists(self.id))
    
    
    def _load(self):
        """ implementation specific method to load a key's data, called from
        session .load() """
        
        try:
          return pickle.loads(self.cache.get(self.id))
        except TypeError:
          # if id not defined pickle can't load None and raise TypeError
          return None
    
    
    def _save(self, expiration_time):
        """ implementation specific method to save a key's data, called from
        session .save() """
        
        pickled_data = pickle.dumps(
            (self._data, expiration_time),
            pickle.HIGHEST_PROTOCOL)
        result = self.cache.setex(self.id, pickled_data, self.timeout * 60)
        if not result:
            raise AssertionError("Session data for id %r not set." %self.id)
    
    
    def _delete(self):
        """ implementation specific method to delete a key, called from
        session .delete() """
        
        self.cache.delete(self.id)
    
    
    def __len__(self):
        """ implementation specific method for the number of active sessions """
        
        return self.cache.dbsize()
    
    
    def acquire_lock(self, id=None, timeout=None):
        """ implementation specific method to acquire an exclusive lock on the
        currently-loaded session data. Will block for a period of seconds until
        timeout is reached.  Uses the same Redis DB as the session cache. """
        
        if not id:
            id = self.id
        if not timeout:
            timeout = self.lock_timeout
        lock_key = "%s-lock" %id
        while timeout >= 0:
            if not self.cache.get(lock_key):
                # this sets the lock and ensures that it will be cleaned up
                # without needing a separate cleanup() operation as well as
                # ensure it won't stick around longer than any other acquire
                # attempt
                result = self.cache.setex(lock_key, True, self.lock_timeout)
                if not result:
                    raise AssertionError("Lock for id %r not set." %id)
                self.locked = True
                return
            timeout -= 1
            if timeout >= 0:
                time.sleep(1)
        raise LockTimeout("Timeout acquiring lock")
    
    
    def release_lock(self, id=None):
        """ implementation specific method to release the lock on the
        currently-loaded session data. """
        
        if not id:
            id = self.id
        self.cache.delete("%s-lock" %id)
        self.locked = False
    
    
    def direct_load(self, id):
        """ additional method used to get the data directly without Session
        init, combines elements from ._load() """
        
        try:
          return pickle.loads(self.cache.get(id))[0]
        except TypeError:
          # if id not defined pickle can't load None and raise TypeError
          return None
    
    
    def direct_save(self, id, data):
        """ additional method used to store the data directly without Session
        init, combines elements from .save() and ._save() """
        
        t = datetime.timedelta(seconds = self.timeout * 60)
        expiration_time = self.now() + t
        if self.debug:
            cherrypy.log('Saving with expiry %s' % expiration_time, 'TOOLS.SESSIONS')
        pickled_data = pickle.dumps((data, expiration_time), pickle.HIGHEST_PROTOCOL)
        result = self.cache.setex(id, pickled_data, self.timeout * 60)
        if not result:
            raise AssertionError("Session data for id %r not set." %id)
    
    
    def direct_delete(self, id):
        """ additional method used to delete the data directly without Session
        init, combines elements from ._delete() """
        
        self.cache.delete(id)
    
    
    def get_all_keys(self):
        """ additional method used to get all of the current session keys """
        
        return [x for x in self.cache.keys() if not x.endswith("-lock")]



cherrypy.lib.sessions.RedisSession = RedisSession

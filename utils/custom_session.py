#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:           MASAS Tools Portal v0.2 Server - Custom RAM Sessions
Author:         Jacob Westfall
Created:        Jan 25, 2013
Updated:        Jan 13, 2014
Copyright:      Copyright (c) 2013-2014 Jacob Westfall
License:        Internal Use Only, No Redistribution Permitted
Description:	Provides a custom RAM backend for sessions that provides methods
                needed to sync with redis_session.py
"""

import time
import datetime
import threading

# see requirements.txt
import cherrypy


class CustomSession(cherrypy.lib.sessions.RamSession):
    """ Custom Ram session derived from RamSession """
    
    def acquire_lock(self, id=None, timeout=None):
        """ Acquire an exclusive lock on the currently-loaded session data. """
        
        if not id:
            id = self.id
        self.locks.setdefault(id, threading.RLock()).acquire()
        self.locked = True
    
    
    def release_lock(self, id=None):
        """ Release the lock on the currently-loaded session data. """
        
        if not id:
            id = self.id
        self.locks[id].release()
        self.locked = False
    
    
    def direct_load(self, id):
        """ additional method used to get the data directly without Session
        init, combines elements from ._load() """
        
        return self.cache.get(id)[0]
    
    
    def direct_save(self, id, data):
        """ additional method used to store the data directly without Session
        init, combines elements from .save() and ._save() """
        
        t = datetime.timedelta(seconds = self.timeout * 60)
        expiration_time = self.now() + t
        if self.debug:
            cherrypy.log("Saving with expiry %s" %expiration_time, "TOOLS.SESSIONS")
        self.cache[id] = (data, expiration_time)
    
    
    def direct_delete(self, id):
        """ additional method used to delete the data directly without Session
        init, combines elements from ._delete() """
        
        self.cache.pop(id, None)
    
    
    def get_all_keys(self):
        """ additional method used to get all of the current session keys """
        
        return self.cache.keys()



cherrypy.lib.sessions.CustomSession = CustomSession

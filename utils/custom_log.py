#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:           MASAS Tools Portal v0.2 Server - Custom Log Handling
Author:         Jacob Westfall
Created:        Mar 03, 2013
Updated:        Jan 13, 2014
Copyright:      Copyright (c) 2013-2014 Jacob Westfall
License:        Internal Use Only, No Redistribution Permitted
Description:	These are custom log handling functions.
"""

import sys
from traceback import extract_tb
import logging

# see requirements.txt
import cherrypy

# python 2.6 or higher required
if sys.version < "2.6":
    print "Python 2.6 or higher is required"
    sys.exit(1)


def error(self, msg=None, context=None, severity=logging.INFO,
    traceback=False, request=True, headers=False, user=None, extra=None):
    """ A new error handler to replace the cherrypy default in _cplogging.py
    
    This new error handling method allows logfiles to contain detailed
    information suitable for sending to Sentry.  The logfiles can start out
    using space delimited values such as "date time severity context" and
    then the remaining information, instead of trying to come up with a
    delimiter that would work for all cases, is simply dumped in using repr.
    This allows a parsing script to consider each individual line of the
    logfile as a single message and use eval to retrieve the additional info
    such as the traceback and headers.  Note, 'self' is provided when this new
    handler is monkey patched in.
    """
    
    result = ['', '', '', '', '', '']
    if not context:
        context = "-"
    if msg:
        # the msg could have spaces so its not part of the other space
        # delimited values
        result[0] = msg
    if traceback:
        # a stack trace frame is a tuple comprised of (filename, line number,
        # function name, text) with a full trace being a list of these frames
        try:
            if isinstance(traceback, list):
                # use a supplied list of frames
                result[1] = traceback
            else:
                # create a traceback
                exc = sys.exc_info()
                if not msg:
                    # assumes that its an unhandled error such as a 500 error
                    # and so fill in missing info
                    result[0] = str(exc[1])
                    headers = True
                result[1] = extract_tb(exc[2])
        except:
            #TODO: include this exception's traceback instead?
            result[1] = [(None, None, None, "Unable to generate traceback")]
    # defaulted to ensure framework errors will have the request info but
    # need to ignore other normal framework logging
    if request and context != "ENGINE":
        try:
            if isinstance(request, list):
                # a list with url, method, params
                result[2] = request
            else:
                # could use .base to add http://host to the path_info
                result[2] = [cherrypy.serving.request.path_info,
                    cherrypy.serving.request.method,
                    cherrypy.serving.request.query_string]
        except:
            result[2] = ["Unable to generate request info", "GET", ""]
    if headers:
        try:
            if isinstance(headers, list):
                # use the current headers in the form of a list of tuples
                # (name, value)
                result[3] = headers
            else:
                result[3] = cherrypy.serving.request.header_list
        except:
            #TODO: include this exception's traceback instead?
            result[3] = [("Error", "Unable to generate headers")]
    if user:
        try:
            result[4] = [user.get("uri", None), user.get("email", None)]
        except:
            #TODO: include this exception's traceback instead?
            result[4] = ["Unable to generate user info", None]
    if extra:
        result[5] = extra
    #TODO: is there a need to catch repr() errors or can it just loop around
    #      with the traceback being logged as a 500 error instead?
    result_str = repr(result)
    #NOTE: not sure if needed but removing newlines just in case, if it
    #      isn't a problem for logging but does cause readability issues
    #      then allow them back in
    result_str = result_str.replace("\n", "")
    
    self.error_log.log(severity, " ".join((context, result_str)))

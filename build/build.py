#!/usr/bin/env python
"""
Name:         MASAS Portal Build
Author:       Jacob Westfall
Created:      Oct 29, 2012
Updated:      Jan 13, 2014
Copyright:    Independent Joint Copyright (c) 2011-2014 MASAS Contributors
License:      Published under the Modified BSD license.  See license.txt for 
              the full text of the license.
Description:  Performs a number of automated build operations.
"""

import os, sys
import optparse
import shutil
from subprocess import PIPE, Popen
try:
    from tools import mergejs
except:
    print "mergejs is required"
    sys.exit(1)
try:
    from tools import jsmin
except:
    print "jsmin is required"
    sys.exit(1)
try:
    from tools import cssmin
except:
    print "cssmin is required"
    sys.exit(1)
if not os.path.isfile("tools/yuicompressor-2.4.7.jar"):
    print "yuicompressor is required"
    sys.exit(1)


# set the relative destination directory, no trailing slash
DEST = "../static"  


def build_deploy_dirs(trial=False, clean=False):
    """ Create or confirm existance of any deploy directories needed by other
    build functions, should be run first.  Cleanout prior to creating is
    supported. """
    
    if trial:
        print "Nothing to do"
        return
    
    # assumes a relative path and DEST will be added, must be in creation order
    dir_list = [\
        "js",
        "css",
        "img",
    ]
    
    if clean:
        for each_dir in dir_list:
            shutil.rmtree(os.path.join(DEST, each_dir))
    
    for each_dir in dir_list:
        new_dir = os.path.join(DEST, each_dir)
        if not os.path.isdir(new_dir):
            print "Creating directory %s" %new_dir
            os.mkdir(new_dir)
    
    print "\nDeployment directories ready"


def build_deploy_files(trial=False):
    """ Copy all files needed for deployment, should be run last """
    
    if trial:
        print "Nothing to do"
        return
    
    print "\n-- Deployment Files --"
    # assumes a relative path, specify a source file or directory and
    # destination directory, preceeding directory structure prior to copy will
    # be created, if a directory is specified all sub-dirs will also be copied
    #NOTE: renaming files during copy should also work
    file_list = [\
        ("../src/portal/themes/images/redWine/", "libs/ExtJS-3.4.2/images/redWine/"),
        ("../src/portal/themes/images/vodkaO/", "libs/ExtJS-3.4.2/images/vodkaO/"),
    ]
    for source,destination in file_list:
        if not os.path.exists(source):
            print "Error: %s doesn't exist" %source
        destination = os.path.join(DEST, destination)
        if os.path.isdir(source):
            # need to remove any existing destination first for copytree
            if os.path.exists(destination):
                shutil.rmtree(destination)
            # let copytree create any directories needed
            shutil.copytree(source, destination,
                ignore=shutil.ignore_patterns("*.bak"))
        else:
            # create necessary directories when copying single files
            dest_dir = os.path.dirname(destination)
            if not os.path.exists(dest_dir):
                os.makedirs(dest_dir)
            shutil.copy(source, destination)
        print "Copied %s to %s" %(source, destination)
    
    print "\nDeployment files copied"
    
    print "\nFiles in /src/portal/img /src/post/img /src/view/img /src/activity/img \
should be manually copied into place and any conflicts resolved"


def build_package(name, source, config, output=None, merger=None, compressor=None,
                  type=None, debug=None, trial_run=False):
    """ Builds a package by merging and compressing a set of files """
    
    # defaults
    if not merger:
        merger = "mergejs"
    if not compressor:
        compressor = "jsmin"
    if not type:
        type = "js"
    
    print "\n-- Building %s Package --" %name
    print "Merging files with %s" %merger
    if merger == "simple":
        # config should instead be a list of filenames to merge
        #NOTE: no trial run available for this merger
        merge_result = []
        for each_file in config:
            file_path = os.path.join(source, each_file)
            print "Exporting: %s" %each_file
            file_header = "/* " + "=" * 70 + "\n   %s\n" %each_file + "   " + \
                "=" * 70 + " */\n\n"
            merge_result.append(file_header)
            with open(file_path) as file_data:
                merge_result.append(file_data.read())
                merge_result.append("\n")
        merged = "".join(merge_result)
    else:
        if trial_run:
            # used to list expected imports and dependencies based on
            # require statements in each javascript file header
            mergejs.run(source, None, config, True)
            print "-- Package Complete --"
            return
        else:
            merged = mergejs.run(source, None, config)
    if debug:
        print "Writing debug output to %s" %debug
        file(debug, "w").write(merged)
    if compressor == "jsmin":
        print "Compressing using jsmin"
        minimized = jsmin.jsmin(merged)
    elif compressor == "cssmin":
        print "Compressing using cssmin"
        minimized = cssmin.cssmin(merged)
    elif compressor == "yui":
        print "Compressing using YUI"
        proc = Popen("java -jar tools/yuicompressor-2.4.7.jar --type %s" %type,
            stdin=PIPE, stdout=PIPE, shell=True)
        minimized, err = proc.communicate(merged)
        if err:
            raise IOError("YUI Error: %s" %err)
    if output:
        print "Writing compressed output to %s" %output
        file(output, "w").write(minimized)
    
    print "-- Package Complete --"


# Packaging functions, prefix of build_ is required


def build_view_css(trial=False):
    
    if trial:
        print "Nothing to do"
        return
    
    source_dir = "../src/view/css"
    config = ["view.css"]
    debug_file = "%s/css/view-debug.css" %DEST
    compress_file = "%s/css/view-min.css" %DEST
    
    build_package("View CSS", source_dir, config, output=compress_file,
        merger="simple", compressor="yui", type="css", debug=debug_file)


def build_view_overrides(trial=False):
    
    source_dir = "../src/view/js"
    config_file = "overrides-view.cfg"
    debug_file = "%s/js/view-overrides-debug.js" %DEST
    compress_file = "%s/js/view-overrides-min.js" %DEST
    
    build_package("View Overrides", source_dir, config_file, output=compress_file,
        compressor="yui", debug=debug_file, trial_run=trial)


def build_view_application(trial=False):
    
    source_dir = "../src/view/js"
    config_file = "view.cfg"
    debug_file = "%s/js/view-debug.js" %DEST
    compress_file = "%s/js/view-min.js" %DEST
    
    build_package("View Application", source_dir, config_file, output=compress_file,
        compressor="yui", debug=debug_file, trial_run=trial)


def build_post_css(trial=False):
    
    if trial:
        print "Nothing to do"
        return
    
    source_dir = "../src/post/css"
    config = ["post.css"]
    debug_file = "%s/css/post-debug.css" %DEST
    compress_file = "%s/css/post-min.css" %DEST
    
    build_package("Post CSS", source_dir, config, output=compress_file,
        merger="simple", compressor="yui", type="css", debug=debug_file)


def build_post_application(trial=False):
    
    source_dir = "../src/post/js"
    for each_app in ["new-entry", "update-entry", "new-cap", "update-cap", "bulk-entry"]:
        config_file = "post-%s.cfg" %each_app
        debug_file = "%s/js/%s-debug.js" %(DEST, each_app)
        compress_file = "%s/js/%s-min.js" %(DEST, each_app)
        build_package("Post Application %s" %each_app, source_dir, config_file,
            output=compress_file, compressor="yui", debug=debug_file,
            trial_run=trial)


def build_activity_css(trial=False):
    
    if trial:
        print "Nothing to do"
        return
    
    source_dir = "../src/activity/css"
    config = ["activity.css"]
    debug_file = "%s/css/activity-debug.css" %DEST
    compress_file = "%s/css/activity-min.css" %DEST
    
    build_package("Activity CSS", source_dir, config, output=compress_file,
        merger="simple", compressor="yui", type="css", debug=debug_file)


def build_activity_application(trial=False):
    
    source_dir = "../src/activity/js"
    config_file = "activity.cfg"
    debug_file = "%s/js/activity-debug.js" %DEST
    compress_file = "%s/js/activity-min.js" %DEST
    
    build_package("Activity Application", source_dir, config_file, output=compress_file,
        compressor="yui", debug=debug_file, trial_run=trial)


def build_theme_css(trial=False):
    
    if trial:
        print "Nothing to do"
        return
    
    source_dir = "../src/portal/themes/css"
    for each_theme in [("all-notheme", "ext-all-notheme.css"),
    ("operational", "xtheme-redWine.css"), ("exercise", "xtheme-vodkaO.css")]:
        config = [each_theme[1]]
        debug_file = "%s/libs/ExtJS-3.4.2/css/ext-%s-debug.css" %(DEST,
            each_theme[0])
        compress_file = "%s/libs/ExtJS-3.4.2/css/ext-%s-min.css" %(DEST,
            each_theme[0])
        
        build_package("%s CSS" %each_theme[0], source_dir, config,
            output=compress_file, merger="simple", compressor="yui", type="css",
            debug=debug_file)


def build_portal_css(trial=False):
    
    if trial:
        print "Nothing to do"
        return
    
    source_dir = "../src/portal/css"
    config = ["portal.css"]
    debug_file = "%s/css/portal-debug.css" %DEST
    compress_file = "%s/css/portal-min.css" %DEST
    
    build_package("Portal CSS", source_dir, config, output=compress_file,
        merger="simple", compressor="yui", type="css", debug=debug_file)


def build_portal_application(trial=False):
    
    source_dir = "../src/portal/js"
    for each_app in ["main", "view"]:
        config_file = "portal-%s.cfg" %each_app
        debug_file = "%s/js/portal-%s-debug.js" %(DEST, each_app)
        compress_file = "%s/js/portal-%s-min.js" %(DEST, each_app)
        build_package("Portal Application %s" %each_app, source_dir, config_file,
            output=compress_file, compressor="yui", debug=debug_file,
            trial_run=trial)


def build_err_console(trial=False):
    
    source_dir = "../src/portal/js"
    config_file = "err-console.cfg"
    debug_file = "%s/js/err-console-debug.js" %DEST
    compress_file = "%s/js/err-console-min.js" %DEST
    build_package("Error Console", source_dir, config_file, output=compress_file,
        compressor="yui", debug=debug_file, trial_run=trial)



if __name__ == '__main__':

    desc = """  These are the available modules that can be individually built:
    deploy_dirs, deploy_files, view_css, view_overrides, view_application,
    post_css, post_application, activity_css, activity_application theme_css,
    portal_css, portal_application, err_console
    """
    parser = optparse.OptionParser(description=desc)
    parser.add_option("-d", help="set an alternate destination, relative or absolute",
        dest="new_dest", action="store")
    parser.add_option("-c", help="make clean by removing old deployment",
        dest="clean", default=False, action="store_true")
    parser.add_option("-p", help="build a single package",
        dest="package", action="store")
    parser.add_option("-t", help="trial run of a single package only",
        dest="trial", default=False, action="store_true")
    (opts, args) = parser.parse_args()
    
    if opts.new_dest:
        DEST = opts.new_dest
        print "Setting new destination to %s\n" %opts.new_dest
    if opts.trial and not opts.package:
        parser.error("You must specify a package for the trial run")
    if opts.package:
        mod = __import__(__name__)
        if not hasattr(mod, "build_%s" %opts.package):
            parser.error("Unable to find %s package" %opts.package)
        getattr(mod, "build_%s" %opts.package)(opts.trial)
    else:
        if opts.clean:
            print "Removing old deployment first\n"
        # these are in the correct build order
        build_deploy_dirs(clean=opts.clean)
        build_view_css()
        build_view_overrides()
        build_view_application()
        build_post_css()
        build_post_application()
        build_activity_css()
        build_activity_application()
        build_theme_css()
        build_portal_css()
        build_portal_application()
        build_err_console()
        build_deploy_files()
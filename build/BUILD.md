# MASAS Portal Deployment Build Instructions

The following steps provide a guideline to installing the MASAS Portal capabilities. 
NOTE: Many steps are not well documented and require deep developer knowledge and intervention. Future versions of the build process should use a real automation framework.

## CONVENTIONS

The command line instructions here assume that the following folders are used, based on the git repository names:
* ~/projects/masas-activity
* ~/projects/masas-posting
* ~/projects/masas-viewing
* ~/projects/masas-x-portal
* ~/projects/ua_parser

If you used different folders, adjust the commands below accordingly.



### Posting & Viewing Tools - Build and Copy
- run builds of viewing and posting tools using the ircan-svn versions to
create the /deploy folders for each
#TODO: document commands 

- after builds are complete, copy /deploy/libs from the posting tool /deploy
folder to /static/libs

- run a diff -rbq on the posting tool /deploy/libs folder compared to the
viewing tool /deploy folders and copy over any additional files that are
specific to the viewing tool

### Activity Log (build & copy)
- run build of activity log and then copy over any additional files as well
* determine diffs 
	$ cd ~/projects/masas-activity/build
	$ python build.py
	$ diff -bq ~/projects/masas-activity/build ~/projects/masas-x-portal/build
* copy different items:
	cp -r ~/projects/masas-activity/deploy/libs ~/projects/masas-x-portal/static

### Clean Up (/deploy folders):
- done with the view/post/activity /deploy folders, they can be deleted

### Copy Posting tool source:
Copy the posting tool original source folders /js /img and /css to /src/post
    $ cp -r ~/projects/masas-posting/js ~/masas-x-portal/src/post/
    $ cp -r ~/projects/masas-posting/img ~/projects/masas-x-portal/src/post
    $ cp -r ~/projects/masas-posting/css ~/projects/masas-x-portal/src/post
### Copy Viewing tool source:
Copy the viewing tool original source folders /js /img and /css to /src/view
    $ cp -r ~/projects/masas-viewing/js ~/projects/masas-x-portal/src/view
    $ cp -r ~/projects/masas-viewing/img ~/projects/masas-x-portal/src/view
    $ cp -r ~/projects/masas-viewing/css ~/projects/masas-x-portal/src/view
### Copy Activity log source:
- copy the activity log original source folders /js /img and /css to /src/activity
	$ cp -r ~/projects/masas-activity/js ~/projects/masas-x-portal/src/activity/
	$ cp -r ~/projects/masas-activity/img ~/projects/masas-x-portal/src/activity/
	$ cp -r ~/projects/masas-activity/css ~/projects/masas-x-portal/src/activity/

### Copy Build Files (*.cfg for Posting, Viewing, and Activity)
* copy the posting tool specific /build/*.cfg files to /build
	$ cs ~/projects/masas-posting/build
    $ cp extjs-post.cfg geoext.cfg openlayers.cfg xmljson.cfg ~/projects/masas-x-portal/build
 
* copy the viewing tool specific /build/*.cfg files to /build
	$ cd ~/projects/masas-viewing/build
	$ cp extjs-view.cfg ~/projects/masas-x-portal/build/

* copy the activity log specific /build/*.cfg files to /build
	$ cp dateformat.cfg handlebars.cfg ~/projects/masas-x-portal/build

### NECESSARY???	
- clean up any .svn folders in /libs with
    find . -name ".svn" -exec rm -rf {} \;

### MANUAL STEPS
The Following manual steps are really up to the developer that makes modifications to the post, view, and activity tools. These instructions MUST be replaced with repeatable steps at some point in the future.

* make sure the build.py script is updated with any changes specific to post, view, activity, or ExtJS libs
* modify /src/post/js /src/view/js and /src/activity/js with any portal specific changes

### Update ua_parser (if necessary)
The User Agent parser (GIT: https://github.com/tobie/ua-parser.git) is used.
* is a good time to update the ua-parser regexes by downloading the latest updates and converting to JSON.  Follow steps in utils/ua-parser to load yaml in and dump json out

	$ cd ~/projects/ua_parser
	$ git pull
    $ rm -rf ~/projects/masas-x-portal/utils/ua_parser
    $ cp -r ~/projects/ua-parser/py/* ~/projects/masas-x-portal/utils/
    $ cp ~/projects/ua-parser/regexes.yaml ~/projects/masas-x-portal/utils/ua_parser/


#Running Build

- build.py
    $ cd ~/projects/masas-x-activity/build
    $ python build.py
    
    

Manual Steps

* DIRECTIVE FROM BUILD: 
    Files in /src/portal/img /src/post/img /src/view/img /src/activity/img should be manually copied into place and any conflicts resolved

- copy /src/portal/img files to /static/img, /src/post/img files to
/static/img, /src/view/img files to /static/img, and /src/activity/img files
to /static/img manually and resolve any conflicts
	$ cp -r ~/projects/masas-x-portal/src/portal/img ~/projects/masas-x-portal/static/img
	$ cp -r ~/projects/masas-x-portal/src/post/img ~/projects/masas-x-portal/static/img
	$ cp -r ~/projects/masas-x-portal/src/view/img ~/projects/masas-x-portal/static/img
	
	

#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:           MASAS Tools Portal v0.2 - Server
Author:         Jacob Westfall
Created:        Nov 01, 2011
Updated:        Jan 13, 2014
Copyright:      Copyright (c) 2011-2014 MASAS Contributors.  Published under the
                Clear BSD license.  See license.txt for the full text of the license.
Description:	This web app serves the MASAS portal and tools.
"""

import os, sys
import optparse
import time, datetime
import random
import urllib2
try:
    import json
except ImportError:
    import simplejson as json
import logging, logging.handlers
from collections import deque
import types

# see requirements.txt
import cherrypy

# portal modules
from libs import common
from libs import ajax
from utils import dbutils
from utils import xff_tool
from utils import user_agents
from utils import custom_log
from utils import custom_session
from utils import redis_session

# cheetah templates
from templates.login_page import login_page as login_page
from templates.suspended_page import suspended_page as suspended_page
from templates.banned_page import banned_page as banned_page
from templates.main_page import main_page as main_page
from templates.welcome_x_page import welcome_x_page as welcome_x_page
from templates.welcome_dev_page import welcome_dev_page as welcome_dev_page
from templates.view_window_page import view_window_page as view_window_page
from templates.view_frame_page import view_frame_page as view_frame_page
from templates.post_entry_page import post_entry_page as post_entry_page
from templates.post_cap_page import post_cap_page as post_cap_page
from templates.bulk_entry_page import bulk_entry_page as bulk_entry_page
from templates.activity_log_page import activity_log_page as activity_log_page
from templates.mobile_tool_page import mobile_tool_page as mobile_tool_page


# python 2.6 or higher required
if sys.version < "2.6":
    print "Python 2.6 or higher is required"
    sys.exit(1)


class Root(object):
    """ HTML server's / class """
    
    def __init__(self, settings={}):
        """ Setup common root attributes """
        
        self.portal_settings = settings
        self.abuse_ips = {}
        self.recent_logins = deque(maxlen=10)
        self.setup_auth_logging()
        
        # sub level instance for /ajax methods
        self.ajax = ajax.Ajax(settings)
    
    
    def setup_auth_logging(self):
        """ Setup the authentication log """
        
        self.auth_logger = logging.getLogger("authentication")
        self.auth_logger.setLevel(logging.DEBUG)
        # file warning setup
        try:
            tofile = logging.handlers.RotatingFileHandler(\
                self.portal_settings["auth_log_filename"], maxBytes=100000,
                backupCount=5)
        except:
            cherrypy.log.error("Auth logfile errors", context="APP",
                severity=logging.ERROR, traceback=True, request=False)
        #TODO: set at debug level if required
        tofile.setLevel(logging.WARNING)
        # use custom date format, removes milliseconds
        format1 = logging.Formatter("%(asctime)s %(message)s",
            datefmt='%Y-%m-%d %H:%M:%S')
        tofile.setFormatter(format1)
        self.auth_logger.addHandler(tofile)
    
    
    @cherrypy.expose
    def default(self, *args, **kwargs):
        
        # setting the severity here to ensure it will be posted to email/sentry
        # instead of just logged to a file like normal
        cherrypy.log.error("%s attempted to access %s" %(cherrypy.request.remote.ip,
            cherrypy.request.path_info), context="APP", severity=logging.WARNING,
            headers=True)
        raise cherrypy.HTTPError(404)
    
    
    @cherrypy.expose
    def index(self, *args, **kwargs):
        """ Show login form. A session ID will be created upon first access
         to track login attempts. """
        
        # check for an already logged in users and login abuses
        self.check_login_abuse()
        if "logged_in" in cherrypy.session and cherrypy.session["logged_in"]:
            raise cherrypy.HTTPRedirect("main")
        html_data = {}
        html_data["site_name"] = self.portal_settings["site_name"]
        html_data["site_slogan"] = self.portal_settings["site_slogan"]
        html_data["site_name_url"] = self.portal_settings["site_name_url"]
        html_data["resource_url"] = self.portal_settings["resource_url"]
        html_data["status_site_url"] = self.portal_settings["statusboard_site"]
        if "secret_error" in kwargs and kwargs["secret_error"]:
            html_data["secret_error"] = kwargs["secret_error"]
        if "abuse" in cherrypy.session:
            if cherrypy.session["abuse"] > 3:
                html_data["show_text_field"] = True
                if "secret_error" in html_data:
                    html_data["secret_error"] += " - Last Attempt"
        # determine whether to use the /src path for development versions of
        # JS and CSS instead of /static, shouldn't be used by production
        if "use_source" in self.portal_settings:
            html_data["use_source"] = True
            html_data["status_icon_url"] = "%s?t=%s" \
                %("/src/portal/img/current-status.png", time.time())
        else:
            # need to add a unique timestamp to the status icon URL so that
            # it isn't cached.  Doing that for the login page here, normally
            # done in javascript when present elsewhere
            html_data["status_icon_url"] = "%s/%s?t=%s" \
                %(self.portal_settings["resource_url"], "img/current-status.png",
                    time.time())
        # initial user agent config, mobile, tablet, or desktop (default)
        # auto detects for the user, then allows them to change if necessary
        user_agent = "desktop"
        try:
            agent_test = user_agents.parse(cherrypy.request.headers.get("User-Agent",
                ""))
            if agent_test.is_tablet:
                user_agent = "tablet"
            elif agent_test.is_mobile:
                user_agent = "mobile"
        except:
            cherrypy.log.error("User agent detection failed", context="APP",
                severity=logging.WARNING, traceback=True, headers=True)
        html_data["user_agent"] = user_agent
        
        return str(login_page(searchList=[html_data]))
    
    
    @cherrypy.expose
    def do_login(self, *args, **kwargs):
        """ Handles the login process, checking info and logging in """
        
        # check for an already logged in users and login abuses
        self.check_login_abuse()
        if "logged_in" in cherrypy.session and cherrypy.session["logged_in"]:
            raise cherrypy.HTTPRedirect("main")
        
        # test that secret is valid
        if "secret" not in kwargs:
            self.update_login_abuse()
            return self.index(secret_error="Access Code Missing")
        user_data = self.secret_check(kwargs["secret"])
        if not user_data:
            self.update_login_abuse()
            return self.index(secret_error="Access Code Invalid")
        
        # upon successful login, start with a new session id to prevent session
        # fixation attacks and reset abuse or any other existing value
        cherrypy.session.regenerate()
        cherrypy.session.clear()
        
        # set the session values
        sess_data = {"logged_in": True,
            "login_time": time.strftime("%a %H:%M"),
            "user_secret": kwargs["secret"],
            # used for temporary attachment storage
            "attached_files": [],
            "attached_size": 0,
            "original_files": [],
            "attachment_types": {"application/atom+xml;type=entry": 1048576},
            # used for temporary import and favorite storage
            "import_entries": [],
            "import_attachments": [],
            "favorite_entries": [],
        }
        sess_data.update(user_data)
        # development and testing override
        if "override_user_secret" in self.portal_settings:
            sess_data["user_secret"] = self.portal_settings["override_user_secret"]
        # use first value as default for now
        #TODO: if the user doesn't have access to the first mode value, should
        #      a lookup be used to set it properly, or continue to require them
        #      to "enable" that mode, for instance operational only.
        sess_data["mode"] = self.portal_settings["masas_modes_order"][0]
        # use device specific settings if provided
        if "device-type" in kwargs and \
        kwargs["device-type"] in ["desktop", "tablet", "mobile"]:
            sess_data["device-type"] = kwargs["device-type"]
        # instead of using the minified versions of JS files, use the debug
        # versions as any errors reported will be easier to find/debug
        if "version" in kwargs:
            if kwargs["version"] == "debug_tools":
                sess_data["debug_tools"] = True
            elif kwargs["version"] == "debug_all":
                sess_data["debug_tools"] = True
                sess_data["debug_all"] = True
        # force debug settings to allow for better testing of new deployments
        if "force_debug_tools" in self.portal_settings:
            sess_data["debug_tools"] = True
        
        cherrypy.session.update(sess_data)
        
        # keeping track of only recent logins for dashboard access
        self.recent_logins.append({"login_time": sess_data["login_time"],
            "name": user_data["name"]})
        # keep archive of all logins
        self.auth_logger.warning("login %s %s" %(user_data["id"],
            cherrypy.request.remote.ip))
        
        raise cherrypy.HTTPRedirect("main")
    
    
    @cherrypy.expose
    def suspended(self, *args, **kwargs):
        """ Show suspended page """
        
        html_data = {}
        html_data["site_name"] = self.portal_settings["site_name"]
        html_data["site_name_url"] = self.portal_settings["site_name_url"]
        
        return str(suspended_page(searchList=[html_data]))


    @cherrypy.expose
    def banned(self, *args, **kwargs):
        """ Show banned page """
        
        html_data = {}
        html_data["site_name"] = self.portal_settings["site_name"]
        html_data["site_name_url"] = self.portal_settings["site_name_url"]
        
        return str(banned_page(searchList=[html_data]))
    
    
    @cherrypy.expose
    def main(self, *args, **kwargs):
        """ Show main page """
        
        self.check_auth_html()
        html_data = {}
        html_data["site_name"] = self.portal_settings["site_name"]
        html_data["resource_url"] = self.portal_settings["resource_url"]
        html_data["account_name"] = common.addslashes(cherrypy.session["name"])
        html_data["welcome_url"] = self.portal_settings["welcome_url"]
        html_data["logout_url"] = self.portal_settings["logout_url"]
        html_data["status_site_url"] = self.portal_settings["statusboard_site"]
        if "bulk_tool_enable" in self.portal_settings:
            html_data["bulk_url"] = True
        if "activity_tool_enable" in self.portal_settings:
            html_data["activity_url"] = True
        if "flex_tool_url" in self.portal_settings:
            html_data["flex_url"] = self.portal_settings["flex_tool_url"]
        if "mobile_tool_url" in self.portal_settings:
            html_data["mobile_url"] = self.portal_settings["mobile_tool_url"]
        html_data.update(self.special_config())
        
        return str(main_page(searchList=[html_data]))
    
    
    @cherrypy.expose
    def welcome(self, *args, **kwargs):
        """ Show welcome page """
        
        self.check_auth_html()
        html_data = {}
        html_data["site_name"] = self.portal_settings["site_name"]
        html_data["resource_url"] = self.portal_settings["resource_url"]
        html_data["status_site_url"] = self.portal_settings["statusboard_site"]
        html_data["data_status_site_url"] = self.portal_settings["data_statusboard_site"]
        # determine whether to use the /src path for development versions of
        # JS and CSS instead of /static, shouldn't be used by production
        if "use_source" in self.portal_settings:
            html_data["use_source"] = True
            html_data["status_icon_url"] = "%s?t=%s" \
                %("/src/portal/img/current-status.png", time.time())
            html_data["data_status_icon_url"] = "%s?t=%s" \
                %("/src/portal/img/current-data-status.png", time.time())
        else:
            # need to add a unique timestamp to the status icon URL so that
            # it isn't cached.  Doing that for the static page here, normally
            # done in javascript when present elsewhere
            html_data["status_icon_url"] = "%s/%s?t=%s" \
                %(self.portal_settings["resource_url"], "img/current-status.png",
                    time.time())
            html_data["data_status_icon_url"] = "%s/%s?t=%s" \
                %(self.portal_settings["resource_url"], "img/current-data-status.png",
                    time.time())
        
        if "site" in kwargs and kwargs["site"] == "dev":
            return str(welcome_dev_page(searchList=[html_data]))
        else:
            return str(welcome_x_page(searchList=[html_data]))
    
    
    @cherrypy.expose
    def view_window(self, *args, **kwargs):
        """ Show viewing tool window """
        
        self.check_auth_html()
        html_data = {}
        html_data["site_name"] = self.portal_settings["site_name"]
        html_data["resource_url"] = self.portal_settings["resource_url"]
        html_data["current_mode"] = cherrypy.session["mode"]
        html_data["account_name"] = common.addslashes(cherrypy.session["name"])
        html_data["status_site_url"] = self.portal_settings["statusboard_site"]
        # when a new view window is spawned, check to see if the user has
        # posting access to any of the hubs defined for this mode
        allow_post = "false"
        user_hubs = dict([ (x["url"], x["post"]) for x in cherrypy.session["hubs"] ])
        mode_hubs = [ x["url"] for x in self.portal_settings["masas_modes"]\
            [cherrypy.session["mode"]]["hubs"] ]
        for hub in mode_hubs:
            if hub in user_hubs:
                if user_hubs[hub] == "Y":
                    allow_post = "true"
                    break
        html_data["allow_post"] = allow_post
        html_data.update(self.special_config())
        
        return str(view_window_page(searchList=[html_data]))
    
    
    @cherrypy.expose
    def view_frame(self, *args, **kwargs):
        """ Show viewing tool frame """
        
        self.check_auth_html()
        html_data = {}
        html_data["resource_url"] = self.portal_settings["resource_url"]
        html_data["icon_service_url"] = self.portal_settings["icon_service_url"]
        html_data["google_map_url"] = self.portal_settings["google_map_url"]
        html_data["current_mode"] = cherrypy.session["mode"]
        feed_list = []
        user_hubs = [ x["url"] for x in cherrypy.session["hubs"] ]
        for hub_count, each_hub in enumerate(self.portal_settings["masas_modes"]\
        [cherrypy.session["mode"]]["hubs"]):
            if each_hub["url"] in user_hubs:
                feed_temp = {"title": each_hub["title"],
                    # add for public feed on Hub
                    "url": each_hub["url"] + "/feed",
                    #TODO: currently always using the same secret
                    "secret": cherrypy.session["user_secret"],
                    # checks the hide secret box
                    "hideSecret": True,
                    # prevents users from accidentally modifying the values
                    "locked": True}
                if hub_count > 0:
                    # the first hub, or the primary, is always enabled but
                    # hubs that are listed after that start out disabled and
                    # the user needs to enable them for viewing, reduces
                    # clutter.
                    feed_temp["disabled"] = True
                feed_list.append(feed_temp)
        html_data["feed_list"] = json.dumps(feed_list)
        html_data["author_search_url"] = \
            self.portal_settings["auth_service_info"]["search"]
        # start out with the user's account, then add other defaults
        authors_list = [[cherrypy.session["uri"], cherrypy.session["name"],
            "Your Account"]]
        if "saved_authors_list" in self.portal_settings:
            authors_list.extend(self.portal_settings["saved_authors_list"])
        html_data["authors_list"] = json.dumps(authors_list)
        # set the session cookie path so static files don't have cookie overhead
        app_config = cherrypy.request.app.config.get("/", None)
        if app_config:
            session_path = app_config.get("tools.sessions.path", None)
            if session_path:
                html_data["session_path"] = session_path
        html_data.update(self.special_config())
        
        return str(view_frame_page(searchList=[html_data]))
    
    
    @cherrypy.expose
    def new_entry(self, *args, **kwargs):
        """ Show New Entry page """
        
        self.check_auth_html()
        html_data = self.entry_template_data()
        html_data["operation"] = "New"
        
        return str(post_entry_page(searchList=[html_data]))
    
    
    @cherrypy.expose
    def clone_entry(self, *args, **kwargs):
        """ Show Clone Entry page """
        
        self.check_auth_html()
        html_data = self.entry_template_data()
        html_data["operation"] = "Clone"
        
        return str(post_entry_page(searchList=[html_data]))
    
    
    @cherrypy.expose
    def update_entry(self, *args, **kwargs):
        """ Show Update Entry page """
        
        self.check_auth_html()
        html_data = self.entry_template_data()
        html_data["operation"] = "Update"
        
        return str(post_entry_page(searchList=[html_data]))
    
    
    @cherrypy.expose
    def new_cap(self, *args, **kwargs):
        """ Show New CAP page """
        
        self.check_auth_html()
        html_data = self.cap_template_data()
        html_data["operation"] = "New"
        
        return str(post_cap_page(searchList=[html_data]))    
    
    
    @cherrypy.expose
    def clone_cap(self, *args, **kwargs):
        """ Show Clone CAP page """
        
        self.check_auth_html()
        html_data = self.cap_template_data()
        html_data["operation"] = "Clone"
        
        return str(post_cap_page(searchList=[html_data]))
    
    
    @cherrypy.expose
    def update_cap(self, *args, **kwargs):
        """ Show Update CAP page """
        
        self.check_auth_html()
        html_data = self.cap_template_data()
        html_data["operation"] = "Update"
        
        return str(post_cap_page(searchList=[html_data]))
    
    
    @cherrypy.expose
    def bulk_entry(self, *args, **kwargs):
        """ Show Bulk Entry page """
        
        self.check_auth_html()
        html_data = self.entry_template_data()
        if "masas_hub_url" not in html_data:
            # not available because the user doesn't have posting
            # rights to the Hubs in this mode
            return "Not Available"
        
        # extend the feed_list to allow copying between modes
        if "feed_list" in html_data:
            feed_list = json.loads(html_data["feed_list"])
            # duplicate matching uses the url because the tools JS code
            # does as well
            dup_check = [ x["url"] for x in feed_list ]
        else:
            feed_list = []
            dup_check = []
        user_hubs = dict([ (x["url"], x["post"]) for x in cherrypy.session["hubs"] ])
        for each_mode in self.portal_settings["masas_modes"]:
            if each_mode == cherrypy.session["mode"]:
                # existing mode hubs have already been setup
                continue
            for each_hub in self.portal_settings["masas_modes"][each_mode]["hubs"]:
                if "%s/feed" %each_hub["url"] in dup_check:
                    # filter out Hubs available to multiple modes
                    continue
                # only hubs available to be posted by this user
                if each_hub["url"] in user_hubs:
                    if user_hubs[each_hub["url"]] == "Y":
                        feed_list.append({ "title": each_hub["title"],
                            # add for public feed on Hub
                            "url": "%s/feed" %each_hub["url"],
                            #TODO: currently always using the same secret
                            "secret": cherrypy.session["user_secret"] })
                        dup_check.append("%s/feed" %each_hub["url"])
        if len(feed_list) > 1:
            html_data["feed_list"] = json.dumps(feed_list)
        
        return str(bulk_entry_page(searchList=[html_data]))
    
    
    @cherrypy.expose
    def activity_log(self, *args, **kwargs):
        """ Show activity log window """
        
        self.check_auth_html()
        html_data = {}
        html_data["site_name"] = self.portal_settings["site_name"]
        html_data["resource_url"] = self.portal_settings["resource_url"]
        html_data["google_map_url"] = self.portal_settings["google_map_url"]
        html_data["google_static_map_url"] = self.portal_settings["google_static_map_url"]
        html_data["current_mode"] = cherrypy.session["mode"]
        html_data["account_name"] = common.addslashes(cherrypy.session["name"])
        html_data["masas_user_secret"] = cherrypy.session["user_secret"]
        feed_list = []
        feed_names = {}
        user_hubs = [ x["url"] for x in cherrypy.session["hubs"] ]
        for each_hub in self.portal_settings["masas_modes"]\
        [cherrypy.session["mode"]]["hubs"]:
            if each_hub["url"] in user_hubs:
                # add for public feed on Hub
                feed_list.append(each_hub["url"] + "/feed")
                feed_names[each_hub["url"] + "/feed"] = each_hub["title"]
        html_data["feed_list"] = json.dumps(feed_list)
        html_data["feed_names"] = json.dumps(feed_names)
        html_data.update(self.special_config())
        
        return str(activity_log_page(searchList=[html_data]))
    
    
    @cherrypy.expose
    def mobile_tool(self, *args, **kwargs):
        """ Show mobile tool window """
        
        #TODO: what to show if mobile tool isn't configured?
        
        self.check_auth_html()
        html_data = {}
        html_data["site_name"] = self.portal_settings["site_name"]
        html_data["resource_url"] = self.portal_settings["resource_url"]
        html_data["google_map_url"] = self.portal_settings["google_map_url"]
        html_data["current_mode"] = cherrypy.session["mode"]
        html_data["account_name"] = common.addslashes(cherrypy.session["name"])
        html_data["masas_user_uri"] = cherrypy.session["uri"]
        html_data["masas_user_secret"] = cherrypy.session["user_secret"]
        feed_list = []
        feed_names = []
        user_hubs = [ x["url"] for x in cherrypy.session["hubs"] ]
        for each_hub in self.portal_settings["masas_modes"]\
        [cherrypy.session["mode"]]["hubs"]:
            if each_hub["url"] in user_hubs:
                #TODO: check for read vs write
                feed_list.append(each_hub["url"])
                feed_names.append(each_hub["title"])
        #html_data["feed_list"] = json.dumps(feed_list)
        #html_data["feed_names"] = json.dumps(feed_names)
        # until multi-hub is supported, first hub only
        html_data["feed_url"] = feed_list[0]
        html_data["feed_name"] = feed_names[0]
        if "mobile_tool_url" in self.portal_settings:
            html_data["mobile_url"] = self.portal_settings["mobile_tool_url"]
            html_data["proxy_root_url"] = self.portal_settings["proxy_root_url"]
        html_data.update(self.special_config())
        
        return str(mobile_tool_page(searchList=[html_data]))
    
    
    @cherrypy.expose
    def logout(self, *args, **kwargs):
        """ Clears the session info and logs the user out """
        
        try:
            self.auth_logger.warning("logout %s %s" %(cherrypy.session["id"],
                cherrypy.session["login_time"]))
        except:
            pass
        # empty this session's data, then expire it on the client side, the
        # cherrypy session cleanup thread will delete it server side later
        cherrypy.session.clear()
        cherrypy.lib.sessions.expire()
        
        raise cherrypy.HTTPRedirect(self.portal_settings["logout_url"])
    
    
    @cherrypy.expose
    def session_stats(self, *args, **kwargs):
        """ Used by dashboard and other administrative applications to
         monitor what sessions are current/recent, JSON results protected with
         and access code. """
        
        if "admin_stats_code" not in self.portal_settings:
            # currently disabled
            raise cherrypy.HTTPError(501)
        if "code" not in kwargs:
            cherrypy.log.error("%s attempted to access session_stats" \
                %cherrypy.request.remote.ip, context="APP", severity=logging.ERROR,
                headers=True)
            raise cherrypy.HTTPError(401)
        if kwargs["code"] != self.portal_settings["admin_stats_code"]:
            cherrypy.log.error("%s attempted to access session_stats" \
                %cherrypy.request.remote.ip, context="APP", severity=logging.ERROR,
                headers=True)
            raise cherrypy.HTTPError(401)
        
        current_list = []
        for sess_id in cherrypy.session.get_all_keys():
            sess_data = cherrypy.session.direct_load(sess_id)
            if sess_data:
                if "logged_in" in sess_data:
                    current_list.append({"name": sess_data["name"],
                        "login_time": sess_data["login_time"]})
        
        self.cleanup_abuse_ips()
        abuse_list = [{"address": x} for x in self.abuse_ips]
        
        # deque's can't be serialized to JSON, so convert to list
        recent_list = [x for x in self.recent_logins]
        
        return json.dumps({"server_time": time.strftime("%H:%M"),
            "current": current_list, "abuse": abuse_list, "recent": recent_list})


## Private Methods


    def check_auth_html(self):
        """ Checks to see if the user is already logged in for HTML requests
        and redirects them to the login page if they are not """
        
        if "logged_in" not in cherrypy.session or \
        not cherrypy.session["logged_in"]:
            raise cherrypy.HTTPRedirect("index")
    
    
    def check_login_abuse(self):
        """ Checks for too many login attempts indicating abuse and
        sends them to the suspended or banned pages """
        
        # session based tracking
        if "abuse" in cherrypy.session:
            # cleanup old abuse values so the user can try again later
            self.cleanup_abuse_session()
            if cherrypy.session["abuse"] > 4:
                self.auth_logger.error("warning login session abuse %s" \
                    %cherrypy.request.remote.ip)
                raise cherrypy.HTTPRedirect("suspended")
        # IP based tracking
        if self.abuse_ips:
            # run cleanup first to remove old entries
            self.cleanup_abuse_ips()
            if cherrypy.request.remote.ip in self.abuse_ips:
                if self.abuse_ips[cherrypy.request.remote.ip]["count"] > 14:
                    if "logged" not in self.abuse_ips[cherrypy.request.remote.ip]:
                        self.auth_logger.error("warning login IP abuse %s" \
                            %cherrypy.request.remote.ip)
                        cherrypy.log.error("Login IP abuse %s" \
                            %cherrypy.request.remote.ip, context="APP",
                            severity=logging.WARNING, headers=True)
                        # log this only once to prevent flooding the logs by an
                        # abusive system
                        self.abuse_ips[cherrypy.request.remote.ip]["logged"] = True
                    raise cherrypy.HTTPRedirect("banned")


    def cleanup_abuse_session(self):
        """ Clean old abuse session values """
        
        current_time = time.time()
        if "abuse_time" in cherrypy.session:
            time_diff = current_time - cherrypy.session["abuse_time"]
            if time_diff > 3600:
                cherrypy.session["abuse"] = 0
                cherrypy.session["abuse_time"] = time.time()


    def cleanup_abuse_ips(self):
        """ Clean old abuse entries from the IP based tracking. """
        
        current_time = time.time()
        clean_list = []
        for entry in self.abuse_ips:
            time_diff = current_time - self.abuse_ips[entry]["time"]
            if time_diff > 3600:
                clean_list.append(entry)
        if clean_list:
            for clean_ip in clean_list:
                del self.abuse_ips[clean_ip]
    
    
    def update_login_abuse(self, count=None):
        """ Updates the login abuse counters and sets the most recent time
        that a failure attempt occurred """
        
        # can advance the abuse count faster if necessary
        if not count:
            count = 1
        # session based tracking applicable to individual users
        if "abuse" in cherrypy.session:
            cherrypy.session["abuse"] += count
        else:
            cherrypy.session["abuse"] = count
        cherrypy.session["abuse_time"] = time.time()
        # IP based tracking applicable to robot/system-wide attempts
        abuse_ip = cherrypy.request.remote.ip
        if abuse_ip in self.abuse_ips:
            self.abuse_ips[abuse_ip]["count"] += count
        else:
            self.abuse_ips[abuse_ip] = {"count": count}
        self.abuse_ips[abuse_ip]["time"] = time.time()
    
    
    def secret_check(self, secret):
        """ Check a Secret value by querying a remote User service """
        
        # development and testing override
        if "override_user_info" in self.portal_settings:
            return self.portal_settings["override_user_info"]
        try:
            query_url = "%s?secret=%s&admin=%s&query_secret=%s" \
                %(self.portal_settings["auth_service_info"]["url"],
                self.portal_settings["auth_service_info"]["secret"],
                self.portal_settings["auth_service_info"]["admin"],
                secret)
            request = urllib2.Request(query_url)
            response = urllib2.urlopen(request)
            json_result = response.read()
        except:
            cherrypy.log.error("Access Control Service Error", context="APP",
                severity=logging.ERROR, traceback=True, request=False)
            return False
        try:
            json_data = json.loads(json_result)
        except:
            cherrypy.log.error("Access Control JSON parse error", context="APP",
                severity=logging.ERROR, traceback=True, request=False)
            return False
        if json_data:
            return json_data
        else:
            return False
    
    
    def entry_template_data(self):
        """ Setup the standard input for Entry templates """
        
        html_data = {}
        html_data["site_name"] = self.portal_settings["site_name"]
        # system settings for this page
        html_data["google_map_url"] = self.portal_settings["google_map_url"]
        html_data["resource_url"] = self.portal_settings["resource_url"]
        html_data["icon_service_url"] = self.portal_settings["icon_service_url"]
        html_data["current_mode"] = cherrypy.session["mode"]
        html_data["account_name"] = common.addslashes(cherrypy.session["name"])
        html_data["masas_user_uri"] = cherrypy.session["uri"]
        if "groups" in cherrypy.session and cherrypy.session["groups"]:
            # string value suitable for Entry
            html_data["masas_user_group"] = " ".join(cherrypy.session["groups"])
        feed_list = []
        user_hubs = dict([ (x["url"], x["post"]) for x in cherrypy.session["hubs"] ])
        for each_hub in self.portal_settings["masas_modes"]\
        [cherrypy.session["mode"]]["hubs"]:
            # only hubs available to be posted by this user
            if each_hub["url"] in user_hubs:
                if user_hubs[each_hub["url"]] == "Y":
                    feed_list.append({ "title": each_hub["title"],
                        # add for public feed on Hub
                        "url": each_hub["url"] + "/feed",
                        #TODO: currently always using the same secret
                        "secret": cherrypy.session["user_secret"] })
        if feed_list:
            # set the first hub values as defaults
            html_data["masas_hub_url"] = feed_list[0]["url"]
            html_data["masas_user_secret"] = feed_list[0]["secret"]
            html_data["feed_list"] = json.dumps(feed_list)
        #TODO: configurable settings for multiple emails
        if "return_address" in self.portal_settings["email"] and \
        "email" in cherrypy.session:
            forward_emails = [cherrypy.session["email"]]
        else:
            forward_emails = None
        html_data["forward_emails"] = json.dumps(forward_emails)
        # set the session cookie path so static files don't have cookie overhead
        app_config = cherrypy.request.app.config.get("/", None)
        if app_config:
            session_path = app_config.get("tools.sessions.path", None)
            if session_path:
                html_data["session_path"] = session_path
        html_data.update(self.special_config())
        
        return html_data
    
    
    def cap_template_data(self):
        """ Setup the standard input for CAP templates """
        
        # common values, then CAP specific
        html_data = self.entry_template_data()
        # simple suffix of epoch time for now
        html_data["cap_identifier"] = "%s-%s" %(self.portal_settings["cap_ident"],
            int(time.time()) )
        html_data["cap_sendername"] = common.addslashes(cherrypy.session["name"])
        html_data["cap_sender"] = cherrypy.session["uri"]
        
        return html_data
    
    
    def special_config(self):
        """ Setup special configuration values """
        
        config = {}
        # determine whether to use the /src path for development versions of
        # JS and CSS instead of /static, shouldn't be used by production
        if "use_source" in self.portal_settings:
            config["use_source"] = True
        # instead of using the minified versions of JS files, use the debug
        # versions as any errors reported will be easier to find/debug
        if "debug_tools" in cherrypy.session:
            config["use_debug_tools"] = True
        if "debug_all" in cherrypy.session:
            config["use_debug_all"] = True
        # device specific options like touch controls
        if "device-type" in cherrypy.session and \
        cherrypy.session["device-type"] != "desktop":
            config["use_touch"] = True
        
        return config



def run():
    """ run method for standalone Portal server """
    
    usage = "usage: portal.py [-s] [-c] config"
    parser = optparse.OptionParser(usage)
    parser.add_option("-s", action="store_true", dest="standalone",
                      help="run as a standalone daemon")
    parser.add_option("-c", action="append", type="string", dest="config_file",
                      help="read config from this python module")
    (options, args) = parser.parse_args()
    
    # don't catch config exceptions, let them show on commandline
    app_config = {}
    if options.config_file:
        # currently uses the first string provided by options and can
        # be relative or absolute url to .py file
        execfile(options.config_file[0], {}, app_config)
    else:
        # try a default config
        execfile("config.py", {}, app_config)
    
    cherrypy.config.update(app_config["cherrypy_server"])
    
    # monkeypatch in a new method for handling errors so that its bound to
    # the global logging instance and will supply "self"
    cherrypy.log.error = types.MethodType(custom_log.error, cherrypy.log)
    
    file_log = logging.handlers.RotatingFileHandler(\
        app_config["portal"]["error_log_filename"], maxBytes=1000000, backupCount=1)
    file_log.setLevel(logging.DEBUG)
    file_log.setFormatter(logging.Formatter("%(asctime)s %(levelname)s %(message)s"))
    cherrypy.log.error_log.addHandler(file_log)
    
    new_app = cherrypy.tree.mount(Root(app_config["portal"]), "/",
        app_config["cherrypy_app"])
    
    # monkeypatch the application level logging instance too
    new_app.log.error = types.MethodType(custom_log.error, new_app.log)
    
    if options.standalone:
        print "Starting Cherrypy Daemon"
        cherrypy.config.update({"log.screen": False})
        cherrypy.process.plugins.Daemonizer(cherrypy.engine).subscribe()
        if hasattr(cherrypy.engine, "signal_handler"):
            cherrypy.engine.signal_handler.subscribe()
        if hasattr(cherrypy.engine, "console_control_handler"):
            cherrypy.engine.console_control_handler.subscribe()
    cherrypy.engine.start()
    cherrypy.engine.block()


if __name__ == '__main__':
    run()
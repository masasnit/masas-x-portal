/**
MASAS Portal - Console and Error Handling
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2013 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.

The handling stages for errors is: error -> try/catch -> window.onerror -> browser
This provides the opportunity to handle errors at the appropriate stage and if
that can't be done, it moves onto the next stage until ultimately, the least
desirable stage, the user's browser will deal with it.

On webpages using this module, ERR, ERR.DEBUG, and ERR.REPORT_URL (can be
optionally commented out to disable report uploads) should be declared first,
then this module should be loaded.  Any other javascript can be added
afterwards with compilation and runtime errors being reported.
*/

/*global ERR */

// used to record a history of actions taken by the user as reported by
// applications with debug logging
ERR.DEBUG_HISTORY = [];
// the number of most recent history records to keep
ERR.DEBUG_HISTORY_MAX = 20;

/* Initial console setup.  If the browser doesn't have a console or even if
   it does but we're not in debug mode, replace with this mock version instead */
if (!'console' in window || typeof console === 'undefined' || ERR.DEBUG === false) {
    // using empty functions for all console methods
    console = {
        log: function () {},
        debug: function () {},
        info: function () {},
        warn: function () {},
        error: function () {},
        userError: function (error) {
            alert(error);
        },
        assert: function () {},
        dir: function () {},
        dirxml: function () {},
        trace: function () {},
        group: function () {},
        groupEnd: function () {},
        time: function () {},
        timeEnd: function () {},
        profile: function () {},
        profileEnd: function () {},
        count: function () {}
    };
}

// store the existing methods so they can be re-used when we override
ERR.prevConsoleDebug = console.debug;
ERR.prevConsoleError = console.error;

/**
Replace the debug console method with one that stores a history of
debug logging and also allows for passing these debug messages onto
a browser's console if available.
*/
console.debug = function () {
    // convert to a real array
    var args = Array.prototype.slice.call(arguments);
    if (args.length === 0) {
        // nothing to do with no message
        return;
    }
    //TODO: handle objects inside the array better with this stringifying
    var msg_string = args.join(', ');
    // this debug history allows a review of what the user did prior to
    // an error occuring
    ERR.DEBUG_HISTORY.push(msg_string);
    if (ERR.DEBUG_HISTORY.length > ERR.DEBUG_HISTORY_MAX) {
        // prune the history keeping only recent records
        ERR.DEBUG_HISTORY.shift();
    }
    // also sending to other existing methods.  Using the proper args
    // array supports consoles that can't accept multiple arguments
    if (ERR.DEBUG) {
        if (typeof(ERR.prevConsoleDebug) === 'function') {
            try {
                if (args.length === 1) {
                    ERR.prevConsoleDebug(args[0]);
                } else {
                    ERR.prevConsoleDebug(args);
                }
            } catch (e) {
                // browser doesn't have a proper debug method or doesn't
                // support multiple arguments so use simplest method
                // available and give up if this fails
                try {
                    console.log(args[0]);
                } catch (e) { }
            }
        }
    }
};

/**
Replace the error console method with one that submits the error to a
reporting location and also allows for passing these error messages onto
a browser's console if available.
*/
console.error = function () {
    // convert to a real array
    var args = Array.prototype.slice.call(arguments);
    if (args.length === 0) {
        // nothing to do with no message
        return;
    }
    //TODO: handle objects inside the array better with this stringifying
    var msg_string = args.join(', ');
    ERR.report_error(msg_string);
    // also sending to other existing methods.  Using the proper args
    // array supports consoles that can't accept multiple arguments
    if (ERR.DEBUG) {
        if (typeof(ERR.prevConsoleError) === 'function') {
            try {
                if (args.length === 1) {
                    ERR.prevConsoleError(args[0]);
                } else {
                    ERR.prevConsoleError(args);
                }
            } catch (e) {
                // browser doesn't have a proper error method or doesn't
                // support multiple arguments so use simplest method
                // available and give up if this fails
                try {
                    console.log(args[0]);
                } catch (e) { }
            }
        }
    }
};

/**
Send the error report to a reporting location.

@param {String} - the error message
@param {String} - the URL of the file in which this error occurred
@param {Integer} - the line number on which the error occurred
*/
ERR.report_error = function (msg, file, line) {
    var report_params = '?msg=' + encodeURIComponent(msg) +
        '&file=' + encodeURIComponent(file) +
        '&line=' + encodeURIComponent(line);
    // send a recent history of debugging actions
    if (ERR.DEBUG_HISTORY.length > 0) {
        var history_string = ERR.DEBUG_HISTORY.join('|');
        report_params += '&history=' + encodeURIComponent(history_string);
    }
    // send a stack trace if supported by the browser
    try {
        var report_stack = new Error().stack;
        if (report_stack) {
            report_params += '&stack=' + encodeURIComponent(report_stack);
        }
    } catch (e) { }
    // using an anonymous img tag to send the data instead of xhr as it
    // resolves same-origin and xhr timeout/transmission error problems.
    if (ERR.REPORT_URL) {
        var img = new Image();
        img.src = ERR.REPORT_URL + report_params;
    }
};

// store any existing error handler for this page
ERR.prevErrorHandler = window.onerror;

/**
Setup the browser's generic error handler so that it reports the error to a
reporting location and may also pass it along to an existing error handler.

Browsers supporting this: Chrome 13+ Firefox 6.0+ Internet Explorer 5.5+
Opera 11.60+ Safari 5.1+

NOTE: If the script URL has a different origin to the document then the
three arguments returned by window.onerror are always 'Script error.', '', 0

@param {String} - the error message
@param {String} - the URL of the file in which this error occurred
@param {Integer} - the line number on which the error occurred
*/
window.onerror = function (msg, file, line) {
	ERR.report_error(msg, file, line);
	if (typeof(ERR.prevErrorHandler) === 'function') {
	    // also sending to previous handler
	    ERR.prevErrorHandler(msg, file, line);
	}
	// true is returned in normal operation to indicate to the browser
	// that the error was handled and is therefore hidden from the user.
	// However when in debug mode, this error should bubble up to the
	// browser so the user knows that something did in fact occur
	return (ERR.DEBUG) ? false : true;
};

/**
 Used to test error handling
 */
ERR.throw_test_error = function () {
    ERR.report_error('Test Error');
};

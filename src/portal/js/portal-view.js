/**
MASAS Portal - Viewing Tool Window
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.
*/

/*global MASAS,Ext */
Ext.namespace('MASAS.Portal');

// to keep track of all spawned tool windows
MASAS.Portal.TOOL_WINDOWS = [];

Ext.onReady(function () {

    // turn on validation errors beside the field globally
    Ext.form.Field.prototype.msgTarget = 'side';
    // Tooltips on
    Ext.QuickTips.init();

    var portalBox = new Ext.Panel({
        region: 'north',
        height: 30,
        border: false,
        layout: 'hbox',
        layoutConfig: { align: 'stretch', pack: 'start' },
        items: [{
            html: '<div class="headerBox"><span class="headerTitle">' +
                MASAS.Portal.SITE_NAME + '&nbsp;&nbsp; - &nbsp;&nbsp;' +
                MASAS.Portal.CURRENT_MODE + ' View</span><span class="headerDetail">Account: ' +
                MASAS.Portal.ACCOUNT_NAME + '</span></div>',
            bodyStyle: { 'background-color': 'white' },
            flex: 1
        }, {
            html: '<div class="statusIndicator" style="visibility: hidden"><a href="' + MASAS.Portal.STATUS_SITE_URL +
                '" target="_blank" title="Open Statusboard">Status: <img src="' +
                MASAS.Portal.STATUS_ICON_URL + '?t=' + (new Date()).getTime() +
                '" id="statusIcon" alt="N/A" height="16" width="16"></a></div>'
        }, new Ext.Button({
            iconCls: 'closeBtn',
            width: 110,
            text: '<b>Close Window</b>',
            tooltip: 'Close this Window',
            handler: function () {
                window.close();
            }
        }) ]
    });

    var contentPanel = new Ext.Panel({
        region: 'center',
        //bodyStyle: 'padding: 10px;',
        defaults: { border: false, autoScroll: true },
        items: [{
            xtype: 'component',
            id: 'iframe-win',
            autoEl: {
                tag: 'iframe',
                height: '100%',
                width: '100%',
                overflow: 'auto',
                frameborder: 0,
                src: MASAS.Portal.IFRAME_START
            }
        }]
    });

    // main layout view, uses entire browser viewport
    var mainView = new Ext.Viewport({
        layout: 'border',
        items: [ portalBox, contentPanel ]
    });


    var toolsWindow = new Ext.Window({
        title: 'Posting Tool',
        id: 'tools-window',
        closable: false,
        collapsed: true,
        expandOnShow: false,
        collapsible: true,
        titleCollapse: true,
        animCollapse: false,
        draggable: false,
        width: 280,
        height: 175,
        //autoScroll: true,
        bodyCssClass: 'toolsBodyColor',
        layout: 'table',
        layoutConfig: { columns: 2 },
        defaults: { border: false, style: { padding: '10px' } },
        items: [{
            html: '&nbsp;&nbsp;&nbsp; Post &nbsp;&nbsp;&nbsp;',
            cellCls: 'titleCell',
            style: { padding: '0px', 'text-align': 'center' },
            bodyCssClass: 'toolsBodyColor',
            colspan: 2
        // post buttons same as in portal-main
        }, new Ext.SplitButton({
            text: '<b>New Entry</b>',
            tooltip: 'Create a New Entry',
            id: 'new-entry-button',
            bodyCssClass: 'toolsBodyColor',
            scale: 'medium',
            iconCls: 'entryBtn',
            iconAlign: 'left',
            //disabled: true,
            menu: { items: [{
                text: 'Clone Previous Entry',
                handler: function () {
                    MASAS.Portal.TOOL_WINDOWS.push(window.open(MASAS.Portal.CLONE_ENTRY_URL));
                }
            }] },
            arrowAlign: 'bottom',
            arrowTooltip: 'Additional options for a New Entry',
            handler: function () {
                var new_win = window.open(MASAS.Portal.NEW_ENTRY_URL);
                // passing along the current map view.  Using a global variable
                // instead of a MASAS. because there may be execution delays
                // preventing it from being available at this point
                if (MASAS.MAPVIEW) {
                    new_win.MAPVIEW = MASAS.MAPVIEW;
                }
                MASAS.Portal.TOOL_WINDOWS.push(new_win);
            },
            cellCls: 'newBtnCell'
        }), new Ext.SplitButton({
            text: '<b>New Alert</b>',
            tooltip: 'Create a New Alert',
            id: 'new-cap-button',
            bodyCssClass: 'toolsBodyColor',
            scale: 'medium',
            iconCls: 'alertBtn',
            iconAlign: 'left',
            //disabled: true,
            menu: { items: [{
                text: 'Clone Previous Alert',
                handler: function () {
                    MASAS.Portal.TOOL_WINDOWS.push(window.open(MASAS.Portal.CLONE_ALERT_URL));
                }
            }] },
            arrowAlign: 'bottom',
            arrowTooltip: 'Additional options for a New Alert',
            handler: function () {
                var new_win = window.open(MASAS.Portal.NEW_ALERT_URL);
                if (MASAS.MAPVIEW) {
                    new_win.MAPVIEW = MASAS.MAPVIEW;
                }
                MASAS.Portal.TOOL_WINDOWS.push(new_win);
            },
            cellCls: 'newBtnCell'
        }), new Ext.Button({
            text: '<b>Update Entry</b>',
            tooltip: 'Update an Existing Entry',
            id: 'update-entry-button',
            bodyCssClass: 'toolsBodyColor',
            scale: 'medium',
            iconCls: 'entryBtn',
            iconAlign: 'left',
            //disabled: true,
            handler: function () {
                MASAS.Portal.TOOL_WINDOWS.push(window.open(MASAS.Portal.UPDATE_ENTRY_URL));
            }
        }), new Ext.Button({
            text: '<b>Update Alert</b>',
            tooltip: 'Update or Cancel an Alert',
            id: 'update-cap-button',
            bodyCssClass: 'toolsBodyColor',
            scale: 'medium',
            iconCls: 'alertBtn',
            iconAlign: 'left',
            //disabled: true,
            handler: function () {
                MASAS.Portal.TOOL_WINDOWS.push(window.open(MASAS.Portal.UPDATE_ALERT_URL));
            }
        }) ],
        listeners: {
            beforecollapse: function () {
                // shrink the amount of space the title bar takes up in the header
                this.setWidth(125);
                // moving the title bar over as well, helps to resolve cases where
                // it doesn't resize properly when the window is changed as it will
                // recalculate now.  Assuming 210 for status and logout boxes, 125
                // for the window width and then 15 more for extra spacing
                var new_x = mainView.getBox().width - 350;
                this.setPagePosition(new_x, this.y);
            },
            beforeexpand: function () {
                // expand the title bar back to normal size
                this.setWidth(280);
                // 280 for window width, other values same as beforecallapse
                var new_x = mainView.getBox().width - 505;
                this.setPagePosition(new_x, this.y);
            }
        }
    });

    // using the beforecollapse calcuations for x
    toolsWindow.x = mainView.getBox().width - 350;
    // fitting the toolbar of the window just inside the header
    toolsWindow.y = 3;
    // IE 8 and below has a problem with setting the window size when its
    // collapsed during initial load, it only shows the title bar, so
    // loading as expanded
    if (Ext.isIE6 || Ext.isIE7 || Ext.isIE8) {
        toolsWindow.collapsed = false;
        // using beforeexpand calculations for x
        toolsWindow.x = mainView.getBox().width - 505;
    }
    toolsWindow.show(this);
    
    if (MASAS.Portal.POSTING_RIGHTS == false) {
        Ext.getCmp('new-entry-button').disable();
        Ext.getCmp('new-cap-button').disable();
        Ext.getCmp('update-entry-button').disable();
        Ext.getCmp('update-cap-button').disable();
    }
    
    
    /**
     * Regularily updates the status icon
     */
    Ext.TaskMgr.start({
        run: function () {
            // refreshing the status icon in case it has changed
            document.getElementById('statusIcon').src = MASAS.Portal.STATUS_ICON_URL +
                '?t=' + (new Date()).getTime();
        },
        // every 30 minutes
        interval: 1800000
    });
    
    
    /**
     * Close any open (forgotten?) Tool windows when leaving this window
     */
    function closeToolWindows() {
        for (var i = 0; i < MASAS.Portal.TOOL_WINDOWS.length; i++) {
            if (!MASAS.Portal.TOOL_WINDOWS[i].closed) {
                MASAS.Portal.TOOL_WINDOWS[i].close();
            }
        }
    }
    window.onunload = closeToolWindows;
    
});

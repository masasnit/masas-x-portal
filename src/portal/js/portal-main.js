/**
MASAS Portal - Main Window
Updated: Jan 13, 2014
Independent Joint Copyright (c) 2011-2014 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.
*/

/*global MASAS,Ext,swfobject */
Ext.namespace('MASAS.Portal');

// to keep track of all spawned tool windows
MASAS.Portal.TOOL_WINDOWS = [];

Ext.onReady(function () {

    // turn on validation errors beside the field globally
    Ext.form.Field.prototype.msgTarget = 'side';
    // tooltips on
    Ext.QuickTips.init();

    var portalBox = new Ext.Panel({
        region: 'north',
        height: 30,
        border: false,
        layout: 'hbox',
        layoutConfig: { align: 'stretch', pack: 'start' },
        items: [{
            html: '<div class="headerBox"><span class="headerTitle">' +
                '<a onclick="MASAS.Portal.confirm_main_return();">' +
                MASAS.Portal.SITE_NAME + '</a></span><span class="headerDetail">Account: ' +
                MASAS.Portal.ACCOUNT_NAME + '</span></div>',
            bodyStyle: { 'background-color': 'white' },
            flex: 1
        }, {
            html: '<div class="statusIndicator" style="visibility: hidden"><a href="' + MASAS.Portal.STATUS_SITE_URL +
                '" target="_blank" title="Open Statusboard">Status: <img src="' +
                MASAS.Portal.STATUS_ICON_URL + '?t=' + (new Date()).getTime() +
                '" id="statusIcon" alt="N/A" height="16" width="16"></a></div>'
        }, new Ext.Button({
            iconCls: 'logoutBtn',
            text: '<b>Logout</b>',
            tooltip: 'Log Out',
            id: 'logout-button',
            width: 75,
            goLogout: false,
            handler: function () {
                this.goLogout = true;
                window.location.href = MASAS.Portal.LOGOUT_URL;
            }
        }) ]
    });

    var contentPanel = new Ext.Panel({
        region: 'center',
        //bodyStyle: 'padding: 10px;',
        defaults: { border: false, autoScroll: true },
        items: [{
            xtype: 'component',
            id: 'iframe-win',
            autoEl: {
                tag: 'iframe',
                height: '100%',
                width: '100%',
                overflow: 'auto',
                frameborder: 0,
                src: MASAS.Portal.IFRAME_START
            }
        }]
    });

    // main layout view, uses entire browser viewport
    var mainView = new Ext.Viewport({
        layout: 'border',
        id: 'main-test',
        items: [ portalBox, contentPanel ]
    });

    
    // switching between modes
    var modeCombo = new Ext.form.ComboBox({
        fieldLabel: '<span style="font-size: 130%; font-weight: bold;">Mode</span>',
        name: 'mode-switch',
        id: 'mode-switch',
        allowBlank: false,
        // by not allowing someone to type a value in here, it makes selection
        // on a tablet device much easier
        editable: false,
        width: 100,
        forceSelection: true,
        triggerAction: 'all',
        mode: 'local',
        displayField: 'name',
        store: new Ext.data.Store({
            proxy: new Ext.data.HttpProxy({ url: MASAS.Portal.MODE_LIST_URL }),
            reader: new Ext.data.JsonReader({
                root: 'modes',
                fields: ['name', 'post', 'selected']
            }),
            autoLoad: true,
            listeners: { 
                exception: function () {
                    alert('Unable to load Modes');
                },
                load: function (store, records) {
                    var disable_buttons = false;
                    if (records.length > 0) {
                        var select_index = store.find('selected', true);
                        if (select_index != -1) {
                            var select_data = records[select_index].data;
                            Ext.getCmp('tools-window').setTitle('Tools - Mode: ' +
                                select_data.name);
                            Ext.getCmp('mode-switch').setValue(select_data.name);
                            Ext.getCmp('view-tool-button').enable();
                            if (select_data.post == true) {
                                Ext.getCmp('new-entry-button').enable();
                                Ext.getCmp('new-cap-button').enable();
                                Ext.getCmp('update-entry-button').enable();
                                Ext.getCmp('update-cap-button').enable();
                            } else {
                                Ext.getCmp('new-entry-button').disable();
                                Ext.getCmp('new-cap-button').disable();
                                Ext.getCmp('update-entry-button').disable();
                                Ext.getCmp('update-cap-button').disable();
                            }
                        } else {
                            disable_buttons = true;
                        }
                    } else {
                        disable_buttons = true;
                    }
                    if (disable_buttons) {
                        // account has no rights to any Hub
                        Ext.getCmp('view-tool-button').disable();
                        Ext.getCmp('new-entry-button').disable();
                        Ext.getCmp('new-cap-button').disable();
                        Ext.getCmp('update-entry-button').disable();
                        Ext.getCmp('update-cap-button').disable();
                    }
                }
            }
        }),
        listeners: {
            select: function (combo, record) {
                Ext.Ajax.request({
                    url: MASAS.Portal.MODE_SET_URL + '?mode=' + record.data.name,
                    failure: function () {
                        alert('Unable to set Mode');
                    }
                });
                // changing the style sheet to match the mode, Training is ext-all
                // as the base, with Operational and Exercise overriding some of it
                Ext.util.CSS.removeStyleSheet('modeStyle');
                if (record.data.name === 'Operational' || record.data.name === 'Exercise') {
                    // using Ext.util.CSS.swapStyleSheet() as example
                    var doc = document;
                    var ss = doc.createElement('link');
                    ss.setAttribute('rel', 'stylesheet');
                    ss.setAttribute('type', 'text/css');
                    ss.setAttribute('id', 'modeStyle');
                    if (record.data.name === 'Operational') {
                        ss.setAttribute('href', MASAS.Portal.OPERATIONAL_CSS_URL);
                    } else {
                        ss.setAttribute('href', MASAS.Portal.EXERCISE_CSS_URL);
                    }
                    doc.getElementsByTagName('head')[0].appendChild(ss);
                }
                Ext.getCmp('tools-window').setTitle('Tools - Mode: ' +
                    record.data.name);
                Ext.getCmp('view-tool-button').enable();
                if (record.data.post == true) {
                    Ext.getCmp('new-entry-button').enable();
                    Ext.getCmp('new-cap-button').enable();
                    Ext.getCmp('update-entry-button').enable();
                    Ext.getCmp('update-cap-button').enable();
                } else {
                    Ext.getCmp('new-entry-button').disable();
                    Ext.getCmp('new-cap-button').disable();
                    Ext.getCmp('update-entry-button').disable();
                    Ext.getCmp('update-cap-button').disable();
                }
                // reload in new mode if viewing in current window
                var iframe_check = new RegExp(MASAS.Portal.CURRENT_VIEW_URL, 'g');
                if (Ext.getDom('iframe-win').src.search(iframe_check) != -1) {
                    Ext.getDom('iframe-win').src = MASAS.Portal.CURRENT_VIEW_URL;
                }
                // warn about any tools left in the previous mode
                for (var i = 0; i < MASAS.Portal.TOOL_WINDOWS.length; i++) {
                    if (!MASAS.Portal.TOOL_WINDOWS[i].closed) {
                        alert('There are browser windows open using Tools in the' +
                           ' previous Mode, please close them.');
                        break;
                    }
                }
            }
        }
    });
    
    
    // add other tool buttons based on what tools are available
    // and can be utilized by this user
    var other_buttons = [];
    if (MASAS.Portal.FLEX_TOOL_URL) {
        // confirm that Flash is available and the right version
        var flash_info = swfobject.getFlashPlayerVersion();
        if (flash_info.major >= 11 && flash_info.minor >= 1) {
            other_buttons.push({
                text: 'Flex Viewer In ' + ((Ext.isIE) ? 'New' : 'Current') + ' Window',
                handler: function () {
                    if (Ext.isIE) {
                        // Workaround for IE 8 and 9 which have problems with the
                        // tools window drop-down from the parent frame not
                        // working with the Flex Viewer in an iframe
                        MASAS.Portal.TOOL_WINDOWS.push(window.open(MASAS.Portal.FLEX_TOOL_URL));
                    } else {
                        Ext.getDom('iframe-win').src = MASAS.Portal.FLEX_TOOL_URL;
                        // auto collapse the tools window the first time its loaded
                        // to simplify the interface
                        if (toolsWindow.firstLoadCollapse) {
                            toolsWindow.collapse();
                            toolsWindow.firstLoadCollapse = false;
                        }
                    }
                }
            });
        }
    }
    if (MASAS.Portal.BULK_ENTRY_URL) {
        other_buttons.push({
            text: 'Bulk Entry Tool In New Window',
            handler: function () {
                MASAS.Portal.TOOL_WINDOWS.push(window.open(MASAS.Portal.BULK_ENTRY_URL));
            }
        });
    }
    if (MASAS.Portal.ACTIVITY_LOG_URL) {
        other_buttons.push({
            text: 'Activity Log In New Window',
            handler: function () {
                MASAS.Portal.TOOL_WINDOWS.push(window.open(MASAS.Portal.ACTIVITY_LOG_URL));
            }
        });
    }
    if (MASAS.Portal.MOBILE_TOOL_URL) {
        other_buttons.push({
            text: 'Mobile Tool In New Window',
            handler: function () {
                MASAS.Portal.TOOL_WINDOWS.push(window.open(MASAS.Portal.MOBILE_TOOL_URL));
            }
        });
    }
    

    var toolsWindow = new Ext.Window({
        title: 'Tools - Mode: ',
        id: 'tools-window',
        closable: false,
        collapsible: true,
        titleCollapse: true,
        // used to auto collapse the window to simplfy the interface
        firstLoadCollapse: true,
        animCollapse: false,
        draggable: false,
        width: 410,
        height: 225,
        //autoScroll: true,
        bodyCssClass: 'toolsBodyColor',
        layout: 'table',
        layoutConfig: { columns: 3 },
        defaults: { border: false, style: { padding: '10px' } },
        items: [ new Ext.FormPanel({
            labelWidth: 50,
            items: [ modeCombo ],
            // in addition to default style for this cell
            cellCls: 'modeCell',
            bodyCssClass: 'toolsBodyColor',
            colspan: 3
        }), {
            id: 'view-title',
            html: '&nbsp;&nbsp;&nbsp; View &nbsp;&nbsp;&nbsp;',
            cellCls: 'titleCell',
            // overriding defaults for style
            style: { padding: '0px', 'text-align': 'center' },
            bodyCssClass: 'toolsBodyColor'
        }, {
            html: '&nbsp;&nbsp;&nbsp; Post &nbsp;&nbsp;&nbsp;',
            cellCls: 'titleCell',
            style: { padding: '0px', 'text-align': 'center' },
            bodyCssClass: 'toolsBodyColor',
            colspan: 2
        }, new Ext.SplitButton({
            text: '<b>Launch In <br>Current Window</b>',
            tooltip: 'Launch the View Tool in the Current Browser Window',
            id: 'view-tool-button',
            bodyCssClass: 'toolsBodyColor',
            scale: 'large',
            iconCls: 'viewBtn',
            iconAlign: 'left',
            //enableToggle: true,
            menu: { items: [{
                text: 'View Tool In New Window',
                handler: function () {
                    MASAS.Portal.TOOL_WINDOWS.push(window.open(MASAS.Portal.NEW_VIEW_URL));
                }
            }] },
            arrowAlign: 'bottom',
            handler: function () {
                Ext.getDom('iframe-win').src = MASAS.Portal.CURRENT_VIEW_URL;
                // auto collapse the tools window the first time its loaded
                // to simplify the interface
                if (toolsWindow.firstLoadCollapse) {
                    toolsWindow.collapse();
                    toolsWindow.firstLoadCollapse = false;
                }
            }
        // post buttons same as in portal-view
        }), new Ext.SplitButton({
            text: '<b>New Entry</b>',
            tooltip: 'Create a New Entry',
            id: 'new-entry-button',
            cellCls: 'newBtnCell',
            bodyCssClass: 'toolsBodyColor',
            scale: 'medium',
            iconCls: 'entryBtn',
            iconAlign: 'left',
            //disabled: true,
            menu: { items: [{
                text: 'Clone Previous Entry',
                handler: function () {
                    MASAS.Portal.TOOL_WINDOWS.push(window.open(MASAS.Portal.CLONE_ENTRY_URL));
                }
            }] },
            arrowAlign: 'bottom',
            arrowTooltip: 'Additional options for a New Entry',
            handler: function () {
                var new_win = window.open(MASAS.Portal.NEW_ENTRY_URL);
                // passing along the current map view.  Using a global variable
                // instead of a MASAS. because there may be execution delays
                // preventing it from being available at this point
                if (MASAS.MAPVIEW) {
                    new_win.MAPVIEW = MASAS.MAPVIEW;
                }
                MASAS.Portal.TOOL_WINDOWS.push(new_win);
            }
        }), new Ext.SplitButton({
            text: '<b>New Alert</b>',
            tooltip: 'Create a New Alert',
            id: 'new-cap-button',
            cellCls: 'newBtnCell',
            bodyCssClass: 'toolsBodyColor',
            scale: 'medium',
            iconCls: 'alertBtn',
            iconAlign: 'left',
            //disabled: true,
            menu: { items: [{
                text: 'Clone Previous Alert',
                handler: function () {
                    MASAS.Portal.TOOL_WINDOWS.push(window.open(MASAS.Portal.CLONE_ALERT_URL));
                }
            }] },
            arrowAlign: 'bottom',
            arrowTooltip: 'Additional options for a New Alert',
            handler: function () {
                var new_win = window.open(MASAS.Portal.NEW_ALERT_URL);
                if (MASAS.MAPVIEW) {
                    new_win.MAPVIEW = MASAS.MAPVIEW;
                }
                MASAS.Portal.TOOL_WINDOWS.push(new_win);
            }
        }), new Ext.Button({
            text: '<b>Other Tools</b>',
            tooltip: 'Open Other Tools',
            id: 'other-tools-button',
            cellCls: 'otherBtnCell',
            bodyCssClass: 'toolsBodyColor',
            scale: 'small',
            enableToggle: false,
            disabled: (other_buttons.length > 0) ? false : true,
            menu: { items: other_buttons }
        }), new Ext.Button({
            text: '<b>Update Entry</b>',
            tooltip: 'Update an Existing Entry',
            id: 'update-entry-button',
            bodyCssClass: 'toolsBodyColor',
            scale: 'medium',
            iconCls: 'entryBtn',
            iconAlign: 'left',
            //disabled: true,
            handler: function () {
                MASAS.Portal.TOOL_WINDOWS.push(window.open(MASAS.Portal.UPDATE_ENTRY_URL));
            }
        }), new Ext.Button({
            text: '<b>Update Alert</b>',
            tooltip: 'Update or Cancel an Alert',
            id: 'update-cap-button',
            bodyCssClass: 'toolsBodyColor',
            scale: 'medium',
            iconCls: 'alertBtn',
            iconAlign: 'left',
            //disabled: true,
            handler: function () {
                MASAS.Portal.TOOL_WINDOWS.push(window.open(MASAS.Portal.UPDATE_ALERT_URL));
            }
        }) ],
        listeners: {
            beforecollapse: function () {
                // shrink the amount of space the title bar takes up in the header
                this.setWidth(200);
                // moving the title bar over as well, helps to resolve cases where
                // it doesn't resize properly when the window is changed as it will
                // recalculate now.  Assuming 175 for status and logout boxes, 200
                // for the window width and then 15 more for extra spacing
                var new_x = mainView.getBox().width - 390;
                this.setPagePosition(new_x, this.y);
            },
            beforeexpand: function () {
                // expand the title bar back to normal size
                this.setWidth(410);
                // 410 for window width, other values same as beforecallapse
                var new_x = mainView.getBox().width - 600;
                this.setPagePosition(new_x, this.y);
            }
        }
    });

    // can cover the status box when first expanded so using just the logout
    // box width of 75, normal window width, plus 15 more for extra spacing
    toolsWindow.x = mainView.getBox().width - 500;
    // fitting the toolbar of the window just inside the header
    toolsWindow.y = 3;
    toolsWindow.show(this);
    
    
    /**
     * Regularily checks to ensure the user remains logged in and updates the
     * status icon
     */
    function checkLoginStatus() {
        Ext.Ajax.request({
            url: MASAS.Portal.LOGIN_STATUS_URL,
            failure: function () {
                var auto_logout = confirm('Your login session seems to have ' +
                    'expired.  Do you want to Logout?');
                if (auto_logout) {
                    Ext.getCmp('logout-button').goLogout = true;
                    window.location.href = MASAS.Portal.LOGOUT_URL;
                }
            }
        });
        // refreshing the status icon in case it has changed
        document.getElementById('statusIcon').src = MASAS.Portal.STATUS_ICON_URL +
            '?t=' + (new Date()).getTime();
    }
    Ext.TaskMgr.start({
        run: checkLoginStatus,
        // every 30 minutes
        interval: 1800000
    });
    
    
    /**
     * Confirm that a user has not click reload or closed a window by accident
     * as this would disrupt their current Tool session.  NOTE: this doesn't
     * work on most tablet devices.
     */
    function confirmWindowClose() {
        if (Ext.getCmp('logout-button').goLogout == false) {
            return 'Leaving this page will close all open MASAS Tools.  You must ' +
                'also remember to Logout if you are finished with your MASAS Session.';
        }
    }
    window.onbeforeunload = confirmWindowClose;
    
    
    /**
     * Close any open (forgotten?) Tool windows when leaving the portal.
     */
    function closeToolWindows() {
        for (var i = 0; i < MASAS.Portal.TOOL_WINDOWS.length; i++) {
            if (!MASAS.Portal.TOOL_WINDOWS[i].closed) {
                MASAS.Portal.TOOL_WINDOWS[i].close();
            }
        }
    }
    window.onunload = closeToolWindows;
    
    
    /**
     * Confirm that a user wants to leave a current Tool session and go back to
     * the main menu.
     */
    MASAS.Portal.confirm_main_return = function () {
        var main_confirm = confirm('Returning to the Main Menu will close all' +
            ' open MASAS Tools, are you sure?');
        if (main_confirm) {
            Ext.getCmp('logout-button').goLogout = true;
            window.location.href = 'main';
        }
    };
    
});

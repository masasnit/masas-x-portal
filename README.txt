** Packaging **

- Follow the instructions in BUILD.txt
- Make sure to update the version number in setup.py
- Add any new modules or files to either setup.py or MANIFEST.in
- Delete the MANIFEST file so it picks up all new files.
- Update requirements.txt with any new packages
- Build the new distribution with python setup.py sdist
- Upload the new distribution package to all hosts.


** Installing **

- Unpack the distribution package into the proper location.
- Rename the package folder to just the version number.
- Copy all of the /static files to their proper location with this version
number used as part of the directory name to ensure they are unique
- Shutdown any running apps that use this package.
- Activate the virtual environment and run pip -r requirements.txt to update
all necessary packages
- Update the config with any changes.
- Create a new soft link for app to the new version location.
- Restart all apps using this package and test for proper operation.
- Ensure crontab entries are in place to run report.py and status.py regularily.

** Gruntfile.js **

The addition of the grunt file will aid in the build process. Currently the grunt
will automatically complete the required shell commands to copy and move files
during the build process.

Installing Grunt

To use Gruntfile.js you will need to install grunt. First you must install npm and node.js. They can be found here:

https://www.npmjs.org/
&
http://nodejs.org/

Once installed go to the home folder of the masas-viewing tool and type:

npm install grunt

Make sure to include the provided Gruntfile.js and package.json when performing the above command.
Then install the required grunt packages. The portal requires use of the shell commands.

npm install grunt-shell

Open Gruntfile.js and configure the following variables:

    pathToViewing: The path to the masas-viewing tool. Example: "/var/www/view"
    pathToPosting: The path to the masas-posting tool. Example:"/var/www/masas-posting"
    pathToPortal:  The path to the masas-portal. Example:"/var/www/masas-portal"

The following should not require changing. These are the names of the folders within the portal:

    staticFolder : "static"
    sourceFolder : "src"
    buildFolder : "build"

Running the following command will copy the required files from the viewing and posting tools:

    grunt prod-build

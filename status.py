#!/usr/bin/env python
#-*- coding: UTF-8 -*-
"""
Name:           MASAS Tools Portal v0.2 Statusboard Check
Author:         Jacob Westfall
Copyright:      Copyright (c) 2013 MASAS Contributors.  Published under the
                Clear BSD license.  See license.txt for the full text of the license.
Created:        Jan 01, 2013
Updated:        Jan 31, 2013
Description:	This app checks a statusboard and updates portal.
"""

import os, sys
import optparse
import urllib2
import logging
from logging.handlers import RotatingFileHandler, SMTPHandler
try:
    import json
except ImportError:
    import simplejson as json
import shutil


# python 2.6 or higher required
if sys.version < "2.6":
    print "Python 2.6 or higher is required"
    sys.exit(1)


class StatusCheck(object):
    """ Checks statusboard for service outages and if there is a change it
    copies the appropriate status icon into place for Portal users """
    
    def __init__(self, debug=False, settings={}):
        """ Setup status check """
        
        self.debug = debug
        self.portal_settings = settings
        self.setup_logging()
    
    
    def setup_logging(self):
        """ use the python logging module to setup logging """
        
        # checks for this logger already having been created
        if logging.root.manager.loggerDict.has_key("status"):
            self.logger = logging.getLogger("status")
        else:
            self.logger = logging.getLogger("status")
            self.logger.setLevel(logging.DEBUG)
            # debugs go to the terminal screen
            screen = logging.StreamHandler()
            screen.setLevel(logging.DEBUG)
            # log to file warnings
            tofile = RotatingFileHandler(self.portal_settings["status_app"]\
                ["logfile"], maxBytes=100000, backupCount=5)
            tofile.setLevel(logging.WARNING)
            # email errors
            email = SMTPHandler(self.portal_settings["email"]["smtp_server"],
                self.portal_settings["email"]["admin_email"],
                self.portal_settings["email"]["admin_email"],
                self.portal_settings["email"]["subject"])
            email.setLevel(logging.ERROR)
            # new format that adds function names and removes milliseconds
            format1 = logging.Formatter("%(asctime)s - %(name)s.%(funcName)s \
- %(message)s", datefmt='%Y-%m-%d %H:%M:%S')
            tofile.setFormatter(format1)
            email.setFormatter(format1)
            format2 = logging.Formatter("%(levelname)s - %(message)s")
            screen.setFormatter(format2)
            # use screen debugging if turned on
            if self.debug:
                self.logger.addHandler(screen)
            self.logger.addHandler(tofile)
            
            # sentry reporting w/email versus email directly for errors
            sentry = None
            if "sentry_dsn" in self.portal_settings:
                try:
                    from raven.handlers.logging import SentryHandler
                    sentry = SentryHandler(dsn=self.portal_settings["sentry_dsn"],
                        site=self.portal_settings["site_name"])
                except Exception, err:
                    print "Unable to initialize Sentry logging. %s" %err
            if sentry:
                sentry.setLevel(logging.ERROR)
                self.logger.addHandler(sentry)
                # use email to send errors with raven/sentry itself as backup
                sentry.client.error_logger.addHandler(email)
            else:
                self.logger.addHandler(email)
    
    
    def run(self):
        """ Run the status check """
        
        prev_state = self.read_state()
        try:
            service_data = self.fetch_services(self.portal_settings["status_app"]\
                ["services_url"])
        except:
            self.logger.error("Services Fetch Error: ", exc_info=True)
            return
        try:
            current_status = self.parse_status(service_data)
        except:
            self.logger.error("Status Parsing Error: ", exc_info=True)
            return
        self.logger.debug("Current status is %s" %current_status)
        if current_status != prev_state:
            self.logger.warning("Status changed to %s" %current_status)
            self.update_icon(current_status)
            self.write_state(current_status)
    
    
    def fetch_services(self, url):
        """ Get the statusboard JSON services data from a target URL """
        
        self.logger.debug("Fetching JSON from %s" %url)
        
        request = urllib2.Request(url,
            headers={"User-Agent": "MASAS Portal Status Check"})
        connection = urllib2.urlopen(request, timeout=120)
        # 2 MB max page size
        data = connection.read(2000000)
        connection.close()
        
        return data
    
    
    def parse_status(self, service_data):
        """ Parse the service status JSON determining if any service is
        down for an overall result """
        
        service_info = json.loads(service_data)
        # only determining between all services as a single value
        current_status = "up"
        for each_service in service_info["services"]:
            if "current-event" in each_service and each_service["current-event"]:
                if "status" in each_service["current-event"]:
                    if each_service["current-event"]["status"].lower() == "down":
                        current_status = "down"
                        self.logger.warning("Service Down: %s" \
                            %each_service["name"])
                    if each_service["current-event"]["status"].lower() == "warning":
                        #TODO: only logging warnings for now
                        self.logger.warning("Service Warning: %s" \
                            %each_service["name"])
        
        return current_status
    
    
    def update_icon(self, current_status):
        """ Update the icon by copying the new icon file accordingly """
        
        if current_status == "up":
            self.logger.debug("Copying %s to %s" \
                %(self.portal_settings["status_app"]["up_icon"],
                  self.portal_settings["status_app"]["status_icon"]))
            shutil.copy(self.portal_settings["status_app"]["up_icon"],
                self.portal_settings["status_app"]["status_icon"])
        elif current_status == "down":
            self.logger.debug("Copying %s to %s" \
                %(self.portal_settings["status_app"]["down_icon"],
                  self.portal_settings["status_app"]["status_icon"]))
            shutil.copy(self.portal_settings["status_app"]["down_icon"],
                self.portal_settings["status_app"]["status_icon"])
    
    
    def read_state(self):
        """ Read the saved state from a previous run """
        
        try:
            if os.path.exists(self.portal_settings["status_app"]["state_file"]):
                s_file = open(self.portal_settings["status_app"]["state_file"], "r")
                state = s_file.read().strip()
                s_file.close()
                self.logger.debug("Loaded state file")
                return state
            else:
                self.logger.warning("State file missing")
                return None
        except:
            self.logger.error("State File Read Error: ", exc_info=True)
            return None
    
    
    def write_state(self, state):
        """ Write the state to a file for next run to use """
        
        try:
            s_file = open(self.portal_settings["status_app"]["state_file"], "w")
            s_file.write(state)
            s_file.close()
        except:
            self.logger.error("State File Write Error: ", exc_info=True)



def run():
    """ run method for Portal statusboard check """
    
    usage = "usage: status [-d] [-c] config"
    parser = optparse.OptionParser(usage)
    parser.add_option("-d", action="store_true", dest="debug",
                      help="print debugging messages")
    parser.add_option("-c", action="append", type="string", dest="config_file",
                      help="read config from this python module")
    (options, args) = parser.parse_args()
    
    # don't catch config exceptions, let them show on commandline
    app_config = {}
    if options.config_file:
        # currently uses the first string provided by options and can
        # be relative or absolute url to .py file
        execfile(options.config_file[0], {}, app_config)
    else:
        # try a default config
        execfile("config.py", {}, app_config)
    
    #TODO: separate section in config for portal versus status?
    
    # run once to start
    check = StatusCheck(debug=options.debug, settings=app_config["portal"])
    check.run()


if __name__ == '__main__':
    run()
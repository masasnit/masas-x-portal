/**
Welcome Page
Updated: Jan 17, 2013
Independent Joint Copyright (c) 2012-2013 MASAS Contributors.  Published
under the Modified BSD license.  See license.txt for the full text of the license.
*/

/*global jQuery,$ */

/**
Load an RSS or Atom feed
*/
function load_feed(url) {
    jQuery.getFeed({
        'url': url,
        success: function (feed) {
            $('#newsLoadingIcon').hide();
            var html = '';    
            for (var i = 0; i < feed.items.length && i < 5; i++) {
                var item = feed.items[i];
                html += '<div class="atomTitle"><a href="' + item.link +
                    '" target="_blank">' + item.title + '</a></div>';
                if (calculate_age(item.updated)) {
                    html += '<div class="atomUpdated recentUpdate">';
                } else {
                    html += '<div class="atomUpdated">';
                }
                html += item.updated + '</div>';
                if (item.description.length > 250) {
                    item.description = jQuery.trim(item.description).substring(0, 250)
                        .split(' ').slice(0, -1).join(' ') + ' ...';
                }
                html += '<div class="atomDescription">' + item.description +
                    '</div>';
            }
            jQuery('#latestNews').append(html);
        },
        failure: function (feed) {
            $('#newsLoadingIcon').hide();
            jQuery('#latestNews').append('.. Error Loading News ..');
        }
    });
}


/**
Determine if the Entry occured recently
*/
function calculate_age(date) {
    var recent = false;
    var current_timestamp = new Date().getTime();
    var prev_timestamp = datetime_parse(date);
    if (prev_timestamp) {
        var diff = current_timestamp - prev_timestamp;
        // recent is 36 hours to account for clock and timezone differences
        if (diff < 129600000) {
            recent = true;
        }
    }
    
    return recent;
}


/**
Parses the datetimes which are different between RSS and Atom
*/
function datetime_parse(date) {
    var timestamp, struct, minutesOffset = 0;
    var numericKeys = [ 1, 4, 5, 6, 7, 10, 11 ];
    // ES5 §15.9.4.2 states that the string should attempt to be parsed as a Date Time String Format string
    // before falling back to any implementation-specific date parsing, so that’s what we do, even if native
    // implementations could be faster
    // 1 YYYY 2 MM 3 DD 4 HH 5 mm 6 ss 7 msec 8 Z 9 ± 10 tzHH 11 tzmm
    if ((struct = /^(\d{4}|[+\-]\d{6})(?:-(\d{2})(?:-(\d{2}))?)?(?:T(\d{2}):(\d{2})(?::(\d{2})(?:\.(\d{3}))?)?(?:(Z)|([+\-])(\d{2})(?::(\d{2}))?)?)?$/.exec(date))) {
        // avoid NaN timestamps caused by “undefined” values being passed to Date.UTC
        for (var i = 0, k; (k = numericKeys[i]); ++i) {
            struct[k] = +struct[k] || 0;
        }
        // allow undefined days and months
        struct[2] = (+struct[2] || 1) - 1;
        struct[3] = +struct[3] || 1;
        if (struct[8] !== 'Z' && struct[9] !== undefined) {
            minutesOffset = struct[10] * 60 + struct[11];
            
            if (struct[9] === '+') {
                minutesOffset = 0 - minutesOffset;
            }
        }
        timestamp = Date.UTC(struct[1], struct[2], struct[3], struct[4], struct[5] + minutesOffset, struct[6], struct[7]);
    } else {
        timestamp = Date.parse(date);
    }
    
    return timestamp;
}


/* ======================================================================
    GeoExt/data/FeatureReader.js
   ====================================================================== */

/**
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 * 
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */

/**
 * @include GeoExt/data/FeatureRecord.js
 * @require OpenLayers/Feature/Vector.js
 */

/** api: (define)
 *  module = GeoExt.data
 *  class = FeatureReader
 *  base_link = `Ext.data.DataReader <http://dev.sencha.com/deploy/dev/docs/?class=Ext.data.DataReader>`_
 */
Ext.namespace('GeoExt', 'GeoExt.data');

/** api: example
 *  Typical usage in a store:
 * 
 *  .. code-block:: javascript
 *     
 *      var store = new Ext.data.Store({
 *          reader: new GeoExt.data.FeatureReader({}, [
 *              {name: 'name', type: 'string'},
 *              {name: 'elevation', type: 'float'}
 *          ])
 *      });
 *      
 */

/** api: constructor
 *  .. class:: FeatureReader(meta, recordType)
 *   
 *      Data reader class to create an array of
 *      :class:`GeoExt.data.FeatureRecord` objects from an
 *      ``OpenLayers.Protocol.Response`` object for use in a
 *      :class:`GeoExt.data.FeatureStore` object.
 */
GeoExt.data.FeatureReader = function(meta, recordType) {
    meta = meta || {};
    if(!(recordType instanceof Function)) {
        recordType = GeoExt.data.FeatureRecord.create(
            recordType || meta.fields || {});
    }
    GeoExt.data.FeatureReader.superclass.constructor.call(
        this, meta, recordType);
};

Ext.extend(GeoExt.data.FeatureReader, Ext.data.DataReader, {

    /**
     * APIProperty: totalRecords
     * {Integer}
     */
    totalRecords: null,

    /** private: method[read]
     *  :param response: ``OpenLayers.Protocol.Response``
     *  :return: ``Object`` An object with two properties. The value of the
     *      ``records`` property is the array of records corresponding to
     *      the features. The value of the ``totalRecords" property is the
     *      number of records in the array.
     *      
     *  This method is only used by a DataProxy which has retrieved data.
     */
    read: function(response) {
        return this.readRecords(response.features);
    },

    /** api: method[readRecords]
     *  :param features: ``Array(OpenLayers.Feature.Vector)`` List of
     *      features for creating records
     *  :return: ``Object``  An object with ``records`` and ``totalRecords``
     *      properties.
     *  
     *  Create a data block containing :class:`GeoExt.data.FeatureRecord`
     *  objects from an array of features.
     */
    readRecords : function(features) {
        var records = [];

        if (features) {
            var recordType = this.recordType, fields = recordType.prototype.fields;
            var i, lenI, j, lenJ, feature, values, field, v;
            for (i = 0, lenI = features.length; i < lenI; i++) {
                feature = features[i];
                values = {};
                if (feature.attributes) {
                    for (j = 0, lenJ = fields.length; j < lenJ; j++){
                        field = fields.items[j];
                        if (/[\[\.]/.test(field.mapping)) {
                            try {
                                v = new Function("obj", "return obj." + field.mapping)(feature.attributes);
                            } catch(e){
                                v = field.defaultValue;
                            }
                        }
                        else {
                            v = feature.attributes[field.mapping || field.name] || field.defaultValue;
                        }
                        if (field.convert) {
                            v = field.convert(v, feature);
                        }
                        values[field.name] = v;
                    }
                }
                values.feature = feature;
                values.state = feature.state;
                values.fid = feature.fid;

                // newly inserted features need to be made into phantom records
                var id = (feature.state === OpenLayers.State.INSERT) ? undefined : feature.id;
                records[records.length] = new recordType(values, id);
            }
        }

        return {
            records: records,
            totalRecords: this.totalRecords != null ? this.totalRecords : records.length
        };
    }
});
/* ======================================================================
    GeoExt/Lang.js
   ====================================================================== */

/**
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 *
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */

/** api: (define)
 *  module = GeoExt
 *  class = Lang
 *  base_link = `Ext.util.Observable <http://dev.sencha.com/deploy/dev/docs/?class=Ext.util.Observable>`_
 */
Ext.namespace("GeoExt");

/** api: constructor
 *  .. class:: Lang
 *
 *      The GeoExt.Lang singleton is created when the library is loaded.
 *      Include all relevant language files after this file in your build.
 */
GeoExt.Lang = new (Ext.extend(Ext.util.Observable, {

    /** api: property[locale]
     *  ``String``
     *  The current language tag.  Use :meth:`set` to set the locale.  Defaults
     *  to the browser language where available.
     */
    locale: navigator.language || navigator.userLanguage,

    /** private: property[dict]
     *  ``Object``
     *  Dictionary of string lookups per language.
     */
    dict: null,

    /** private: method[constructor]
     *  Construct the Lang singleton.
     */
    constructor: function() {
        this.addEvents(
            /** api: event[localize]
             *  Fires when localized strings are set.  Listeners will receive a
             *  single ``locale`` event with the language tag.
             */
            "localize"
        );
        this.dict = {};
        Ext.util.Observable.constructor.apply(this, arguments);
    },

    /** api: method[add]
     *  :param locale: ``String`` A language tag that follows the "en-CA"
     *      convention (http://www.ietf.org/rfc/rfc3066.txt).
     *  :param lookup: ``Object`` An object with properties that are dot
     *      delimited names of objects with localizable strings (e.g.
     *      "GeoExt.VectorLegend.prototype").  The values for these properties
     *      are objects that will be used to extend the target objects with
     *      localized strings (e.g. {untitledPrefix: "Untitiled "})
     *
     *  Add translation strings to the dictionary.  This method can be called
     *  multiple times with the same language tag (locale argument) to extend
     *  a single dictionary.
     */
    add: function(locale, lookup) {
        var obj = this.dict[locale];
        if (!obj) {
            this.dict[locale] = Ext.apply({}, lookup);
        } else {
            for (var key in lookup) {
                obj[key] = Ext.apply(obj[key] || {}, lookup[key]);
            }
        }
        if (!locale || locale === this.locale) {
            this.set(locale);
        } else if (this.locale.indexOf(locale + "-") === 0) {
            // current locale is regional variation of added strings
            // call set so newly added strings are used where appropriate
            this.set(this.locale);
        }
    },

    /** api: method[set]
     * :arg locale: ''String'' Language identifier tag following recommendations
     *     at http://www.ietf.org/rfc/rfc3066.txt.
     *
     * Set the language for all GeoExt components.  This will use any localized
     * strings in the dictionary (set with the :meth:`add` method) that
     * correspond to the complete matching language tag or any "higher order"
     * tag (e.g. setting "en-CA" will use strings from the "en" dictionary if
     * matching strings are not found in the "en-CA" dictionary).
     */
    set: function(locale) {
        // compile lookup based on primary and all subtags
        var tags = locale ? locale.split("-") : [];
        var id = "";
        var lookup = {}, parent;
        for (var i=0, ii=tags.length; i<ii; ++i) {
            id += (id && "-" || "") + tags[i];
            if (id in this.dict) {
                parent = this.dict[id];
                for (var str in parent) {
                    if (str in lookup) {
                        Ext.apply(lookup[str], parent[str]);
                    } else {
                        lookup[str] = Ext.apply({}, parent[str]);
                    }
                }
            }
        }

        // now extend all objects given by dot delimited names in lookup
        for (var str in lookup) {
            var obj = window;
            var parts = str.split(".");
            var missing = false;
            for (var i=0, ii=parts.length; i<ii; ++i) {
                var name = parts[i];
                if (name in obj) {
                    obj = obj[name];
                } else {
                    missing = true;
                    break;
                }
            }
            if (!missing) {
                Ext.apply(obj, lookup[str]);
            }
        }
        this.locale = locale;
        this.fireEvent("localize", locale);
    }
}))();

/* ======================================================================
    GeoExt/data/FeatureStore.js
   ====================================================================== */

/**
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 * 
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */

/**
 * @include GeoExt/data/FeatureReader.js
 * @require OpenLayers/Feature/Vector.js
 */

/** api: (define)
 *  module = GeoExt.data
 *  class = FeatureStore
 *  base_link = `Ext.data.Store <http://dev.sencha.com/deploy/dev/docs/?class=Ext.data.Store>`_
 */
Ext.namespace("GeoExt.data");

/** api: constructor
 *  .. class:: FeatureStore
 *
 *      A store containing :class:`GeoExt.data.FeatureRecord` entries that
 *      optionally synchronizes with an ``OpenLayers.Layer.Vector``.
 */

/** api: example
 *  Sample code to create a store with features from a vector layer:
 *  
 *  .. code-block:: javascript
 *
 *      var store = new GeoExt.data.FeatureStore({
 *          layer: myLayer,
 *          features: myFeatures
 *      });
 */

/**
 * Class: GeoExt.data.FeatureStoreMixin
 * A store that synchronizes a features array of an {OpenLayers.Layer.Vector} with a
 * feature store holding {<GeoExt.data.FeatureRecord>} entries.
 * 
 * This class can not be instantiated directly. Instead, it is meant to extend
 * {Ext.data.Store} or a subclass of it:
 * (start code)
 * var store = new (Ext.extend(Ext.data.Store, new GeoExt.data.FeatureStoreMixin))({
 *     layer: myLayer,
 *     features: myFeatures
 * });
 * (end)
 * 
 * For convenience, a {<GeoExt.data.FeatureStore>} class is available as a
 * shortcut to the Ext.extend sequence in the above code snippet. The above
 * is equivalent to:
 * (start code)
 * var store = new GeoExt.data.FeatureStore({
 *     layer: myLayer,
 *     features: myFeatures
 * });
 * (end)
 */
GeoExt.data.FeatureStoreMixin = function() {
    return {
        /** api: config[layer]
         *  ``OpenLayers.Layer.Vector``  Layer to synchronize the store with.
         */
        layer: null,
        
        /** api: config[features]
         *  ``Array(OpenLayers.Feature.Vector)``  Features that will be added to the
         *  store (and the layer if provided).
         */

        /** api: config[reader]
         *  ``Ext.data.DataReader`` The reader used to produce records from objects
         *  features.  Default is :class:`GeoExt.data.FeatureReader`.
         */
        reader: null,

        /** api: config[featureFilter]
         *  ``OpenLayers.Filter`` This filter is evaluated before a feature
         *  record is added to the store.
         */
        featureFilter: null,
        
        /** api: config[initDir]
         *  ``Number``  Bitfields specifying the direction to use for the
         *  initial sync between the layer and the store, if set to 0 then no
         *  initial sync is done. Default is
         *  ``GeoExt.data.FeatureStore.LAYER_TO_STORE|GeoExt.data.FeatureStore.STORE_TO_LAYER``.
         */

        /** private */
        constructor: function(config) {
            config = config || {};
            config.reader = config.reader ||
                            new GeoExt.data.FeatureReader({}, config.fields);
            var layer = config.layer;
            delete config.layer;
            // 'features' option - is an alias 'data' option
            if (config.features) {
                config.data = config.features;
            }
            delete config.features;
            // "initDir" option
            var options = {initDir: config.initDir};
            delete config.initDir;
            arguments.callee.superclass.constructor.call(this, config);
            if(layer) {
                this.bind(layer, options);
            }
        },

        /** api: method[bind]
         *  :param layer: ``OpenLayers.Layer`` Layer that the store should be
         *      synchronized with.
         *  
         *  Bind this store to a layer instance, once bound the store
         *  is synchronized with the layer and vice-versa.
         */ 
        bind: function(layer, options) {
            if(this.layer) {
                // already bound
                return;
            }
            this.layer = layer;
            options = options || {};

            var initDir = options.initDir;
            if(options.initDir == undefined) {
                initDir = GeoExt.data.FeatureStore.LAYER_TO_STORE |
                          GeoExt.data.FeatureStore.STORE_TO_LAYER;
            }

            // create a snapshot of the layer's features
            var features = layer.features.slice(0);

            if(initDir & GeoExt.data.FeatureStore.STORE_TO_LAYER) {
                var records = this.getRange();
                for(var i=records.length - 1; i>=0; i--) {
                    this.layer.addFeatures([records[i].getFeature()]);
                }
            }

            if(initDir & GeoExt.data.FeatureStore.LAYER_TO_STORE) {
                this.loadData(features, true /* append */);
            }

            layer.events.on({
                "featuresadded": this.onFeaturesAdded,
                "featuresremoved": this.onFeaturesRemoved,
                "featuremodified": this.onFeatureModified,
                scope: this
            });
            this.on({
                "load": this.onLoad,
                "clear": this.onClear,
                "add": this.onAdd,
                "remove": this.onRemove,
                "update": this.onUpdate,
                scope: this
            });
        },

        /** api: method[unbind]
         *  Unbind this store from the layer it is currently bound.
         */
        unbind: function() {
            if(this.layer) {
                this.layer.events.un({
                    "featuresadded": this.onFeaturesAdded,
                    "featuresremoved": this.onFeaturesRemoved,
                    "featuremodified": this.onFeatureModified,
                    scope: this
                });
                this.un("load", this.onLoad, this);
                this.un("clear", this.onClear, this);
                this.un("add", this.onAdd, this);
                this.un("remove", this.onRemove, this);
                this.un("update", this.onUpdate, this);

                this.layer = null;
            }
        },
       
        /** api: method[getRecordFromFeature]
         *  :arg feature: ``OpenLayers.Vector.Feature``
         *  :returns: :class:`GeoExt.data.FeatureRecord` The record corresponding
         *      to the given feature.  Returns null if no record matches.
         *
         *  *Deprecated* Use getByFeature instead.
         *
         *  Get the record corresponding to a feature.
         */
        getRecordFromFeature: function(feature) {
            return this.getByFeature(feature) || null;
        },
        
        /** api: method[getByFeature]
         *  :arg feature: ``OpenLayers.Vector.Feature``
         *  :returns: :class:`GeoExt.data.FeatureRecord` The record corresponding
         *      to the given feature.  Returns undefined if no record matches.
         *
         *  Get the record corresponding to a feature.
         */
        getByFeature: function(feature) {
            var record;
            if(feature.state !== OpenLayers.State.INSERT) {
                record = this.getById(feature.id);
            } else {
                var index = this.findBy(function(r) {
                    return r.getFeature() === feature;
                });
                if(index > -1) {
                    record = this.getAt(index);
                }
            }
            return record;
        },
       
        /** private: method[onFeaturesAdded]
         *  Handler for layer featuresadded event
         */
        onFeaturesAdded: function(evt) {
            if(!this._adding) {
                var features = evt.features, toAdd = features;
                if(this.featureFilter) {
                    toAdd = [];
                    var i, len, feature;
                    for(var i=0, len=features.length; i<len; i++) {
                        feature = features[i];
                        if (this.featureFilter.evaluate(feature) !== false) {
                            toAdd.push(feature);
                        }
                    }
                }
                // add feature records to the store, when called with
                // append true loadData triggers an "add" event and
                // then a "load" event
                this._adding = true;
                this.loadData(toAdd, true /* append */);
                delete this._adding;
            }
        },
        
        /** private: method[onFeaturesRemoved]
         *  Handler for layer featuresremoved event
         */
        onFeaturesRemoved: function(evt){
            if(!this._removing) {
                var features = evt.features, feature, record, i;
                for(i=features.length - 1; i>=0; i--) {
                    feature = features[i];
                    record = this.getByFeature(feature);
                    if (record) {
                        this._removing = true;
                        try {
                            this.remove(record);
                        } catch (err) {
                            /* Added by JPW
                             * IE 9+ throws an error when leaving the page as it
                             * tries to destroy/cleanup layers.  This catch here
                             * helps to resolve, but could also be catching errors
                             * that occur in other cases.  Ideally, there would
                             * be a "beforedestroy" event for the layer and it
                             * could be inspected to determine if the features
                             * are being removed as part of destroying the layer
                             * or just because new features are being reloaded.
                             * In the meantime, logging them at least so they
                             * can be reviewed if necessary.  Use Persist console.
                             */
                            console.log('GeoExt.FeatureStore.onFeaturesRemoved error: ' + err.message);
                        }
                        delete this._removing;
                    }
                }
            }
        },
        
        /** private: method[onFeatureModified]
         *  Handler for layer featuremodified event
         */
        onFeatureModified: function(evt) {
            if(!this._updating) {
                var feature = evt.feature;
                var record = this.getByFeature(feature);
                if(record !== undefined) {
                    record.beginEdit();
                    var attributes = feature.attributes;
                    if(attributes) {
                        var fields = this.recordType.prototype.fields;
                        for(var i=0, len=fields.length; i<len; i++) {
                            var field = fields.items[i];
                            var key = field.mapping || field.name;
                            if(key in attributes) {
                                record.set(field.name, field.convert(attributes[key]));
                            }
                        }
                    }
                    // the calls to set below won't trigger "update"
                    // events because we called beginEdit to start a
                    // "transaction", "update" will be triggered by
                    // endEdit
                    record.set("state", feature.state);
                    record.set("fid", feature.fid);
                    record.setFeature(feature);
                    this._updating = true;
                    record.endEdit();
                    delete this._updating;
                }
            }
        },

        /** private: method[addFeaturesToLayer]
         *  Given an array of records add features to the layer. This
         *  function is used by the onLoad and onAdd handlers.
         */
        addFeaturesToLayer: function(records) {
            var i, len, features;
            features = new Array((len=records.length));
            for(i=0; i<len; i++) {
                features[i] = records[i].getFeature();
            }
            if(features.length > 0) {
                this._adding = true;
                this.layer.addFeatures(features);
                delete this._adding;
            }
        },
       
        /** private: method[onLoad]
         *  :param store: ``Ext.data.Store``
         *  :param records: ``Array(Ext.data.Record)``
         *  :param options: ``Object``
         * 
         *  Handler for store load event
         */
        onLoad: function(store, records, options) {
            // if options.add is true an "add" event was already
            // triggered, and onAdd already did the work of 
            // adding the features to the layer.
            if(!options || options.add !== true) {
                this._removing = true;
                this.layer.removeFeatures(this.layer.features);
                delete this._removing;

                this.addFeaturesToLayer(records);
            }
        },
        
        /** private: method[onClear]
         *  :param store: ``Ext.data.Store``
         *      
         *  Handler for store clear event
         */
        onClear: function(store) {
            this._removing = true;
            this.layer.removeFeatures(this.layer.features);
            delete this._removing;
        },
        
        /** private: method[onAdd]
         *  :param store: ``Ext.data.Store``
         *  :param records: ``Array(Ext.data.Record)``
         *  :param index: ``Number``
         * 
         *  Handler for store add event
         */
        onAdd: function(store, records, index) {
            if(!this._adding) {
                // addFeaturesToLayer takes care of setting
                // this._adding to true and deleting it
                this.addFeaturesToLayer(records);
            }
        },
        
        /** private: method[onRemove]
         *  :param store: ``Ext.data.Store``
         *  :param records: ``Array(Ext.data.Record)``
         *  :param index: ``Number``
         *      
         *  Handler for store remove event
         */
        onRemove: function(store, record, index){
            if(!this._removing) {
                var feature = record.getFeature();
                if (this.layer.getFeatureById(feature.id) != null) {
                    this._removing = true;
                    this.layer.removeFeatures([record.getFeature()]);
                    delete this._removing;
                }
            }
        },

        /** private: method[onUpdate]
         *  :param store: ``Ext.data.Store``
         *  :param record: ``Ext.data.Record``
         *  :param operation: ``String``
         *
         *  Handler for update.
         */
        onUpdate: function(store, record, operation) {
            if(!this._updating) {
                /**
                  * TODO: remove this if the FeatureReader adds attributes
                  * for all fields that map to feature.attributes.
                  * In that case, it would be sufficient to check (key in feature.attributes). 
                  */
                var defaultFields = new GeoExt.data.FeatureRecord().fields;
                var feature = record.getFeature();
                if (feature.state !== OpenLayers.State.INSERT) {
                    feature.state = OpenLayers.State.UPDATE;
                }
                if(record.fields) {
                    var cont = this.layer.events.triggerEvent(
                        "beforefeaturemodified", {feature: feature}
                    );
                    if(cont !== false) {
                        var attributes = feature.attributes;
                        record.fields.each(
                            function(field) {
                                var key = field.mapping || field.name;
                                if (!defaultFields.containsKey(key)) {
                                    attributes[key] = record.get(field.name);
                                }
                            }
                        );
                        this._updating = true;
                        this.layer.events.triggerEvent(
                            "featuremodified", {feature: feature}
                        );
                        delete this._updating;
                        if (this.layer.getFeatureById(feature.id) != null) {
                            this.layer.drawFeature(feature);
                        }
                    }
                }
            }
        },

        /** private: method[destroy]
         */
        destroy: function() {
            this.unbind();
            GeoExt.data.FeatureStore.superclass.destroy.call(this);
        }

    };
};

GeoExt.data.FeatureStore = Ext.extend(
    Ext.data.Store,
    new GeoExt.data.FeatureStoreMixin
);

/**
 * Constant: GeoExt.data.FeatureStore.LAYER_TO_STORE
 * {Integer} Constant used to make the store be automatically updated
 * when changes occur in the layer.
 */
GeoExt.data.FeatureStore.LAYER_TO_STORE = 1;

/**
 * Constant: GeoExt.data.FeatureStore.STORE_TO_LAYER
 * {Integer} Constant used to make the layer be automatically updated
 * when changes occur in the store.
 */
GeoExt.data.FeatureStore.STORE_TO_LAYER = 2;
/* ======================================================================
    GeoExt/data/FeatureRecord.js
   ====================================================================== */

/**
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 * 
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */

/** api: (define)
 *  module = GeoExt.data
 *  class = FeatureRecord
 *  base_link = `Ext.data.Record <http://dev.sencha.com/deploy/dev/docs/?class=Ext.data.Record>`_
 */
Ext.namespace("GeoExt.data");

/** api: constructor
 *  .. class:: FeatureRecord
 *  
 *      A record that represents an ``OpenLayers.Feature.Vector``. This record
 *      will always have at least the following fields:
 *
 *      * state ``String``
 *      * fid ``String``
 *
 */
GeoExt.data.FeatureRecord = Ext.data.Record.create([
    {name: "feature"}, {name: "state"}, {name: "fid"}
]);

/** api: method[getFeature]
 *  :return: ``OpenLayers.Feature.Vector``
 *
 *  Gets the feature for this record.
 */
GeoExt.data.FeatureRecord.prototype.getFeature = function() {
    return this.get("feature");
};

/** api: method[setFeature]
 *  :param feature: ``OpenLayers.Feature.Vector``
 *
 *  Sets the feature for this record.
 */
GeoExt.data.FeatureRecord.prototype.setFeature = function(feature) {
    if (feature !== this.data.feature) {
        this.dirty = true;
        if (!this.modified) {
            this.modified = {};
        }
        if (this.modified.feature === undefined) {
            this.modified.feature = this.data.feature;
        }
        this.data.feature = feature;
        if (!this.editing){
            this.afterEdit();
        }
    }
};

/** api: classmethod[create]
 *  :param o: ``Array`` Field definition as in ``Ext.data.Record.create``. Can
 *      be omitted if no additional fields are required.
 *  :return: ``Function`` A specialized :class:`GeoExt.data.FeatureRecord`
 *      constructor.
 *  
 *  Creates a constructor for a :class:`GeoExt.data.FeatureRecord`, optionally
 *  with additional fields.
 */
GeoExt.data.FeatureRecord.create = function(o) {
    var f = Ext.extend(GeoExt.data.FeatureRecord, {});
    var p = f.prototype;

    p.fields = new Ext.util.MixedCollection(false, function(field) {
        return field.name;
    });

    GeoExt.data.FeatureRecord.prototype.fields.each(function(f) {
        p.fields.add(f);
    });

    if(o) {
        for(var i = 0, len = o.length; i < len; i++){
            p.fields.add(new Ext.data.Field(o[i]));
        }
    }

    f.getField = function(name) {
        return p.fields.get(name);
    };

    return f;
};
/* ======================================================================
    GeoExt/widgets/MapPanel.js
   ====================================================================== */

/**
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 * 
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */

/**
 * @include GeoExt/data/LayerStore.js
 * @require OpenLayers/Map.js
 * @require OpenLayers/BaseTypes/LonLat.js
 * @require OpenLayers/BaseTypes/Bounds.js
 */

/** api: (define)
 *  module = GeoExt
 *  class = MapPanel
 *  base_link = `Ext.Panel <http://dev.sencha.com/deploy/dev/docs/?class=Ext.Panel>`_
 */
Ext.namespace("GeoExt");

/** api: example
 *  Sample code to create a panel with a new map:
 * 
 *  .. code-block:: javascript
 *     
 *      var mapPanel = new GeoExt.MapPanel({
 *          border: false,
 *          renderTo: "div-id",
 *          map: {
 *              maxExtent: new OpenLayers.Bounds(-90, -45, 90, 45)
 *          }
 *      });
 *     
 *  Sample code to create a map panel with a bottom toolbar in a Window:
 * 
 *  .. code-block:: javascript
 * 
 *      var win = new Ext.Window({
 *          title: "My Map",
 *          items: [{
 *              xtype: "gx_mappanel",
 *              bbar: new Ext.Toolbar()
 *          }]
 *      });
 */

/** api: constructor
 *  .. class:: MapPanel(config)
 *   
 *      Create a panel container for a map. The map contained by this panel
 *      will initially be zoomed to either the center and zoom level configured
 *      by the ``center`` and ``zoom`` configuration options, or the configured
 *      ``extent``, or - if neither are provided - the extent returned by the
 *      map's ``getExtent()`` method.
 */
GeoExt.MapPanel = Ext.extend(Ext.Panel, {

    /** api: config[map]
     *  ``OpenLayers.Map or Object``  A configured map or a configuration object
     *  for the map constructor.  A configured map will be available after
     *  construction through the :attr:`map` property.
     */

    /** api: property[map]
     *  ``OpenLayers.Map`` or ``Object``  A map or map configuration.
     */
    map: null,
    
    /** api: config[layers]
     *  ``GeoExt.data.LayerStore or GeoExt.data.GroupingStore or Array(OpenLayers.Layer)``
     *  A store holding records. The layers provided here will be added to this
     *  MapPanel's map when it is rendered.
     */
    
    /** api: property[layers]
     *  :class:`GeoExt.data.LayerStore`  A store containing
     *  :class:`GeoExt.data.LayerRecord` objects.
     */
    layers: null,
    
    /** api: config[center]
     *  ``OpenLayers.LonLat or Array(Number)``  A location for the initial map
     *  center.  If an array is provided, the first two items should represent
     *  x & y coordinates.
     */
    center: null,

    /** api: config[zoom]
     *  ``Number``  An initial zoom level for the map.
     */
    zoom: null,

    /** api: config[extent]
     *  ``OpenLayers.Bounds or Array(Number)``  An initial extent for the map (used
     *  if center and zoom are not provided.  If an array, the first four items
     *  should be minx, miny, maxx, maxy.
     */
    extent: null,
    
    /** api: config[prettyStateKeys]
     *  ``Boolean`` Set this to true if you want pretty strings in the MapPanel's
     *  state keys. More specifically, layer.name instead of layer.id will be used
     *  in the state keys if this option is set to true. But in that case you have
     *  to make sure you don't have two layers with the same name. Defaults to 
     *  false.
     */
    prettyStateKeys: false,

    /** private: property[stateEvents]
     *  ``Array(String)`` Array of state events
     */
    stateEvents: ["aftermapmove",
                  "afterlayervisibilitychange",
                  "afterlayeropacitychange",
                  "afterlayerorderchange",
                  "afterlayernamechange",
                  "afterlayeradd",
                  "afterlayerremove"],

    /** private: method[initComponent]
     *  Initializes the map panel. Creates an OpenLayers map if
     *  none was provided in the config options passed to the
     *  constructor.
     */
    initComponent: function(){
        if(!(this.map instanceof OpenLayers.Map)) {
            this.map = new OpenLayers.Map(
                Ext.applyIf(this.map || {}, {allOverlays: true})
            );
        }
        var layers = this.layers;
        if(!layers || layers instanceof Array) {
            this.layers = new GeoExt.data.LayerStore({
                layers: layers,
                map: this.map.layers.length > 0 ? this.map : null
            });
        }
        
        if(typeof this.center == "string") {
            this.center = OpenLayers.LonLat.fromString(this.center);
        } else if(this.center instanceof Array) {
            this.center = new OpenLayers.LonLat(this.center[0], this.center[1]);
        }
        if(typeof this.extent == "string") {
            this.extent = OpenLayers.Bounds.fromString(this.extent);
        } else if(this.extent instanceof Array) {
            this.extent = OpenLayers.Bounds.fromArray(this.extent);
        }
        
        GeoExt.MapPanel.superclass.initComponent.call(this);

        this.addEvents(
            /** private: event[aftermapmove]
             *  Fires after the map is moved.
             */
            "aftermapmove",

            /** private: event[afterlayervisibilitychange]
             *  Fires after a layer changed visibility.
             */
            "afterlayervisibilitychange",

            /** private: event[afterlayeropacitychange]
             *  Fires after a layer changed opacity.
             */
            "afterlayeropacitychange",

            /** private: event[afterlayerorderchange]
             *  Fires after a layer order changed.
             */
            "afterlayerorderchange",

            /** private: event[afterlayernamechange]
             *  Fires after a layer name changed.
             */
            "afterlayernamechange",

            /** private: event[afterlayeradd]
             *  Fires after a layer added to the map.
             */
            "afterlayeradd",

            /** private: event[afterlayerremove]
             *  Fires after a layer removed from the map.
             */
            "afterlayerremove"
        );
        this.map.events.on({
            "moveend": this.onMoveend,
            "changelayer": this.onChangelayer,
            "addlayer": this.onAddlayer,
            "removelayer": this.onRemovelayer,
            scope: this
        });
        //TODO This should be handled by a LayoutManager
        this.on("afterlayout", function() {
            //TODO remove function check when we require OpenLayers > 2.11
            if (typeof this.map.getViewport === "function") {
                this.items.each(function(cmp) {
                    if (typeof cmp.addToMapPanel === "function") {
                        cmp.getEl().appendTo(this.map.getViewport());
                    }
                }, this);
            }
        }, this);
    },

    /** private: method[onMoveend]
     *
     *  The "moveend" listener.
     */
    onMoveend: function() {
        this.fireEvent("aftermapmove");
    },

    /** private: method[onChangelayer]
     *  :param e: ``Object``
     *
     * The "changelayer" listener.
     */
    onChangelayer: function(e) {
        if(e.property) {
            if(e.property === "visibility") {
                this.fireEvent("afterlayervisibilitychange");
            } else if(e.property === "order") {
                this.fireEvent("afterlayerorderchange");
            } else if(e.property === "name") {
                this.fireEvent("afterlayernamechange");
            } else if(e.property === "opacity") {
                this.fireEvent("afterlayeropacitychange");
            }
        }
    },

    /** private: method[onAddlayer]
     */
    onAddlayer: function() {
        this.fireEvent("afterlayeradd");
    },

    /** private: method[onRemovelayer]
     */
    onRemovelayer: function() {
        this.fireEvent("afterlayerremove");
    },

    /** private: method[applyState]
     *  :param state: ``Object`` The state to apply.
     *
     *  Apply the state provided as an argument.
     */
    applyState: function(state) {

        // if we get strings for state.x, state.y or state.zoom
        // OpenLayers will take care of converting them to the
        // appropriate types so we don't bother with that
        this.center = new OpenLayers.LonLat(state.x, state.y);
        this.zoom = state.zoom;

        // set layer visibility and opacity
        var i, l, layer, layerId, visibility, opacity;
        var layers = this.map.layers;
        for(i=0, l=layers.length; i<l; i++) {
            layer = layers[i];
            layerId = this.prettyStateKeys ? layer.name : layer.id;
            visibility = state["visibility_" + layerId];
            if(visibility !== undefined) {
                // convert to boolean
                visibility = (/^true$/i).test(visibility);
                if(layer.isBaseLayer) {
                    if(visibility) {
                        this.map.setBaseLayer(layer);
                    }
                } else {
                    layer.setVisibility(visibility);
                }
            }
            opacity = state["opacity_" + layerId];
            if(opacity !== undefined) {
                layer.setOpacity(opacity);
            }
        }
    },

    /** private: method[getState]
     *  :return:  ``Object`` The state.
     *
     *  Returns the current state for the map panel.
     */
    getState: function() {
        var state;

        // Ext delays the call to getState when a state event
        // occurs, so the MapPanel may have been destroyed
        // between the time the event occurred and the time
        // getState is called
        if(!this.map) {
            return;
        }

        // record location and zoom level
        var center = this.map.getCenter();
        // map may not be centered yet, because it may still have zero
        // dimensions or no layers
        state = center ? {
            x: center.lon,
            y: center.lat,
            zoom: this.map.getZoom()
        } : {};

        // record layer visibility and opacity
        var i, l, layer, layerId, layers = this.map.layers;
        for(i=0, l=layers.length; i<l; i++) {
            layer = layers[i];
            layerId = this.prettyStateKeys ? layer.name : layer.id;
            state["visibility_" + layerId] = layer.getVisibility();
            state["opacity_" + layerId] = layer.opacity == null ?
                1 : layer.opacity;
        }

        return state;
    },

    /** private: method[updateMapSize]
     *  Tell the map that it needs to recalculate its size and position.
     */
    updateMapSize: function() {
        if(this.map) {
            this.map.updateSize();
        }
    },

    /** private: method[renderMap]
     *  Private method called after the panel has been rendered or after it
     *  has been laid out by its parent's layout.
     */
    renderMap: function() {
        var map = this.map;
        map.render(this.body.dom);

        this.layers.bind(map);

        if (map.layers.length > 0) {
            this.setInitialExtent();
        } else {
            this.layers.on("add", this.setInitialExtent, this, {single: true});
        }
    },
    
    /** private: method[setInitialExtent]
     *  Sets the initial extent of this panel's map
     */
    setInitialExtent: function() {
        var map = this.map;
        if(this.center || this.zoom != null) {
            // both do not have to be defined
            map.setCenter(this.center, this.zoom);
        } else if(this.extent) {
            map.zoomToExtent(this.extent);
        } else {
            map.zoomToMaxExtent();
        }
    },
    
    /** private: method[afterRender]
     *  Private method called after the panel has been rendered.
     */
    afterRender: function() {
        GeoExt.MapPanel.superclass.afterRender.apply(this, arguments);
        if(!this.ownerCt) {
            this.renderMap();
        } else {
            this.ownerCt.on("move", this.updateMapSize, this);
            this.ownerCt.on({
                "afterlayout": this.afterLayout,
                scope: this
            });
        }
    },
    
    /** private: method[afterLayout]
     *  Private method called after owner container has been laid out until
     *  this panel has dimensions greater than zero.
     */
    afterLayout: function() {
        var width = this.getInnerWidth() -
                                this.body.getBorderWidth("lr");
        var height = this.getInnerHeight() -
                                this.body.getBorderWidth("tb");
        if (width > 0 && height > 0) {
            this.ownerCt.un("afterlayout", this.afterLayout, this);
            this.renderMap();
        }
    },

    /** private: method[onResize]
     *  Private method called after the panel has been resized.
     */
    onResize: function() {
        GeoExt.MapPanel.superclass.onResize.apply(this, arguments);
        this.updateMapSize();
    },
    
    /** private: method[onBeforeAdd]
     *  Private method called before a component is added to the panel.
     */
    onBeforeAdd: function(item) {
        if(typeof item.addToMapPanel === "function") {
            item.addToMapPanel(this);
        }
        GeoExt.MapPanel.superclass.onBeforeAdd.apply(this, arguments);
    },
    
    /** private: method[remove]
     *  Private method called when a component is removed from the panel.
     */
    remove: function(item, autoDestroy) {
        if(typeof item.removeFromMapPanel === "function") {
            item.removeFromMapPanel(this);
        }
        GeoExt.MapPanel.superclass.remove.apply(this, arguments);
    },

    /** private: method[beforeDestroy]
     *  Private method called during the destroy sequence.
     */
    beforeDestroy: function() {
        if(this.ownerCt) {
            this.ownerCt.un("move", this.updateMapSize, this);
        }
        if(this.map && this.map.events) {
            this.map.events.un({
                "moveend": this.onMoveend,
                "changelayer": this.onChangelayer,
                "addlayer": this.onAddlayer,
                "removelayer": this.onRemovelayer,
                scope: this
            });
        }
        // if the map panel was passed a map instance, this map instance
        // is under the user's responsibility
        if(!this.initialConfig.map ||
           !(this.initialConfig.map instanceof OpenLayers.Map)) {
            // we created the map, we destroy it
            if(this.map && this.map.destroy) {
                this.map.destroy();
            }
        }
        delete this.map;
        GeoExt.MapPanel.superclass.beforeDestroy.apply(this, arguments);
    }
    
});

/** api: function[guess]
 *  :return: ``GeoExt.MapPanel`` The first map panel found by the Ext
 *      component manager.
 *  
 *  Convenience function for guessing the map panel of an application. This
 *     can reliably be used for all applications that just have one map panel
 *     in the viewport.
 */
GeoExt.MapPanel.guess = function() {
    return Ext.ComponentMgr.all.find(function(o) { 
        return o instanceof GeoExt.MapPanel; 
    }); 
};


/** api: xtype = gx_mappanel */
Ext.reg('gx_mappanel', GeoExt.MapPanel); 
/* ======================================================================
    GeoExt/data/LayerRecord.js
   ====================================================================== */

/**
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 * 
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */

/** api: (define)
 *  module = GeoExt.data
 *  class = LayerRecord
 *  base_link = `Ext.data.Record <http://dev.sencha.com/deploy/dev/docs/?class=Ext.data.Record>`_
 */
Ext.namespace("GeoExt.data");

/** api: constructor
 *  .. class:: LayerRecord
 *  
 *      A record that represents an ``OpenLayers.Layer``. This record
 *      will always have at least the following fields:
 *
 *      * title ``String``
 */
GeoExt.data.LayerRecord = Ext.data.Record.create([
    {name: "layer"},
    {name: "title", type: "string", mapping: "name"}
]);

/** api: method[getLayer]
 *  :return: ``OpenLayers.Layer``
 *
 *  Gets the layer for this record.
 */
GeoExt.data.LayerRecord.prototype.getLayer = function() {
    return this.get("layer");
};

/** api: method[setLayer]
 *  :param layer: ``OpenLayers.Layer``
 *
 *  Sets the layer for this record.
 */
GeoExt.data.LayerRecord.prototype.setLayer = function(layer) {
    if (layer !== this.data.layer) {
        this.dirty = true;
        if(!this.modified) {
            this.modified = {};
        }
        if(this.modified.layer === undefined) {
            this.modified.layer = this.data.layer;
        }
        this.data.layer = layer;
        if(!this.editing) {
            this.afterEdit();
        }
    }
};

/** api: method[clone]
 *  :param id: ``String`` (optional) A new Record id.
 *  :return: class:`GeoExt.data.LayerRecord` A new layer record.
 *  
 *  Creates a clone of this LayerRecord. 
 */
GeoExt.data.LayerRecord.prototype.clone = function(id) { 
    var layer = this.getLayer() && this.getLayer().clone(); 
    return new this.constructor( 
        Ext.applyIf({layer: layer}, this.data), 
        id || layer.id
    );
}; 

/** api: classmethod[create]
 *  :param o: ``Array`` Field definition as in ``Ext.data.Record.create``. Can
 *      be omitted if no additional fields are required.
 *  :return: ``Function`` A specialized :class:`GeoExt.data.LayerRecord`
 *      constructor.
 *  
 *  Creates a constructor for a :class:`GeoExt.data.LayerRecord`, optionally
 *  with additional fields.
 */
GeoExt.data.LayerRecord.create = function(o) {
    var f = Ext.extend(GeoExt.data.LayerRecord, {});
    var p = f.prototype;

    p.fields = new Ext.util.MixedCollection(false, function(field) {
        return field.name;
    });

    GeoExt.data.LayerRecord.prototype.fields.each(function(f) {
        p.fields.add(f);
    });

    if(o) {
        for(var i = 0, len = o.length; i < len; i++){
            p.fields.add(new Ext.data.Field(o[i]));
        }
    }

    f.getField = function(name) {
        return p.fields.get(name);
    };

    return f;
};
/* ======================================================================
    GeoExt/data/ProtocolProxy.js
   ====================================================================== */

/**
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 * 
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */

/**
 * @require OpenLayers/BaseTypes.js
 */

/** api: (define)
 *  module = GeoExt.data
 *  class = ProtocolProxy
 *  base_link = `Ext.data.DataProxy <http://dev.sencha.com/deploy/dev/docs/?class=Ext.data.DataProxy>`_
 */
Ext.namespace('GeoExt', 'GeoExt.data');

GeoExt.data.ProtocolProxy = function(config) {
    Ext.apply(this, config);
    GeoExt.data.ProtocolProxy.superclass.constructor.apply(this, arguments);
};

/** api: constructor
 *  .. class:: ProtocolProxy
 *   
 *      A data proxy for use with ``OpenLayers.Protocol`` objects.
 */
Ext.extend(GeoExt.data.ProtocolProxy, Ext.data.DataProxy, {

    /** api: config[protocol]
     *  ``OpenLayers.Protocol``
     *  The protocol used to fetch features.
     */
    protocol: null,

    /** api: config[abortPrevious]
     *  ``Boolean``
     *  Abort any previous request before issuing another.  Default is ``true``.
     */
    abortPrevious: true,

    /** api: config[setParamsAsOptions]
     *  ``Boolean``
     *  Should options.params be set directly on options before passing it into
     *  the protocol's read method? Default is ``false``.
     */
    setParamsAsOptions: false,

    /** private: property[response]
     *  ``OpenLayers.Protocol.Response``
     *  The response returned by the read call on the protocol.
     */
    response: null,

    /** private: method[load]
     *  :param params: ``Object`` An object containing properties which are to
     *      be used as HTTP parameters for the request to the remote server.
     *  :param reader: ``Ext.data.DataReader`` The Reader object which converts
     *      the data object into a block of ``Ext.data.Records``.
     *  :param callback: ``Function`` The function into which to pass the block
     *      of ``Ext.data.Records``. The function is passed the Record block
     *      object, the ``args`` argument passed to the load function, and a
     *      boolean success indicator.
     *  :param scope: ``Object`` The scope in which to call the callback.
     *  :param arg: ``Object`` An optional argument which is passed to the
     *      callback as its second parameter.
     *
     *  Calls ``read`` on the protocol.
     */
    load: function(params, reader, callback, scope, arg) {
        if (this.fireEvent("beforeload", this, params) !== false) {
            var o = {
                params: params || {},
                request: {
                    callback: callback,
                    scope: scope,
                    arg: arg
                },
                reader: reader
            };
            var cb = OpenLayers.Function.bind(this.loadResponse, this, o);
            if (this.abortPrevious) {
                this.abortRequest();
            }
            var options = {
                params: params,
                callback: cb,
                scope: this
            };
            Ext.applyIf(options, arg);
            if (this.setParamsAsOptions === true) {
                Ext.applyIf(options, options.params);
                delete options.params;
            }
            this.response = this.protocol.read(options);
        } else {
           callback.call(scope || this, null, arg, false);
        }
    },

    /** private: method[abortRequest]
     *  Called to abort any ongoing request.
     */
    abortRequest: function() {
        if (this.response) {
            this.protocol.abort(this.response);
            this.response = null;
        }
    },

    /** private: method[loadResponse]
     *  :param o: ``Object``
     *  :param response: ``OpenLayers.Protocol.Response``
     *  
     *  Handle response from the protocol
     */
    loadResponse: function(o, response) {
        if (response.success()) {
            var result = o.reader.read(response);
            this.fireEvent("load", this, o, o.request.arg);
            o.request.callback.call(
               o.request.scope, result, o.request.arg, true);
        } else {
            this.fireEvent("loadexception", this, o, response);
            o.request.callback.call(
                o.request.scope, null, o.request.arg, false);
        }
    }
});
/* ======================================================================
    GeoExt/data/LayerStore.js
   ====================================================================== */

/**
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 * 
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */

/**
 * @include GeoExt/data/LayerReader.js
 * @include GeoExt/widgets/MapPanel.js
 */

/** api: (define)
 *  module = GeoExt.data
 *  class = LayerStore
 *  base_link = `Ext.data.Store <http://dev.sencha.com/deploy/dev/docs/?class=Ext.data.Store>`_
 */
Ext.namespace("GeoExt.data");

/** private: constructor
 *  .. class:: LayerStoreMixin
 *      A store that synchronizes a layers array of an {OpenLayers.Map} with a
 *      layer store holding {<GeoExt.data.LayerRecord>} entries.
 * 
 *      This class can not be instantiated directly. Instead, it is meant to
 *      extend ``Ext.data.Store`` or a subclass of it.
 */

/** private: example
 *  Sample code to extend a store with the LayerStoreMixin.
 *
 *  .. code-block:: javascript
 *  
 *      var store = new (Ext.extend(Ext.data.Store, new GeoExt.data.LayerStoreMixin))({
 *          map: myMap,
 *          layers: myLayers
 *      });
 * 
 *  For convenience, a :class:`GeoExt.data.LayerStore` class is available as a
 *  shortcut to the ``Ext.extend`` sequence in the above code snippet.
 */

GeoExt.data.LayerStoreMixin = function() {
    return {
        /** api: config[map]
         *  ``OpenLayers.Map``
         *  Map that this store will be in sync with. If not provided, the
         *  store will not be bound to a map.
         */
        
        /** api: property[map]
         *  ``OpenLayers.Map``
         *  Map that the store is synchronized with, if any.
         */
        map: null,
        
        /** api: config[layers]
         *  ``Array(OpenLayers.Layer)``
         *  Layers that will be added to the store (and the map, depending on the
         *  value of the ``initDir`` option.
         */
        
        /** api: config[initDir]
         *  ``Number``
         *  Bitfields specifying the direction to use for the initial sync between
         *  the map and the store, if set to 0 then no initial sync is done.
         *  Defaults to ``GeoExt.data.LayerStore.MAP_TO_STORE|GeoExt.data.LayerStore.STORE_TO_MAP``
         */

        /** api: config[fields]
         *  ``Array``
         *  If provided a custom layer record type with additional fields will be
         *  used. Default fields for every layer record are `layer`
         *  (``OpenLayers.Layer``) `title` (``String``). The value of this option is
         *  either a field definition objects as passed to the
         *  :meth:`GeoExt.data.LayerRecord.create` function or a
         *  :class:`GeoExt.data.LayerRecord` constructor created using
         *  :meth:`GeoExt.data.LayerRecord.create`.
         */

        /** api: config[reader]
         *  ``Ext.data.DataReader`` The reader used to produce
         *  :class:`GeoExt.data.LayerRecord` objects from ``OpenLayers.Layer``
         *  objects.  If not provided, a :class:`GeoExt.data.LayerReader` will be
         *  used.
         */
        reader: null,

        /** private: method[constructor]
         */
        constructor: function(config) {
            config = config || {};
            config.reader = config.reader ||
                            new GeoExt.data.LayerReader({}, config.fields);
            delete config.fields;
            // "map" option
            var map = config.map instanceof GeoExt.MapPanel ?
                      config.map.map : config.map;
            delete config.map;
            // "layers" option - is an alias to "data" option
            if(config.layers) {
                config.data = config.layers;
            }
            delete config.layers;
            // "initDir" option
            var options = {initDir: config.initDir};
            delete config.initDir;
            arguments.callee.superclass.constructor.call(this, config);
            
            this.addEvents(
                /** api:event[bind]
                 *  Fires when the store is bound to a map.
                 *
                 *  Listener arguments:
                 *  * :class:`GeoExt.data.LayerStore`
                 *  * ``OpenLayers.Map``
                 */
                "bind"
            );
            
            if(map) {
                this.bind(map, options);
            }
        },

        /** api: method[bind]
         *  :param map: ``OpenLayers.Map`` The map instance.
         *  :param options: ``Object``
         *  
         *  Bind this store to a map instance, once bound the store
         *  is synchronized with the map and vice-versa.
         */
        bind: function(map, options) {
            if(this.map) {
                // already bound
                return;
            }
            this.map = map;
            options = options || {};

            var initDir = options.initDir;
            if(options.initDir == undefined) {
                initDir = GeoExt.data.LayerStore.MAP_TO_STORE |
                          GeoExt.data.LayerStore.STORE_TO_MAP;
            }

            // create a snapshot of the map's layers
            var layers = map.layers.slice(0);

            if(initDir & GeoExt.data.LayerStore.STORE_TO_MAP) {
                this.each(function(record) {
                    this.map.addLayer(record.getLayer());
                }, this);
            }
            if(initDir & GeoExt.data.LayerStore.MAP_TO_STORE) {
                this.loadData(layers, true);
            }

            map.events.on({
                "changelayer": this.onChangeLayer,
                "addlayer": this.onAddLayer,
                "removelayer": this.onRemoveLayer,
                scope: this
            });
            this.on({
                "load": this.onLoad,
                "clear": this.onClear,
                "add": this.onAdd,
                "remove": this.onRemove,
                "update": this.onUpdate,
                scope: this
            });
            this.data.on({
                "replace" : this.onReplace,
                scope: this
            });
            this.fireEvent("bind", this, map);
        },

        /** api: method[unbind]
         *  Unbind this store from the map it is currently bound.
         */
        unbind: function() {
            if(this.map) {
                this.map.events.un({
                    "changelayer": this.onChangeLayer,
                    "addlayer": this.onAddLayer,
                    "removelayer": this.onRemoveLayer,
                    scope: this
                });
                this.un("load", this.onLoad, this);
                this.un("clear", this.onClear, this);
                this.un("add", this.onAdd, this);
                this.un("remove", this.onRemove, this);

                this.data.un("replace", this.onReplace, this);

                this.map = null;
            }
        },
        
        /** private: method[onChangeLayer]
         *  :param evt: ``Object``
         * 
         *  Handler for layer changes.  When layer order changes, this moves the
         *  appropriate record within the store.
         */
        onChangeLayer: function(evt) {
            var layer = evt.layer;
            var recordIndex = this.findBy(function(rec, id) {
                return rec.getLayer() === layer;
            });
            if(recordIndex > -1) {
                var record = this.getAt(recordIndex);
                if(evt.property === "order") {
                    if(!this._adding && !this._removing) {
                        var layerIndex = this.map.getLayerIndex(layer);
                        if(layerIndex !== recordIndex) {
                            this._removing = true;
                            this.remove(record);
                            delete this._removing;
                            this._adding = true;
                            this.insert(layerIndex, [record]);
                            delete this._adding;
                        }
                    }
                } else if(evt.property === "name") {
                    record.set("title", layer.name);
                } else {
                    this.fireEvent("update", this, record, Ext.data.Record.EDIT);
                }
            }
        },
       
        /** private: method[onAddLayer]
         *  :param evt: ``Object``
         *  
         *  Handler for a map's addlayer event
         */
        onAddLayer: function(evt) {
            if(!this._adding) {
                var layer = evt.layer;
                this._adding = true;
                this.loadData([layer], true);
                delete this._adding;
            }
        },
        
        /** private: method[onRemoveLayer]
         *  :param evt: ``Object``
         * 
         *  Handler for a map's removelayer event
         */
        onRemoveLayer: function(evt){
            //TODO replace the check for undloadDestroy with a listener for the
            // map's beforedestroy event, doing unbind(). This can be done as soon
            // as http://trac.openlayers.org/ticket/2136 is fixed.
            if(this.map.unloadDestroy) {
                if(!this._removing) {
                    var layer = evt.layer;
                    this._removing = true;
                    this.remove(this.getById(layer.id));
                    delete this._removing;
                }
            } else {
                this.unbind();
            }
        },
        
        /** private: method[onLoad]
         *  :param store: ``Ext.data.Store``
         *  :param records: ``Array(Ext.data.Record)``
         *  :param options: ``Object``
         * 
         *  Handler for a store's load event
         */
        onLoad: function(store, records, options) {
            if (!Ext.isArray(records)) {
                records = [records];
            }
            if (options && !options.add) {
                this._removing = true;
                for (var i = this.map.layers.length - 1; i >= 0; i--) {
                    this.map.removeLayer(this.map.layers[i]);
                }
                delete this._removing;

                // layers has already been added to map on "add" event
                var len = records.length;
                if (len > 0) {
                    var layers = new Array(len);
                    for (var j = 0; j < len; j++) {
                        layers[j] = records[j].getLayer();
                    }
                    this._adding = true;
                    this.map.addLayers(layers);
                    delete this._adding;
                }
            }
        },
        
        /** private: method[onClear]
         *  :param store: ``Ext.data.Store``
         * 
         *  Handler for a store's clear event
         */
        onClear: function(store) {
            this._removing = true;
            for (var i = this.map.layers.length - 1; i >= 0; i--) {
                this.map.removeLayer(this.map.layers[i]);
            }
            delete this._removing;
        },
        
        /** private: method[onAdd]
         *  :param store: ``Ext.data.Store``
         *  :param records: ``Array(Ext.data.Record)``
         *  :param index: ``Number``
         * 
         *  Handler for a store's add event
         */
        onAdd: function(store, records, index) {
            if(!this._adding) {
                this._adding = true;
                var layer;
                for(var i=records.length-1; i>=0; --i) {
                    layer = records[i].getLayer();
                    this.map.addLayer(layer);
                    if(index !== this.map.layers.length-1) {
                        this.map.setLayerIndex(layer, index);
                    }
                }
                delete this._adding;
            }
        },
        
        /** private: method[onRemove]
         *  :param store: ``Ext.data.Store``
         *  :param record: ``Ext.data.Record``
         *  :param index: ``Number``
         * 
         *  Handler for a store's remove event
         */
        onRemove: function(store, record, index){
            if(!this._removing) {
                var layer = record.getLayer();
                if (this.map.getLayer(layer.id) != null) {
                    this._removing = true;
                    this.removeMapLayer(record);
                    delete this._removing;
                }
            }
        },
        
        /** private: method[onUpdate]
         *  :param store: ``Ext.data.Store``
         *  :param record: ``Ext.data.Record``
         *  :param operation: ``Number``
         * 
         *  Handler for a store's update event
         */
        onUpdate: function(store, record, operation) {
            if(operation === Ext.data.Record.EDIT) {
                if (record.modified && record.modified.title) {
                    var layer = record.getLayer();
                    var title = record.get("title");
                    if(title !== layer.name) {
                        layer.setName(title);
                    }
                }
            }
        },

        /** private: method[removeMapLayer]
         *  :param record: ``Ext.data.Record``
         *  
         *  Removes a record's layer from the bound map.
         */
        removeMapLayer: function(record){
            this.map.removeLayer(record.getLayer());
        },

        /** private: method[onReplace]
         *  :param key: ``String``
         *  :param oldRecord: ``Object`` In this case, a record that has been
         *      replaced.
         *  :param newRecord: ``Object`` In this case, a record that is replacing
         *      oldRecord.

         *  Handler for a store's data collections' replace event
         */
        onReplace: function(key, oldRecord, newRecord){
            this.removeMapLayer(oldRecord);
        },
        
        /** api: method[getByLayer]
         *  :param layer: ``OpenLayers.Layer``
         *  :return: :class:`GeoExt.data.LayerRecord` or undefined if not found
         *  
         *  Get the record for the specified layer
         */
        getByLayer: function(layer) {
            var index = this.findBy(function(r) {
                return r.getLayer() === layer;
            });
            if(index > -1) {
                return this.getAt(index);
            }
        },
        
        /** private: method[destroy]
         */
        destroy: function() {
            this.unbind();
            GeoExt.data.LayerStore.superclass.destroy.call(this);
        }
    };
};

/** api: example
 *  Sample to create a new store containing a cache of
 *  :class:`GeoExt.data.LayerRecord` instances derived from map layers.
 *
 *  .. code-block:: javascript
 *  
 *      var store = new GeoExt.data.LayerStore({
 *          map: myMap,
 *          layers: myLayers
 *      });
 */

/** api: constructor
 *  .. class:: LayerStore
 *
 *      A store that contains a cache of :class:`GeoExt.data.LayerRecord`
 *      objects.
 */
GeoExt.data.LayerStore = Ext.extend(
    Ext.data.Store,
    new GeoExt.data.LayerStoreMixin
);

/**
 * Constant: GeoExt.data.LayerStore.MAP_TO_STORE
 * {Integer} Constant used to make the store be automatically updated
 * when changes occur in the map.
 */
GeoExt.data.LayerStore.MAP_TO_STORE = 1;

/**
 * Constant: GeoExt.data.LayerStore.STORE_TO_MAP
 * {Integer} Constant used to make the map be automatically updated
 * when changes occur in the store.
 */
GeoExt.data.LayerStore.STORE_TO_MAP = 2;
/* ======================================================================
    GeoExt/widgets/Popup.js
   ====================================================================== */

/**
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 *
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */

/**
 * @include GeoExt/widgets/MapPanel.js
 * @require OpenLayers/Feature/Vector.js
 * @require OpenLayers/Geometry.js
 * @require OpenLayers/BaseTypes/Pixel.js
 */

/** api: (define)
 *  module = GeoExt
 *  class = Popup
 *  base_link = `Ext.Window <http://dev.sencha.com/deploy/dev/docs/?class=Ext.Window>`_
 */
Ext.namespace("GeoExt");

/** api: example
 *  Sample code to create a popup anchored to a feature:
 *
 *  .. code-block:: javascript
 *
 *      var popup = new GeoExt.Popup({
 *          title: "My Popup",
 *          location: feature,
 *          width: 200,
 *          html: "<div>Popup content</div>",
 *          collapsible: true
 *      });
 */

/** api: constructor
 *  .. class:: Popup(config)
 *
 *      Popups are a specialized Window that supports anchoring
 *      to a particular location in a MapPanel.  When a popup
 *      is anchored to a location, that means that the popup
 *      will visibly point to the location on the map, and move
 *      accordingly when the map is panned or zoomed.
 */
GeoExt.Popup = Ext.extend(Ext.Window, {

    /** api: config[anchored]
     *  ``Boolean``  The popup begins anchored to its location.  Default is
     *  ``true``.
     */
    anchored: true,

    /** api: config[map]
     *  ``OpenLayers.Map`` or :class:`GeoExt.MapPanel`
     *  The map this popup will be anchored to (only required if ``anchored``
     *  is set to true and the map cannot be derived from the ``location``'s
     *  layer.
     */
    map: null,

    /** api: config[panIn]
     *  ``Boolean`` The popup should pan the map so that the popup is
     *  fully in view when it is rendered.  Default is ``true``.
     */
    panIn: true,

    /** api: config[unpinnable]
     *  ``Boolean`` The popup should have a "unpin" tool that unanchors it from
     *  its location.  Default is ``true``.
     */
    unpinnable: true,

    /** api: config[location]
     *  ``OpenLayers.Feature.Vector`` or ``OpenLayers.LonLat`` or
     *  ``OpenLayers.Pixel`` or ``OpenLayers.Geometry`` A location for this
     *  popup's anchor.
     */

    /** private: property[location]
     *  ``OpenLayers.LonLat``
     */
    location: null,

    /** private: property[insideViewport]
     *  ``Boolean`` Wether the popup is currently inside the map viewport.
     */
    insideViewport: null,

    /**
     * Some Ext.Window defaults need to be overriden here
     * because some Ext.Window behavior is not currently supported.
     */

    /** private: config[animCollapse]
     *  ``Boolean`` Animate the transition when the panel is collapsed.
     *  Default is ``false``.  Collapsing animation is not supported yet for
     *  popups.
     */
    animCollapse: false,

    /** private: config[draggable]
     *  ``Boolean`` Enable dragging of this Panel.  Defaults to ``false``
     *  because the popup defaults to being anchored, and anchored popups
     *  should not be draggable.
     */
    draggable: false,

    /** private: config[shadow]
     *  ``Boolean`` Give the popup window a shadow.  Defaults to ``false``
     *  because shadows are not supported yet for popups (the shadow does
     *  not look good with the anchor).
     */
    shadow: false,

    /** api: config[popupCls]
     *  ``String`` CSS class name for the popup DOM elements.  Default is
     *  "gx-popup".
     */
    popupCls: "gx-popup",

    /** api: config[ancCls]
     *  ``String``  CSS class name for the popup's anchor.
     */
    ancCls: null,

    /** api: config[anchorPosition]
     *  ``String``  Controls the anchor position for the popup. If set to
     *  ``auto``, the anchor will be positioned on the top or the bottom of
     *  the window, minimizing map movement. Supported values are ``bottom-left``,
     *  ``bottom-right``, ``top-left``, ``top-right`` or ``auto``.
     *  Defaults to ``auto``.
     */
    anchorPosition: "auto",

    /** private: method[initComponent]
     *  Initializes the popup.
     */
    initComponent: function() {
        if(this.map instanceof GeoExt.MapPanel) {
            this.map = this.map.map;
        }
        if(!this.map && this.location instanceof OpenLayers.Feature.Vector &&
                                                        this.location.layer) {
            this.map = this.location.layer.map;
        }
        if (this.location instanceof OpenLayers.Feature.Vector) {
            this.location = this.location.geometry;
        }
        if (this.location instanceof OpenLayers.Geometry) {
            if (typeof this.location.getCentroid == "function") {
                this.location = this.location.getCentroid();
            }
            this.location = this.location.getBounds().getCenterLonLat();
        } else if (this.location instanceof OpenLayers.Pixel) {
            this.location = this.map.getLonLatFromViewPortPx(this.location);
        }
        if (!(this.location instanceof OpenLayers.LonLat)) {
            this.anchored = false;
        }

        var mapExtent =  this.map.getExtent();
        if (mapExtent && this.location) {
            this.insideViewport = mapExtent.containsLonLat(this.location);
        }

        if(this.anchored) {
            this.addAnchorEvents();
            this.elements += ',anc';
        } else {
            this.unpinnable = false;
        }

        this.baseCls = this.popupCls + " " + this.baseCls;

        GeoExt.Popup.superclass.initComponent.call(this);
    },

    /** private: method[onRender]
     *  Executes when the popup is rendered.
     */
    onRender: function(ct, position) {
        GeoExt.Popup.superclass.onRender.call(this, ct, position);
        if (this.anchored) {
            this.ancCls = this.popupCls + "-anc";
            //create anchor dom element.
            this.createElement("anc", this.el.dom);
        } else {
            this.makeDraggable();
        }
    },

    /** private: method[initTools]
     *  Initializes the tools on the popup.  In particular,
     *  it adds the 'unpin' tool if the popup is unpinnable.
     */
    initTools : function() {
        if(this.unpinnable) {
            this.addTool({
                id: 'unpin',
                handler: this.unanchorPopup.createDelegate(this, [])
            });
        }

        GeoExt.Popup.superclass.initTools.call(this);
    },

    /** private: method[show]
     *  Override.
     */
    show: function() {
        GeoExt.Popup.superclass.show.apply(this, arguments);
        if(this.anchored) {
            this.position();
            if(this.panIn && !this._mapMove) {
                this.panIntoView();
            }
        }
    },

    /** private: method[maximize]
     *  Override.
     */
    maximize: function() {
        if(!this.maximized && this.anc) {
            this.unanchorPopup();
        }
        GeoExt.Popup.superclass.maximize.apply(this, arguments);
    },

    /** api: method[setSize]
     *  :param w: ``Integer``
     *  :param h: ``Integer``
     *
     *  Sets the size of the popup, taking into account the size of the anchor.
     */
    setSize: function(w, h) {
        if(this.anc) {
            var ancSize = this.anc.getSize();
            if(typeof w == 'object') {
                h = w.height - ancSize.height;
                w = w.width;
            } else if(!isNaN(h)){
                h = h - ancSize.height;
            }
        }
        GeoExt.Popup.superclass.setSize.call(this, w, h);
    },

    /** private: method[position]
     *  Positions the popup relative to its location
     */
    position: function() {
        var me = this;
        if(me._mapMove === true) {
            me.insideViewport = me.map.getExtent().containsLonLat(me.location);
            if(me.insideViewport !== me.isVisible()) {
                me.setVisible(me.insideViewport);
            }
        }

        if(me.isVisible()) {
            var locationPx = me.map.getPixelFromLonLat(me.location),
                mapBox = Ext.fly(me.map.div).getBox(true),
                y = locationPx.y + mapBox.y,
                x = locationPx.x + mapBox.x,
                elSize = me.el.getSize(),
                ancSize = me.anc.getSize(),
                ancPos = me.anchorPosition;
            if (ancPos.indexOf("right") > -1 || locationPx.x > mapBox.width / 2) {
                // right
                me.anc.addClass("right");
                var ancRight = me.el.getX(true) + elSize.width -
                               me.anc.getX(true) - ancSize.width;
                x -= elSize.width - ancRight - ancSize.width / 2;
            } else {
                // left
                me.anc.removeClass("right");
                var ancX = me.anc.getLeft(true);
                x -= ancX + ancSize.width / 2;
            }

            if (ancPos.indexOf("bottom") > -1 || locationPx.y > mapBox.height / 2) {
                // bottom
                me.anc.removeClass("top");
                y -= elSize.height + ancSize.height;
            } else {
                // top
                me.anc.addClass("top");
                y += ancSize.height; // ok
            }

            // Needed to have the right position on the first display
            // (no flash on the center of the map).
            me.setPagePosition(x, y);
            // position in the next cycle - otherwise strange shifts can occur.
            window.setTimeout(function() {
                if (me.dom) {
                    me.setPagePosition(x, y);
                }
            }, 0);
        }
    },

    /** private: method[makeDraggable]
     *  Make the window draggable
     */
    makeDraggable: function() {
        this.draggable = true;
        this.header.addClass("x-window-draggable");
        this.dd = new Ext.Window.DD(this);
    },

    /** private: method[unanchorPopup]
     *  Unanchors a popup from its location.  This removes the popup from its
     *  MapPanel and adds it to the page body.
     */
    unanchorPopup: function() {
        this.removeAnchorEvents();

        this.makeDraggable();

        //remove anchor
        this.anc.remove();
        this.anc = null;

        //hide unpin tool
        this.tools.unpin.hide();
    },

    /** private: method[panIntoView]
     *  Pans the MapPanel's map so that an anchored popup can come entirely
     *  into view, with padding specified as per normal OpenLayers.Map popup
     *  padding.
     */
    panIntoView: function() {
        var mapBox = Ext.fly(this.map.div).getBox(true);

        //assumed viewport takes up whole body element of map panel
        var popupPos =  this.getPosition(true);
        popupPos[0] -= mapBox.x;
        popupPos[1] -= mapBox.y;

        var panelSize = [mapBox.width, mapBox.height]; // [X,Y]

        var popupSize = this.getSize();

        var newPos = [popupPos[0], popupPos[1]];

        //For now, using native OpenLayers popup padding.  This may not be ideal.
        var padding = this.map.paddingForPopups;

        // X
        if(popupPos[0] < padding.left) {
            newPos[0] = padding.left;
        } else if(popupPos[0] + popupSize.width > panelSize[0] - padding.right) {
            newPos[0] = panelSize[0] - padding.right - popupSize.width;
        }

        // Y
        if(popupPos[1] < padding.top) {
            newPos[1] = padding.top;
        } else if(popupPos[1] + popupSize.height > panelSize[1] - padding.bottom) {
            newPos[1] = panelSize[1] - padding.bottom - popupSize.height;
        }

        var dx = popupPos[0] - newPos[0];
        var dy = popupPos[1] - newPos[1];

        this.map.pan(dx, dy);
    },

    /** private: method[onMapMove]
     */
    onMapMove: function() {
        if (!(this.hidden && this.insideViewport)){
            this._mapMove = true;
            this.position();
            delete this._mapMove;
        }
    },

    /** private: method[addAnchorEvents]
     */
    addAnchorEvents: function() {
        this.map.events.on({
            "move" : this.onMapMove,
            scope : this
        });

        this.on({
            "resize": this.position,
            "collapse": this.position,
            "expand": this.position,
            scope: this
        });
    },

    /** private: method[removeAnchorEvents]
     */
    removeAnchorEvents: function() {
        //stop position with location
        this.map.events.un({
            "move" : this.onMapMove,
            scope : this
        });

        this.un("resize", this.position, this);
        this.un("collapse", this.position, this);
        this.un("expand", this.position, this);

    },

    /** private: method[beforeDestroy]
     *  Cleanup events before destroying the popup.
     */
    beforeDestroy: function() {
        if(this.anchored) {
            this.removeAnchorEvents();
        }
        GeoExt.Popup.superclass.beforeDestroy.call(this);
    }
});

/** api: xtype = gx_popup */
Ext.reg('gx_popup', GeoExt.Popup);
/* ======================================================================
    GeoExt/widgets/grid/FeatureSelectionModel.js
   ====================================================================== */

/**
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 * 
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */

/**
 * @require OpenLayers/Control/SelectFeature.js
 * @require OpenLayers/Layer/Vector.js
 * @require OpenLayers/BaseTypes/Class.js
 */

/** api: (define)
 *  module = GeoExt.grid
 *  class = FeatureSelectionModel
 *  base_link = `Ext.grid.RowSelectionModel <http://dev.sencha.com/deploy/dev/docs/?class=Ext.grid.RowSelectionModel>`_
 */

Ext.namespace('GeoExt.grid');

/** api: constructor
 *  .. class:: FeatureSelectionModel
 *
 *      A row selection model which enables automatic selection of features
 *      in the map when rows are selected in the grid and vice-versa.
 */

/** api: example
 *  Sample code to create a feature grid with a feature selection model:
 *  
 *  .. code-block:: javascript
 *
 *       var gridPanel = new Ext.grid.GridPanel({
 *          title: "Feature Grid",
 *          region: "east",
 *          store: store,
 *          width: 320,
 *          columns: [{
 *              header: "Name",
 *              width: 200,
 *              dataIndex: "name"
 *          }, {
 *              header: "Elevation",
 *              width: 100,
 *              dataIndex: "elevation"
 *          }],
 *          sm: new GeoExt.grid.FeatureSelectionModel() 
 *      });
 */

GeoExt.grid.FeatureSelectionModelMixin = function() {
    return {
        /** api: config[autoActivateControl]
         *  ``Boolean`` If true the select feature control is activated and
         *  deactivated when binding and unbinding. Defaults to true.
         */
        autoActivateControl: true,

        /** api: config[layerFromStore]
         *  ``Boolean`` If true, and if the constructor is passed neither a
         *  layer nor a select feature control, a select feature control is
         *  created using the layer found in the grid's store. Set it to
         *  false if you want to manually bind the selection model to a
         *  layer. Defaults to true.
         */
        layerFromStore: true,

        /** api: config[selectControl]
         *
         *  ``OpenLayers.Control.SelectFeature`` A select feature control. If not
         *  provided one will be created.  If provided any "layer" config option
         *  will be ignored, and its "multiple" option will be used to configure
         *  the selectionModel. If an ``Object`` is provided here, it will be
         *  passed as config to the SelectFeature constructor, and the "layer"
         *  config option will be used for the layer.

         */
         
        /** api: config[controlConfig]
         *
         * Used as optional config for createSelectControl. Added by JW.
         */
         
         
        /** private: property[selectControl] 
         *  ``OpenLayers.Control.SelectFeature`` The select feature control 
         *  instance. 
         */ 
        selectControl: null, 
        
        /** api: config[layer]
         *  ``OpenLayers.Layer.Vector`` The vector layer used for the creation of
         *  the select feature control, it must already be added to the map. If not
         *  provided, the layer bound to the grid's store, if any, will be used.
         */

        /** private: property[bound]
         *  ``Boolean`` Flag indicating if the selection model is bound.
         */
        bound: false,
        
        /** private: property[superclass]
         *  ``Ext.grid.AbstractSelectionModel`` Our superclass.
         */
        superclass: null,
        
        /** private: property[selectedFeatures]
         *  ``Array`` An array to store the selected features.
         */
        selectedFeatures: [],
        
        /** api: config[autoPanMapOnSelection]
         *  ``Boolean`` If true the map will recenter on feature selection
         *  so that the selected features are visible. Defaults to false.
         */
        autoPanMapOnSelection: false,

        /** private */
        constructor: function(config) {
            config = config || {};
            if(config.selectControl instanceof OpenLayers.Control.SelectFeature) { 
                if(!config.singleSelect) {
                    var ctrl = config.selectControl;
                    config.singleSelect = !(ctrl.multiple || !!ctrl.multipleKey);
                }
            } else if(config.layer instanceof OpenLayers.Layer.Vector) {
                this.selectControl = this.createSelectControl(
                    config.layer, config.selectControl
                );
                delete config.layer;
                delete config.selectControl;
            }
            if (config.autoPanMapOnSelection) {
                this.autoPanMapOnSelection = true;
                delete config.autoPanMapOnSelection;
            }
            this.superclass = arguments.callee.superclass;
            this.superclass.constructor.call(this, config);
        },
        
        /** private: method[initEvents]
         *
         *  Called after this.grid is defined
         */
        initEvents: function() {
            this.superclass.initEvents.call(this);
            if(this.layerFromStore) {
                var layer = this.grid.getStore() && this.grid.getStore().layer;
                if(layer &&
                   !(this.selectControl instanceof OpenLayers.Control.SelectFeature)) {
                    this.selectControl = this.createSelectControl(
                        layer, this.selectControl
                    );
                }
            }
            if(this.selectControl) {
                this.bind(this.selectControl);
            }
        },

        /** private: createSelectControl
         *  :param layer: ``OpenLayers.Layer.Vector`` The vector layer.
         *  :param config: ``Object`` The select feature control config.
         *
         *  Create the select feature control.
         */
        createSelectControl: function(layer, config) {
            if (!config) {
                if (this.config && this.config.controlConfig) {
                    config = this.config.controlConfig;
                } else {
                    config = {};
                }
            }
            var singleSelect = config.singleSelect !== undefined ?
                               config.singleSelect : this.singleSelect;
            config = OpenLayers.Util.extend({
                toggle: true,
                multipleKey: singleSelect ? null :
                    (Ext.isMac ? "metaKey" : "ctrlKey")
            }, config);
            var selectControl = new OpenLayers.Control.SelectFeature(
                layer, config
            );
            layer.map.addControl(selectControl);
            return selectControl;
        },
        
        /** api: method[bind]
         *
         *  :param obj: ``OpenLayers.Layer.Vector`` or
         *      ``OpenLayers.Control.SelectFeature`` The object this selection model
         *      should be bound to, either a vector layer or a select feature
         *      control.
         *  :param options: ``Object`` An object with a "controlConfig"
         *      property referencing the configuration object to pass to the
         *      ``OpenLayers.Control.SelectFeature`` constructor.
         *  :return: ``OpenLayers.Control.SelectFeature`` The select feature
         *      control this selection model uses.
         *
         *  Bind the selection model to a layer or a SelectFeature control.
         */
        bind: function(obj, options) {
            if(!this.bound) {
                options = options || {};
                this.selectControl = obj;
                if(obj instanceof OpenLayers.Layer.Vector) {
                    this.selectControl = this.createSelectControl(
                        obj, options.controlConfig
                    );
                }
                if(this.autoActivateControl) {
                    this.selectControl.activate();
                }
                var layers = this.getLayers();
                for(var i = 0, len = layers.length; i < len; i++) {
                    layers[i].events.on({
                        featureselected: this.featureSelected,
                        featureunselected: this.featureUnselected,
                        scope: this
                    });
                }
                this.on("rowselect", this.rowSelected, this);
                this.on("rowdeselect", this.rowDeselected, this);
                this.bound = true;
            }
            return this.selectControl;
        },
        
        /** api: method[unbind]
         *  :return: ``OpenLayers.Control.SelectFeature`` The select feature
         *      control this selection model used.
         *
         *  Unbind the selection model from the layer or SelectFeature control.
         */
        unbind: function() {
            var selectControl = this.selectControl;
            if(this.bound) {
                var layers = this.getLayers();
                for(var i = 0, len = layers.length; i < len; i++) {
                    layers[i].events.un({
                        featureselected: this.featureSelected,
                        featureunselected: this.featureUnselected,
                        scope: this
                    });
                }
                this.un("rowselect", this.rowSelected, this);
                this.un("rowdeselect", this.rowDeselected, this);
                if(this.autoActivateControl) {
                    selectControl.deactivate();
                }
                this.selectControl = null;
                this.bound = false;
            }
            return selectControl;
        },
        
        /** private: method[featureSelected]
         *  :param evt: ``Object`` An object with a feature property referencing
         *                         the selected feature.
         */
        featureSelected: function(evt) {
            if(!this._selecting) {
                var store = this.grid.store;
                var row = store.findBy(function(record, id) {
                    return record.getFeature() == evt.feature;
                });
                if(row != -1 && !this.isSelected(row)) {
                    this._selecting = true;
                    this.selectRow(row, !this.singleSelect);
                    this._selecting = false;
                    // focus the row in the grid to ensure it is visible
                    this.grid.getView().focusRow(row);
                }
            }
        },
        
        /** private: method[featureUnselected]
         *  :param evt: ``Object`` An object with a feature property referencing
         *                         the unselected feature.
         */
        featureUnselected: function(evt) {
            if(!this._selecting) {
                var store = this.grid.store;
                var row = store.findBy(function(record, id) {
                    return record.getFeature() == evt.feature;
                });
                if(row != -1 && this.isSelected(row)) {
                    this._selecting = true;
                    this.deselectRow(row); 
                    this._selecting = false;
                    this.grid.getView().focusRow(row);
                }
            }
        },
        
        /** private: method[rowSelected]
         *  :param model: ``Ext.grid.RowSelectModel`` The row select model.
         *  :param row: ``Integer`` The row index.
         *  :param record: ``Ext.data.Record`` The record.
         */
        rowSelected: function(model, row, record) {
            var feature = record.getFeature();
            if(!this._selecting && feature) {
                var layers = this.getLayers();
                for(var i = 0, len = layers.length; i < len; i++) {
                    if(layers[i].selectedFeatures.indexOf(feature) == -1) {
                        this._selecting = true;
                        this.selectControl.select(feature);
                        this._selecting = false;
                        this.selectedFeatures.push(feature);
                        break;
                    }
                }
                if(this.autoPanMapOnSelection) {
                    this.recenterToSelectionExtent();
                }
             }
        },
        
        /** private: method[rowDeselected]
         *  :param model: ``Ext.grid.RowSelectModel`` The row select model.
         *  :param row: ``Integer`` The row index.
         *  :param record: ``Ext.data.Record`` The record.
         */
        rowDeselected: function(model, row, record) {
            var feature = record.getFeature();
            if(!this._selecting && feature) {
                var layers = this.getLayers();
                for(var i = 0, len = layers.length; i < len; i++) {
                    if(layers[i].selectedFeatures.indexOf(feature) != -1) {
                        this._selecting = true;
                        this.selectControl.unselect(feature);
                        this._selecting = false;
                        OpenLayers.Util.removeItem(this.selectedFeatures, feature);
                        break;
                    }
                }
                if(this.autoPanMapOnSelection && this.selectedFeatures.length > 0) {
                    this.recenterToSelectionExtent();
                }
            }
        },

        /** private: method[getLayers]
         *  Return the layers attached to the select feature control.
         */
        getLayers: function() {
            return this.selectControl.layers || [this.selectControl.layer];
        },
        
        /**
         * private: method[recenterToSelectionExtent]
         * centers the map in order to display all
         * selected features
         */
        recenterToSelectionExtent: function() {
            var map = this.selectControl.map;
            var selectionExtent = this.getSelectionExtent();
            var selectionExtentZoom = map.getZoomForExtent(selectionExtent, false);
            if(selectionExtentZoom > map.getZoom()) {
                map.setCenter(selectionExtent.getCenterLonLat());
            }
            else {
                map.zoomToExtent(selectionExtent);
            }
        },
        
        /** api: method[getSelectionExtent]
         *  :return: ``OpenLayers.Bounds`` or null if the layer has no features with
         *      geometries
         *
         *  Calculates the max extent which includes all selected features.
         */
        getSelectionExtent: function () {
            var maxExtent = null;
            var features = this.selectedFeatures;
            if(features && (features.length > 0)) {
                var geometry = null;
                for(var i=0, len=features.length; i<len; i++) {
                    geometry = features[i].geometry;
                    if (geometry) {
                        if (maxExtent === null) {
                            maxExtent = new OpenLayers.Bounds();
                        }
                        maxExtent.extend(geometry.getBounds());
                    }
                }
            }
            return maxExtent;
        }
    };
};

GeoExt.grid.FeatureSelectionModel = Ext.extend(
    Ext.grid.RowSelectionModel,
    new GeoExt.grid.FeatureSelectionModelMixin
);
/* ======================================================================
    GeoExt/widgets/Action.js
   ====================================================================== */

/**
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 * 
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */

/**
 * @require OpenLayers/Control.js
 */

/** api: (define)
 *  module = GeoExt
 *  class = Action
 *  base_link = `Ext.Action <http://dev.sencha.com/deploy/dev/docs/?class=Ext.Action>`_
 */
Ext.namespace("GeoExt");

/** api: example
 *  Sample code to create a toolbar with an OpenLayers control into it.
 * 
 *  .. code-block:: javascript
 *  
 *      var action = new GeoExt.Action({
 *          text: "max extent",
 *          control: new OpenLayers.Control.ZoomToMaxExtent(),
 *          map: map
 *      });
 *      var toolbar = new Ext.Toolbar([action]);
 */

/** api: constructor
 *  .. class:: Action(config)
 *  
 *      Create a GeoExt.Action instance. A GeoExt.Action is created
 *      to insert an OpenLayers control in a toolbar as a button or
 *      in a menu as a menu item. A GeoExt.Action instance can be
 *      used like a regular Ext.Action, look at the Ext.Action API
 *      doc for more detail.
 */
GeoExt.Action = Ext.extend(Ext.Action, {

    /** api: config[control]
     *  ``OpenLayers.Control`` The OpenLayers control wrapped in this action.
     */
    control: null,

    /** api: config[activateOnEnable]
     *  ``Boolean`` Activate the action's control when the action is enabled.
     *  Default is ``false``.
     */

    /** api: property[activateOnEnable]
     *  ``Boolean`` Activate the action's control when the action is enabled.
     */
    activateOnEnable: false,

    /** api: config[deactivateOnDisable]
     *  ``Boolean`` Deactivate the action's control when the action is disabled.
     *  Default is ``false``.
     */

    /** api: property[deactivateOnDisable]
     *  ``Boolean`` Deactivate the action's control when the action is disabled.
     */
    deactivateOnDisable: false,

    /** api: config[map]
     *  ``OpenLayers.Map`` The OpenLayers map that the control should be added
     *  to.  For controls that don't need to be added to a map or have already
     *  been added to one, this config property may be omitted.
     */
    map: null,

    /** private: property[uScope]
     *  ``Object`` The user-provided scope, used when calling uHandler,
     *  uToggleHandler, and uCheckHandler.
     */
    uScope: null,

    /** private: property[uHandler]
     *  ``Function`` References the function the user passes through
     *  the "handler" property.
     */
    uHandler: null,

    /** private: property[uToggleHandler]
     *  ``Function`` References the function the user passes through
     *  the "toggleHandler" property.
     */
    uToggleHandler: null,

    /** private: property[uCheckHandler]
     *  ``Function`` References the function the user passes through
     *  the "checkHandler" property.
     */
    uCheckHandler: null,

    /** private */
    constructor: function(config) {
        
        // store the user scope and handlers
        this.uScope = config.scope;
        this.uHandler = config.handler;
        this.uToggleHandler = config.toggleHandler;
        this.uCheckHandler = config.checkHandler;

        config.scope = this;
        config.handler = this.pHandler;
        config.toggleHandler = this.pToggleHandler;
        config.checkHandler = this.pCheckHandler;

        // set control in the instance, the Ext.Action
        // constructor won't do it for us
        var ctrl = this.control = config.control;
        delete config.control;
        
        this.activateOnEnable = !!config.activateOnEnable;
        delete config.activateOnEnable;
        this.deactivateOnDisable = !!config.deactivateOnDisable;
        delete config.deactivateOnDisable;

        // register "activate" and "deactivate" listeners
        // on the control
        if(ctrl) {
            // If map is provided in config, add control to map.
            if(config.map) {
                config.map.addControl(ctrl);
                delete config.map;
            }
            if((config.pressed || config.checked) && ctrl.map) {
                ctrl.activate();
            }
            if (ctrl.active) {
                config.pressed = true;
                config.checked = true;
            }
            ctrl.events.on({
                activate: this.onCtrlActivate,
                deactivate: this.onCtrlDeactivate,
                scope: this
            });
        }

        arguments.callee.superclass.constructor.call(this, config);
    },

    /** private: method[pHandler]
     *  :param cmp: ``Ext.Component`` The component that triggers the handler.
     *
     *  The private handler.
     */
    pHandler: function(cmp) {
        var ctrl = this.control;
        if(ctrl &&
           ctrl.type == OpenLayers.Control.TYPE_BUTTON) {
            ctrl.trigger();
        }
        if(this.uHandler) {
            this.uHandler.apply(this.uScope, arguments);
        }
    },

    /** private: method[pTogleHandler]
     *  :param cmp: ``Ext.Component`` The component that triggers the toggle handler.
     *  :param state: ``Boolean`` The state of the toggle.
     *
     *  The private toggle handler.
     */
    pToggleHandler: function(cmp, state) {
        this.changeControlState(state);
        if(this.uToggleHandler) {
            this.uToggleHandler.apply(this.uScope, arguments);
        }
    },

    /** private: method[pCheckHandler]
     *  :param cmp: ``Ext.Component`` The component that triggers the check handler.
     *  :param state: ``Boolean`` The state of the toggle.
     *
     *  The private check handler.
     */
    pCheckHandler: function(cmp, state) {
        this.changeControlState(state);
        if(this.uCheckHandler) {
            this.uCheckHandler.apply(this.uScope, arguments);
        }
    },

    /** private: method[changeControlState]
     *  :param state: ``Boolean`` The state of the toggle.
     *
     *  Change the control state depending on the state boolean.
     */
    changeControlState: function(state) {
        if(state) {
            if(!this._activating) {
                this._activating = true;
                this.control.activate();
                // update initialConfig for next component created from this action
                this.initialConfig.pressed = true;
                this.initialConfig.checked = true;
                this._activating = false;
            }
        } else {
            if(!this._deactivating) {
                this._deactivating = true;
                this.control.deactivate();
                // update initialConfig for next component created from this action
                this.initialConfig.pressed = false;
                this.initialConfig.checked = false;
                this._deactivating = false;
            }
        }
    },

    /** private: method[onCtrlActivate]
     *  
     *  Called when this action's control is activated.
     */
    onCtrlActivate: function() {
        var ctrl = this.control;
        if(ctrl.type == OpenLayers.Control.TYPE_BUTTON) {
            this.enable();
        } else {
            // deal with buttons
            this.safeCallEach("toggle", [true]);
            // deal with check items
            this.safeCallEach("setChecked", [true]);
        }
    },

    /** private: method[onCtrlDeactivate]
     *  
     *  Called when this action's control is deactivated.
     */
    onCtrlDeactivate: function() {
        var ctrl = this.control;
        if(ctrl.type == OpenLayers.Control.TYPE_BUTTON) {
            this.disable();
        } else {
            // deal with buttons
            this.safeCallEach("toggle", [false]);
            // deal with check items
            this.safeCallEach("setChecked", [false]);
        }
    },

    /** private: method[safeCallEach]
     *
     */
    safeCallEach: function(fnName, args) {
        var cs = this.items;
        for(var i = 0, len = cs.length; i < len; i++){
            if(cs[i][fnName]) {
                cs[i].rendered ?
                    cs[i][fnName].apply(cs[i], args) :
                    cs[i].on({
                        "render": cs[i][fnName].createDelegate(cs[i], args),
                        single: true
                    });
            }
        }
    },
    
    /** private: method[setDisabled]
     *  :param v: ``Boolean`` Disable the action's components.
     *
     *  Override method on super to optionally deactivate controls on disable.
     */
    setDisabled : function(v) {
        if (!v && this.activateOnEnable && this.control && !this.control.active) {
            this.control.activate();
        }
        if (v && this.deactivateOnDisable && this.control && this.control.active) {
            this.control.deactivate();
        }
        return GeoExt.Action.superclass.setDisabled.apply(this, arguments);
    }

});
/* ======================================================================
    GeoExt/data/LayerReader.js
   ====================================================================== */

/**
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 * 
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */

/**
 * @include GeoExt/data/LayerRecord.js
 */

/** api: (define)
 *  module = GeoExt.data
 *  class = LayerReader
 *  base_link = `Ext.data.DataReader <http://dev.sencha.com/deploy/dev/docs/?class=Ext.data.DataReader>`_
 */
Ext.namespace("GeoExt", "GeoExt.data");

/** api: example
 *  Sample using a reader to create records from an array of layers:
 * 
 *  .. code-block:: javascript
 *     
 *      var reader = new GeoExt.data.LayerReader();
 *      var layerData = reader.readRecords(map.layers);
 *      var numRecords = layerData.totalRecords;
 *      var layerRecords = layerData.records;
 */

/** api: constructor
 *  .. class:: LayerReader(meta, recordType)
 *  
 *      Data reader class to create an array of
 *      :class:`GeoExt.data.LayerRecord` objects from an array of 
 *      ``OpenLayers.Layer`` objects for use in a
 *      :class:`GeoExt.data.LayerStore` object.
 */
GeoExt.data.LayerReader = function(meta, recordType) {
    meta = meta || {};
    if(!(recordType instanceof Function)) {
        recordType = GeoExt.data.LayerRecord.create(
            recordType || meta.fields || {});
    }
    GeoExt.data.LayerReader.superclass.constructor.call(
        this, meta, recordType);
};

Ext.extend(GeoExt.data.LayerReader, Ext.data.DataReader, {

    /** private: property[totalRecords]
     *  ``Integer``
     */
    totalRecords: null,

    /** api: method[readRecords]
     *  :param layers: ``Array(OpenLayers.Layer)`` List of layers for creating
     *      records.
     *  :return: ``Object``  An object with ``records`` and ``totalRecords``
     *      properties.
     *  
     *  From an array of ``OpenLayers.Layer`` objects create a data block
     *  containing :class:`GeoExt.data.LayerRecord` objects.
     */
    readRecords : function(layers) {
        var records = [];
        if(layers) {
            var recordType = this.recordType, fields = recordType.prototype.fields;
            var i, lenI, j, lenJ, layer, values, field, v;
            for(i = 0, lenI = layers.length; i < lenI; i++) {
                layer = layers[i];
                values = {};
                for(j = 0, lenJ = fields.length; j < lenJ; j++){
                    field = fields.items[j];
                    v = layer[field.mapping || field.name] ||
                        field.defaultValue;
                    v = field.convert(v);
                    values[field.name] = v;
                }
                values.layer = layer;
                records[records.length] = new recordType(values, layer.id);
            }
        }
        return {
            records: records,
            totalRecords: this.totalRecords != null ? this.totalRecords : records.length
        };
    }
});
/* ======================================================================
    GeoExt/widgets/grid/SymbolizerColumn.js
   ====================================================================== */

/**
 * Copyright (c) 2008-2012 The Open Source Geospatial Foundation
 * 
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */

/**
 * @include GeoExt/widgets/FeatureRenderer.js
 */

 /** api: (define)
  *  module = GeoExt.grid
  *  class = SymbolizerColumn
  *  base_link = `Ext.grid.Column <http://dev.sencha.com/deploy/dev/docs/?class=Ext.grid.Column>`_
  */

Ext.namespace('GeoExt.grid');

/** api: constructor
 *  .. class:: SymbolizerColumn(config)
 *
 *      Grid column for rendering a symbolizer or an array of symbolizers.
 */
GeoExt.grid.SymbolizerColumn = Ext.extend(Ext.grid.Column, {

    /** private: method[renderer]
     */ 
    renderer: function(value, meta) {
        if (value != null) {
            var id = Ext.id();
            window.setTimeout(function() {
                var ct = Ext.get(id);
                // ct for old field may not exist any more during a grid update
                if (ct) {
                    new GeoExt.FeatureRenderer({
                        symbolizers: value instanceof Array ? value : [value],
                        renderTo: ct
                    });
                }
            }, 0);
            meta.css = "gx-grid-symbolizercol";
            return '<div id="' + id + '"></div>';
        }
    }
});

/** api: xtype = gx_symbolizercolumn */
Ext.grid.Column.types.gx_symbolizercolumn = GeoExt.grid.SymbolizerColumn;
/* ======================================================================
    extensions/ClusterSelectionModel.js
   ====================================================================== */

/**
 * Copyright (c) 2008-2010 The Open Source Geospatial Foundation
 *
 * Published under the BSD license.
 * See http://svn.geoext.org/core/trunk/geoext/license.txt for the full text
 * of the license.
 */

/** api: (define)
 *  module = GeoExt.grid
 *  class = ClusterSelectionModel
 *  base_link = `Ext.grid.RowSelectionModel <http://dev.sencha.com/deploy/dev/docs/?class=Ext.grid.RowSelectionModel>`_
 */

Ext.namespace('GeoExt.grid');

/** api: constructor
 *  .. class:: ClusterSelectionModel
 *
 *      A row selection model which enables automatic selection of features
 *      in the map when rows are selected in the grid and vice-versa.
 */

/** api: example
 *  Sample code to create a feature grid with a feature selection model:
 *
*  .. code-block:: javascript
*
*       var gridPanel = new Ext.grid.GridPanel({
*          title: "Feature Grid",
*          region: "east",
*          store: store,
*          width: 320,
*          columns: [{
*              header: "Name",
*              width: 200,
*              dataIndex: "name"
*          }, {
*              header: "Elevation",
*              width: 100,
*              dataIndex: "elevation"
*          }],
*          sm: new GeoExt.grid.FeatureSelectionModel()
*      });
*/

GeoExt.grid.ClusterSelectionModelMixin = function () {
    return {

        /** api: config[layerFromStore]
         *  ``Boolean`` If true, and if the constructor is passed neither a
         *  layer nor a select feature control, a select feature control is
         *  created using the layer found in the grid's store. Set it to
         *  false if you want to manually bind the selection model to a
         *  layer. Defaults to true.
         */
        layerFromStore: true,

        /** api: config[layer]
         *  ``OpenLayers.Layer.Vector`` The vector layer used for the creation of
         *  the select feature control, it must already be added to the map. If not
         *  provided, the layer bound to the grid's store, if any, will be used.
         */

        /** private: property[bound]
         *  ``Boolean`` Flag indicating if the selection model is bound.
         */
        bound: false,

        /** private: property[superclass]
         *  ``Ext.grid.AbstractSelectionModel`` Our superclass.
         */
        superclass: null,

        /** private */
        constructor: function (config) {
            config = config || {};
            this.superclass = arguments.callee.superclass;
            this.superclass.constructor.call(this, config);
        },

        /** private: method[initEvents]
         *
         *  Called after this.grid is defined
         */
        initEvents: function () {
            this.superclass.initEvents.call(this);
            if (this.layerFromStore) {
                var layer = this.grid.getStore() && this.grid.getStore().layer;
            }
            this.bind(layer);
        },

        /** api: method[bind]
         *
         *  :param layer: ``OpenLayers.Layer.Vector`` The vector layer this selection
         *      model should be bound to.
         *  Bind the selection model to a layer.
         */
        bind: function (layer) {
            if (!this.bound && layer) {
                this.layer = layer;
                this.on("rowselect", this.rowSelected, this);
                this.on("rowdeselect", this.rowDeselected, this);
                this.bound = true;
            }
        },

        /** api: method[unbind]
         *  Unbind the selection model from the layer.
         */
        unbind: function () {
            var layer = this.layer;
            if (this.bound) {
                this.un("rowselect", this.rowSelected, this);
                this.un("rowdeselect", this.rowDeselected, this);
                this.bound = false;
            }
        },

        /** private: method[rowSelected]
         *  :param model: ``Ext.grid.RowSelectModel`` The row select model.
         *  :param row: ``Integer`` The row index.
         *  :param record: ``Ext.data.Record`` The record.
         */
        rowSelected: function (model, row, record) {
            var feature = record.getFeature();
            var layer = this.layer;
            if (!this._selecting && feature) {
                if (layer.selectedFeatures.indexOf(feature) == -1) {
                    this._selecting = true;
                    //find feature within a cluster
                    var cluster = this.findClusterByFeature(layer, feature);
                    if (cluster) {
                        if (layer.events.triggerEvent("beforefeatureselected", {feature: cluster}) !== false) {
                            layer.events.triggerEvent("featureselected", {
                                feature: cluster,
                                targetFeature: feature
                            });
                        }
                    }
                    this._selecting = false;
                }
            }
        },

        /** private: method[rowDeselected]
         *  :param model: ``Ext.grid.RowSelectModel`` The row select model.
         *  :param row: ``Integer`` The row index.
         *  :param record: ``Ext.data.Record`` The record.
         */
        rowDeselected: function (model, row, record) {
            var feature = record.getFeature();
            if (!this._selecting && feature) {
                var layer = this.layer;
                //find feature within a cluster
                var cluster = this.findClusterByFeature(layer, feature);
                if (cluster) {
                    this._selecting = true;
                    layer.events.triggerEvent("featureunselected", {
                        feature: cluster,
                        targetFeature: feature
                    });
                    this._selecting = false;
                }
            }
        },

        /** private: method[findClusterByFeature]
         *  :param layer: ``OpenLayers.Layer.Vector``
         *  :param feature: ``OpenLayers.Feature.Vector``
         *  :return: ``OpenLayers.Feature.Vector`` The clustered feature containing the specified feature
         *  Return the clustered feature containing the specified feature
         */
        findClusterByFeature: function (layer, feature) {
            for (var i = 0, len = layer.features.length; i < len; i++) {
                var cfeat = layer.features[i];
                if (cfeat == feature) {
                    return cfeat;
                }
                else if (cfeat.cluster) {
                    if (cfeat.cluster.length == 1 && cfeat.cluster[0] == feature) {
                        return cfeat;
                    }
                    else {
                        for (var j = 0, clen = cfeat.cluster.length; j < clen; j++) {
                            if (cfeat.cluster[j] == feature) {
                                return cfeat;
                            }
                        }
                    }
                }
            }
        }
    };
};

GeoExt.grid.ClusterSelectionModel = Ext.extend(
    Ext.grid.RowSelectionModel,
    new GeoExt.grid.ClusterSelectionModelMixin
);
/* ======================================================================
    extensions/OverviewMapPanel.js
   ====================================================================== */


GeoExt.OverviewMapPanel = Ext.extend(Ext.Panel, {
    map: null,
    floating: true,
    hideMode: 'offsets', 
    ovMapAlign: 'br-br',
    inMapPanel: true,
    //layout: 'fit',
    ctrlOptions: null,
    mapOptions: null,
    layers: null,
    control: null,
    
    initComponent: function () {
        var config = Ext.apply({}, this.initialConfig);
        // create the control
        this.control = this.createControl(config);
        // var ctrlElem = new Ext.Element(this.control.div).wrap();
        var node = Ext.DomQuery.selectNode('#ovwrap .olMap') ||
            Ext.DomQuery.selectNode('#ovwrap .olControlOverviewMapElement');
        this.controlElem = new Ext.Element(node).wrap();
        this.items = [new Ext.BoxComponent(this.controlElem)];
        if (this.inMapPanel && config.hidden !== false) {
            config.hidden = true;
        }
        GeoExt.OverviewMapPanel.superclass.initComponent.apply(this, arguments);
        this.on({
            'show': function (cmp) {
                if (this.inMapPanel) {
                    this.getEl().anchorTo(this.map.viewPortDiv, this.ovMapAlign);
                }
            },
            scope: this
        });
    },
    
    afterRender: function () {
        GeoExt.OverviewMapPanel.superclass.afterRender.apply(this, arguments);
        if (this.autoShow) {
            this.show();
        }
        // force resize of overview map
        /*
        var w = this.getInnerWidth()-this.getFrameWidth();
        var headerHt = (this.header) ? this.header.getHeight() : 0;
        var h=this.getInnerHeight()-this.getFrameHeight()- headerHt;
        this.controlElem.setSize(w,h);
        */
        if (this.floating & this.inMapPanel) {
            this.getEl().anchorTo(this.map.viewPortDiv, this.ovMapAlign);
        }
        Ext.removeNode(Ext.getDom('ovwrap')); //nnn
    },
    
    beforeDestroy: function () {
        if (this.control) {
            this.control.destroy();
            this.control = null;
        }
        GeoExt.OverviewMapPanel.superclass.beforeDestroy.call(this);
    },
    
    createControl: function (opts) {
        // short circuit if someone supplies the control
        if (this.control) {
            return this.control;
        }
        // regular path
        opts.ctrlOptions = opts.ctrlOptions || {};
        // move mapOptions to the ctrlOptions if needed
        if (opts.mapOptions) {
            opts.ctrlOptions.mapOptions = opts.mapOptions;
            delete opts.mapOptions;
        }
        // move layers to the ctrlOptions if needed
        if (opts.layers) {
            opts.ctrlOptions.layers = opts.layers;
            delete opts.layers;
        }
        // infer the map if it is not given
        if (!opts.map) {
            opts.map = GeoExt.MapPanel.guess().map;
        }
        // assign the actual OL Map if a MapPanel was passed
        if (opts.map instanceof GeoExt.MapPanel) {
            opts.map = opts.map.map;
        }
        // use the size of this container unless a size was specifically given
        // specifically set a div if one hasn't been set to make 'outsideViewport' = true
        Ext.applyIf(opts.ctrlOptions,
            {size: new OpenLayers.Size(opts.width, opts.height),
            div: Ext.DomHelper.append(Ext.getBody().dom, {tag: 'div', id: 'ovwrap'})
        });
        var control = new OpenLayers.Control.OverviewMap(opts.ctrlOptions);
        opts.map.addControl(control);
        return control;
    },
    
    xtype: 'gx_overviewmap'
});

Ext.reg('gx_overviewmap', GeoExt.OverviewMapPanel);

/* ======================================================================
    JsonXml.js
   ====================================================================== */

﻿/*
    The below work is licensed under Creative Commons GNU LGPL License.

    Original work:

    License:     http://creativecommons.org/licenses/LGPL/2.1/
    Author:      Stefan Goessner/2006
    Web:         http://goessner.net/ 

    Modifications made:

    Version:     0.9-p5
    Description: Restructured code, JSLint validated (no strict whitespaces),
                 added handling of empty arrays, empty strings, and int/floats values.
    Author:      Michael Schøler/2008-01-29
    Web:         http://michael.hinnerup.net/blog/2008/01/26/converting-json-to-xml-and-xml-to-json/
*/

/*global alert */
var xmlJsonClass = {
    // Param "xml": Element or document DOM node.
    // Param "tab": Tab or indent string for pretty output formatting omit or use empty string "" to supress.
    // Returns:     JSON string
    xml2json: function(xml, tab) {
        if (xml.nodeType === 9) {
            // document node
            xml = xml.documentElement;
        }
        var nws = this.removeWhite(xml);
        var obj = this.toObj(nws);
        var json = this.toJson(obj, xml.nodeName, "\t");
        return "{\n" + tab + (tab ? json.replace(/\t/g, tab) : json.replace(/\t|\n/g, "")) + "\n}";
    },

    // Param "o":   JavaScript object
    // Param "tab": tab or indent string for pretty output formatting omit or use empty string "" to supress.
    // Returns:     XML string
    json2xml: function(o, tab) {
        var toXml = function(v, name, ind) {
            var xml = "";
            var i, n;
            if (v instanceof Array) {
                if (v.length === 0) {
                    xml += ind + "<"+name+">__EMPTY_ARRAY_</"+name+">\n";
                }
                else {
                    for (i = 0, n = v.length; i < n; i += 1) {
                        var sXml = ind + toXml(v[i], name, ind+"\t") + "\n";
                        xml += sXml;
                    }
                }
            }
            else if (typeof(v) === "object") {
                var hasChild = false;
                xml += ind + "<" + name;
                var m;
                for (m in v) if (v.hasOwnProperty(m)) {
                    if (m.charAt(0) === "@") {
                        xml += " " + m.substr(1) + "=\"" + v[m].toString() + "\"";
                    }
                    else {
                        hasChild = true;
                    }
                }
                xml += hasChild ? ">\n" : "/>\n";
                if (hasChild) {
                    for (m in v) if (v.hasOwnProperty(m)) {
                        if (m === "#text") {
                            xml += v[m];
                        }
                        else if (m === "#cdata") {
                            xml += "<![CDATA[" + v[m] + "]]>";
                        }
                        else if (m.charAt(0) !== "@") {
                            xml += toXml(v[m], m, ind+"\t");
                        }
                    }
                    xml += (xml.charAt(xml.length - 1) === "\n" ? ind : "") + "</" + name + ">\n";
                }
            }
            else {
                if (v.toString() === "\"\"" || v.toString().length === 0) {
                    xml += ind + "<" + name + ">__EMPTY_STRING_</" + name + ">\n";
                } 
                else {
                    xml += ind + "<" + name + ">" + v.toString() + "</" + name + ">\n";
                }
            }
            return xml;
        };
        var xml = "";
        var m;
        for (m in o) if (o.hasOwnProperty(m)) {
            xml += toXml(o[m], m, "");
        }
        return tab ? xml.replace(/\t/g, tab) : xml.replace(/\t|\n/g, "");
    },

    // Internal methods
    toObj: function(xml) {
        var o = {};
        if (xml.nodeType === 1) {
            // element node ..
            if (xml.attributes.length) {
                // element with attributes ..
                var i;
                for (i = 0; i < xml.attributes.length; i += 1) {
                    o["@" + xml.attributes[i].nodeName] = (xml.attributes[i].nodeValue || "").toString();
                }
            }
            if (xml.firstChild) {
                // element has child nodes ..
                var textChild = 0, cdataChild = 0, hasElementChild = false;
                var n;
                for (n = xml.firstChild; n; n = n.nextSibling) {
                    if (n.nodeType === 1) {
                        hasElementChild = true;
                    }
                    else if (n.nodeType === 3 && n.nodeValue.match(/[^ \f\n\r\t\v]/)) {
                        // non-whitespace text
                        textChild += 1;
                    }
                    else if (n.nodeType === 4) {
                        // cdata section node
                        cdataChild += 1;
                    }
                }
                if (hasElementChild) {
                    if (textChild < 2 && cdataChild < 2) {
                        // structured element with evtl. a single text or/and cdata node ..
                        this.removeWhite(xml);
                        for (n = xml.firstChild; n; n = n.nextSibling) {
                            if (n.nodeType === 3) {
                                // text node
                                o["#text"] = this.escape(n.nodeValue);
                            }
                            else if (n.nodeType === 4) {
                                // cdata node
                             o["#cdata"] = this.escape(n.nodeValue);
                            }
                            else if (o[n.nodeName]) {
                                // multiple occurence of element ..
                                if (o[n.nodeName] instanceof Array) {
                                    o[n.nodeName][o[n.nodeName].length] = this.toObj(n);
                                }
                                else {
                                    o[n.nodeName] = [o[n.nodeName], this.toObj(n)];
                                }
                            }
                            else {
                                // first occurence of element ..
                                o[n.nodeName] = this.toObj(n);
                            }
                        }
                    }
                    else {
                        // mixed content
                        if (!xml.attributes.length) {
                            o = this.escape(this.innerXml(xml));
                        }
                        else {
                            o["#text"] = this.escape(this.innerXml(xml));
                        }
                    }
                }
                else if (textChild) {
                    // pure text
                    if (!xml.attributes.length) {
                        o = this.escape(this.innerXml(xml));
                        if (o === "__EMPTY_ARRAY_") {
                            o = "[]";
                        } else if (o === "__EMPTY_STRING_") {
                            o = "";
                        }
                    }
                    else {
                        o["#text"] = this.escape(this.innerXml(xml));
                    }
                }
                else if (cdataChild) {
                    // cdata
                    if (cdataChild > 1) {
                        o = this.escape(this.innerXml(xml));
                    }
                    else {
                        for (n = xml.firstChild; n; n = n.nextSibling) {
                            o["#cdata"] = this.escape(n.nodeValue);
                        }
                    }
                }
            }
            if (!xml.attributes.length && !xml.firstChild) {
                o = null;
            }
        }
        else if (xml.nodeType === 9) {
            // document.node
            o = this.toObj(xml.documentElement);
        }
        else {
            alert("unhandled node type: " + xml.nodeType);
        }
        return o;
    },
    toJson: function(o, name, ind) {
        var json = name ? ("\"" + name + "\"") : "";
        if (o === "[]") {
            json += (name ? ":[]" : "[]");
        } 
        else if (o instanceof Array) {
            var n, i;
            for (i = 0, n = o.length; i < n; i += 1) {
                o[i] = this.toJson(o[i], "", ind + "\t");
            }
            json += (name ? ":[" : "[") + (o.length > 1 ? ("\n" + ind + "\t" + o.join(",\n" + ind + "\t") + "\n" + ind) : o.join("")) + "]";
        }
        else if (o === null) {
            json += (name && ":") + "null";
        }
        else if (typeof(o) === "object") {
            var arr = [];
            var m;
            for (m in o) if (o.hasOwnProperty(m)) {
                arr[arr.length] = this.toJson(o[m], m, ind + "\t");
            }
            json += (name ? ":{" : "{") + (arr.length > 1 ? ("\n" + ind + "\t" + arr.join(",\n" + ind + "\t") + "\n" + ind) : arr.join("")) + "}";
        }
        else if (typeof(o) === "string") {
            o = o.toString();
            var objRegExp  = /(^-?\d+\.?\d*$)/;
            if (objRegExp.test(o)) {
                /* Added by Jacob Westfall, Feb 2011
                 * check for leading zero and make this a string instead,
                 * errors were being raised by many browsers about this
                 */
                if (o.substring(0,1) == '0') {
                    json += (name && ":") + "\"" + o + "\"";
                } else {
                    // int or float
                    json += (name && ":") + o;
                }
            }
            else {
                json += (name && ":") + "\"" + o + "\"";
            }
        }
        else {
            json += (name && ":") + o.toString();
        }
        return json;
    },
    innerXml: function(node) {
        var s = "";
        if ("innerHTML" in node) {
            s = node.innerHTML;
        }
        else {
            var asXml = function(n) {
                var s = "", i;
                if (n.nodeType === 1) {
                    s += "<" + n.nodeName;
                    for (i = 0; i < n.attributes.length; i += 1) {
                        s += " " + n.attributes[i].nodeName + "=\"" + (n.attributes[i].nodeValue || "").toString() + "\"";
                    }
                    if (n.firstChild) {
                        s += ">";
                        for (var c = n.firstChild; c; c = c.nextSibling) {
                            s += asXml(c);
                        }
                        s += "</" + n.nodeName + ">";
                    }
                    else {
                        s += "/>";
                    }
                }
                else if (n.nodeType === 3) {
                    s += n.nodeValue;
                }
                else if (n.nodeType === 4) {
                    s += "<![CDATA[" + n.nodeValue + "]]>";
                }
                return s;
            };
            for (var c = node.firstChild; c; c = c.nextSibling) {
                s += asXml(c);
            }
        }
        return s;
    },
    escape: function(txt) {
        return txt.replace(/[\\]/g, "\\\\").replace(/[\"]/g, '\\"').replace(/[\n]/g, '\\n').replace(/[\r]/g, '\\r');
    },
    removeWhite: function(e) {
        e.normalize();
        var n;
        for (n = e.firstChild; n; ) {
            if (n.nodeType === 3) {
                // text node
                if (!n.nodeValue.match(/[^ \f\n\r\t\v]/)) {
                    // pure whitespace text node
                    var nxt = n.nextSibling;
                    e.removeChild(n);
                    n = nxt;
                }
                else {
                    n = n.nextSibling;
                }
            }
            else if (n.nodeType === 1) {
                // element node
                this.removeWhite(n);
                n = n.nextSibling;
            }
            else {
                // any other node
                n = n.nextSibling;
            }
        }
        return e;
    }
};
/* ======================================================================
    json2.js
   ====================================================================== */

/*
    http://www.JSON.org/json2.js
    2010-11-17

    Public Domain.

    NO WARRANTY EXPRESSED OR IMPLIED. USE AT YOUR OWN RISK.

    See http://www.JSON.org/js.html


    This code should be minified before deployment.
    See http://javascript.crockford.com/jsmin.html

    USE YOUR OWN COPY. IT IS EXTREMELY UNWISE TO LOAD CODE FROM SERVERS YOU DO
    NOT CONTROL.


    This file creates a global JSON object containing two methods: stringify
    and parse.

        JSON.stringify(value, replacer, space)
            value       any JavaScript value, usually an object or array.

            replacer    an optional parameter that determines how object
                        values are stringified for objects. It can be a
                        function or an array of strings.

            space       an optional parameter that specifies the indentation
                        of nested structures. If it is omitted, the text will
                        be packed without extra whitespace. If it is a number,
                        it will specify the number of spaces to indent at each
                        level. If it is a string (such as '\t' or '&nbsp;'),
                        it contains the characters used to indent at each level.

            This method produces a JSON text from a JavaScript value.

            When an object value is found, if the object contains a toJSON
            method, its toJSON method will be called and the result will be
            stringified. A toJSON method does not serialize: it returns the
            value represented by the name/value pair that should be serialized,
            or undefined if nothing should be serialized. The toJSON method
            will be passed the key associated with the value, and this will be
            bound to the value

            For example, this would serialize Dates as ISO strings.

                Date.prototype.toJSON = function (key) {
                    function f(n) {
                        // Format integers to have at least two digits.
                        return n < 10 ? '0' + n : n;
                    }

                    return this.getUTCFullYear()   + '-' +
                         f(this.getUTCMonth() + 1) + '-' +
                         f(this.getUTCDate())      + 'T' +
                         f(this.getUTCHours())     + ':' +
                         f(this.getUTCMinutes())   + ':' +
                         f(this.getUTCSeconds())   + 'Z';
                };

            You can provide an optional replacer method. It will be passed the
            key and value of each member, with this bound to the containing
            object. The value that is returned from your method will be
            serialized. If your method returns undefined, then the member will
            be excluded from the serialization.

            If the replacer parameter is an array of strings, then it will be
            used to select the members to be serialized. It filters the results
            such that only members with keys listed in the replacer array are
            stringified.

            Values that do not have JSON representations, such as undefined or
            functions, will not be serialized. Such values in objects will be
            dropped; in arrays they will be replaced with null. You can use
            a replacer function to replace those with JSON values.
            JSON.stringify(undefined) returns undefined.

            The optional space parameter produces a stringification of the
            value that is filled with line breaks and indentation to make it
            easier to read.

            If the space parameter is a non-empty string, then that string will
            be used for indentation. If the space parameter is a number, then
            the indentation will be that many spaces.

            Example:

            text = JSON.stringify(['e', {pluribus: 'unum'}]);
            // text is '["e",{"pluribus":"unum"}]'


            text = JSON.stringify(['e', {pluribus: 'unum'}], null, '\t');
            // text is '[\n\t"e",\n\t{\n\t\t"pluribus": "unum"\n\t}\n]'

            text = JSON.stringify([new Date()], function (key, value) {
                return this[key] instanceof Date ?
                    'Date(' + this[key] + ')' : value;
            });
            // text is '["Date(---current time---)"]'


        JSON.parse(text, reviver)
            This method parses a JSON text to produce an object or array.
            It can throw a SyntaxError exception.

            The optional reviver parameter is a function that can filter and
            transform the results. It receives each of the keys and values,
            and its return value is used instead of the original value.
            If it returns what it received, then the structure is not modified.
            If it returns undefined then the member is deleted.

            Example:

            // Parse the text. Values that look like ISO date strings will
            // be converted to Date objects.

            myData = JSON.parse(text, function (key, value) {
                var a;
                if (typeof value === 'string') {
                    a =
/^(\d{4})-(\d{2})-(\d{2})T(\d{2}):(\d{2}):(\d{2}(?:\.\d*)?)Z$/.exec(value);
                    if (a) {
                        return new Date(Date.UTC(+a[1], +a[2] - 1, +a[3], +a[4],
                            +a[5], +a[6]));
                    }
                }
                return value;
            });

            myData = JSON.parse('["Date(09/09/2001)"]', function (key, value) {
                var d;
                if (typeof value === 'string' &&
                        value.slice(0, 5) === 'Date(' &&
                        value.slice(-1) === ')') {
                    d = new Date(value.slice(5, -1));
                    if (d) {
                        return d;
                    }
                }
                return value;
            });


    This is a reference implementation. You are free to copy, modify, or
    redistribute.
*/

/*jslint evil: true, strict: false, regexp: false */

/*members "", "\b", "\t", "\n", "\f", "\r", "\"", JSON, "\\", apply,
    call, charCodeAt, getUTCDate, getUTCFullYear, getUTCHours,
    getUTCMinutes, getUTCMonth, getUTCSeconds, hasOwnProperty, join,
    lastIndex, length, parse, prototype, push, replace, slice, stringify,
    test, toJSON, toString, valueOf
*/


// Create a JSON object only if one does not already exist. We create the
// methods in a closure to avoid creating global variables.

if (!this.JSON) {
    this.JSON = {};
}

(function () {
    "use strict";

    function f(n) {
        // Format integers to have at least two digits.
        return n < 10 ? '0' + n : n;
    }

    if (typeof Date.prototype.toJSON !== 'function') {

        Date.prototype.toJSON = function (key) {

            return isFinite(this.valueOf()) ?
                   this.getUTCFullYear()   + '-' +
                 f(this.getUTCMonth() + 1) + '-' +
                 f(this.getUTCDate())      + 'T' +
                 f(this.getUTCHours())     + ':' +
                 f(this.getUTCMinutes())   + ':' +
                 f(this.getUTCSeconds())   + 'Z' : null;
        };

        String.prototype.toJSON =
        Number.prototype.toJSON =
        Boolean.prototype.toJSON = function (key) {
            return this.valueOf();
        };
    }

    var cx = /[\u0000\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        escapable = /[\\\"\x00-\x1f\x7f-\x9f\u00ad\u0600-\u0604\u070f\u17b4\u17b5\u200c-\u200f\u2028-\u202f\u2060-\u206f\ufeff\ufff0-\uffff]/g,
        gap,
        indent,
        meta = {    // table of character substitutions
            '\b': '\\b',
            '\t': '\\t',
            '\n': '\\n',
            '\f': '\\f',
            '\r': '\\r',
            '"' : '\\"',
            '\\': '\\\\'
        },
        rep;


    function quote(string) {

// If the string contains no control characters, no quote characters, and no
// backslash characters, then we can safely slap some quotes around it.
// Otherwise we must also replace the offending characters with safe escape
// sequences.

        escapable.lastIndex = 0;
        return escapable.test(string) ?
            '"' + string.replace(escapable, function (a) {
                var c = meta[a];
                return typeof c === 'string' ? c :
                    '\\u' + ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
            }) + '"' :
            '"' + string + '"';
    }


    function str(key, holder) {

// Produce a string from holder[key].

        var i,          // The loop counter.
            k,          // The member key.
            v,          // The member value.
            length,
            mind = gap,
            partial,
            value = holder[key];

// If the value has a toJSON method, call it to obtain a replacement value.

        if (value && typeof value === 'object' &&
                typeof value.toJSON === 'function') {
            value = value.toJSON(key);
        }

// If we were called with a replacer function, then call the replacer to
// obtain a replacement value.

        if (typeof rep === 'function') {
            value = rep.call(holder, key, value);
        }

// What happens next depends on the value's type.

        switch (typeof value) {
        case 'string':
            return quote(value);

        case 'number':

// JSON numbers must be finite. Encode non-finite numbers as null.

            return isFinite(value) ? String(value) : 'null';

        case 'boolean':
        case 'null':

// If the value is a boolean or null, convert it to a string. Note:
// typeof null does not produce 'null'. The case is included here in
// the remote chance that this gets fixed someday.

            return String(value);

// If the type is 'object', we might be dealing with an object or an array or
// null.

        case 'object':

// Due to a specification blunder in ECMAScript, typeof null is 'object',
// so watch out for that case.

            if (!value) {
                return 'null';
            }

// Make an array to hold the partial results of stringifying this object value.

            gap += indent;
            partial = [];

// Is the value an array?

            if (Object.prototype.toString.apply(value) === '[object Array]') {

// The value is an array. Stringify every element. Use null as a placeholder
// for non-JSON values.

                length = value.length;
                for (i = 0; i < length; i += 1) {
                    partial[i] = str(i, value) || 'null';
                }

// Join all of the elements together, separated with commas, and wrap them in
// brackets.

                v = partial.length === 0 ? '[]' :
                    gap ? '[\n' + gap +
                            partial.join(',\n' + gap) + '\n' +
                                mind + ']' :
                          '[' + partial.join(',') + ']';
                gap = mind;
                return v;
            }

// If the replacer is an array, use it to select the members to be stringified.

            if (rep && typeof rep === 'object') {
                length = rep.length;
                for (i = 0; i < length; i += 1) {
                    k = rep[i];
                    if (typeof k === 'string') {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            } else {

// Otherwise, iterate through all of the keys in the object.

                for (k in value) {
                    if (Object.hasOwnProperty.call(value, k)) {
                        v = str(k, value);
                        if (v) {
                            partial.push(quote(k) + (gap ? ': ' : ':') + v);
                        }
                    }
                }
            }

// Join all of the member texts together, separated with commas,
// and wrap them in braces.

            v = partial.length === 0 ? '{}' :
                gap ? '{\n' + gap + partial.join(',\n' + gap) + '\n' +
                        mind + '}' : '{' + partial.join(',') + '}';
            gap = mind;
            return v;
        }
    }

// If the JSON object does not yet have a stringify method, give it one.

    if (typeof JSON.stringify !== 'function') {
        JSON.stringify = function (value, replacer, space) {

// The stringify method takes a value and an optional replacer, and an optional
// space parameter, and returns a JSON text. The replacer can be a function
// that can replace values, or an array of strings that will select the keys.
// A default replacer method can be provided. Use of the space parameter can
// produce text that is more easily readable.

            var i;
            gap = '';
            indent = '';

// If the space parameter is a number, make an indent string containing that
// many spaces.

            if (typeof space === 'number') {
                for (i = 0; i < space; i += 1) {
                    indent += ' ';
                }

// If the space parameter is a string, it will be used as the indent string.

            } else if (typeof space === 'string') {
                indent = space;
            }

// If there is a replacer, it must be a function or an array.
// Otherwise, throw an error.

            rep = replacer;
            if (replacer && typeof replacer !== 'function' &&
                    (typeof replacer !== 'object' ||
                     typeof replacer.length !== 'number')) {
                throw new Error('JSON.stringify');
            }

// Make a fake root object containing our value under the key of ''.
// Return the result of stringifying the value.

            return str('', {'': value});
        };
    }


// If the JSON object does not yet have a parse method, give it one.

    if (typeof JSON.parse !== 'function') {
        JSON.parse = function (text, reviver) {

// The parse method takes a text and an optional reviver function, and returns
// a JavaScript value if the text is a valid JSON text.

            var j;

            function walk(holder, key) {

// The walk method is used to recursively walk the resulting structure so
// that modifications can be made.

                var k, v, value = holder[key];
                if (value && typeof value === 'object') {
                    for (k in value) {
                        if (Object.hasOwnProperty.call(value, k)) {
                            v = walk(value, k);
                            if (v !== undefined) {
                                value[k] = v;
                            } else {
                                delete value[k];
                            }
                        }
                    }
                }
                return reviver.call(holder, key, value);
            }


// Parsing happens in four stages. In the first stage, we replace certain
// Unicode characters with escape sequences. JavaScript handles many characters
// incorrectly, either silently deleting them, or treating them as line endings.

            text = String(text);
            cx.lastIndex = 0;
            if (cx.test(text)) {
                text = text.replace(cx, function (a) {
                    return '\\u' +
                        ('0000' + a.charCodeAt(0).toString(16)).slice(-4);
                });
            }

// In the second stage, we run the text against regular expressions that look
// for non-JSON patterns. We are especially concerned with '()' and 'new'
// because they can cause invocation, and '=' because it can cause mutation.
// But just to be safe, we want to reject all unexpected forms.

// We split the second stage into 4 regexp operations in order to work around
// crippling inefficiencies in IE's and Safari's regexp engines. First we
// replace the JSON backslash pairs with '@' (a non-JSON character). Second, we
// replace all simple value tokens with ']' characters. Third, we delete all
// open brackets that follow a colon or comma or that begin the text. Finally,
// we look to see that the remaining characters are only whitespace or ']' or
// ',' or ':' or '{' or '}'. If that is so, then the text is safe for eval.

            if (/^[\],:{}\s]*$/
.test(text.replace(/\\(?:["\\\/bfnrt]|u[0-9a-fA-F]{4})/g, '@')
.replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']')
.replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

// In the third stage we use the eval function to compile the text into a
// JavaScript structure. The '{' operator is subject to a syntactic ambiguity
// in JavaScript: it can begin a block or an object literal. We wrap the text
// in parens to eliminate the ambiguity.

                j = eval('(' + text + ')');

// In the optional fourth stage, we recursively walk the new structure, passing
// each name/value pair to a reviver function for possible transformation.

                return typeof reviver === 'function' ?
                    walk({'': j}, '') : j;
            }

// If the text is not JSON parseable, then a SyntaxError is thrown.

            throw new SyntaxError('JSON.parse');
        };
    }
}());

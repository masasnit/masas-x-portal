//Grunt is just JavaScript running in node, after all...
module.exports = function(grunt) {

  // All upfront config goes in a massive nested object.
  grunt.initConfig({
    // You can set arbitrary key-value pairs.
    pathToViewing: "/var/www/view",
    pathToPosting: "/var/www/masas-posting",
    pathToPortal: "/var/www/masas-portal",
    staticFolder : "static",
    sourceFolder : "src",
    buildFolder : "build",
    // You can also set the value of a key as parsed JSON.
    // Allows us to reference properties we declared in package.json.
    pkg: grunt.file.readJSON('package.json'),
    // Grunt tasks are associated with specific properties.
    // these names generally match their npm package name.

      'shell': {
        'copyLibs': {
            'options': {
                'stdout': true
            },
            'command': ['cp -r <%= pathToPosting %>/deploy/libs <%= staticFolder %>/libs', 'cp -r <%= pathToViewing %>/deploy/libs <%= staticFolder %>/libs'].join('&&')
        },
        'copyPostingSource' : {
        	'options': {
                'stdout': true
            },
            'command' : ['cp -r <%= pathToPosting %>/js <%= sourceFolder %>/post', 'cp -r <%= pathToPosting %>/css <%= sourceFolder %>/post', 'cp -r <%= pathToPosting %>/img <%= sourceFolder %>/post'].join('&&')
        },
        'copyViewingSource' : {
        	'options': {
                'stdout': true
            },
            'command' : ['cp -r <%= pathToViewing %>/js <%= sourceFolder %>/post', 'cp -r <%= pathToViewing %>/css <%= sourceFolder %>/post', 'cp -r <%= pathToViewing %>/img <%= sourceFolder %>/post'].join('&&')
        },
        'copyBuildFiles' : {
        	'options': {
                'stdout': true
            },
            'command' : ['cd <%= pathToPosting %>/build', 'cp -r extjs-post.cfg geoext.cfg openlayers.cfg xmljson.cfg <%= pathToPortal %>/<%= buildFolder %>', 'cd <%= pathToViewing %>/build','cp -r extjs-view.cfg <%= pathToPortal %>/<%= buildFolder %>'].join('&&')
        },
        'runPortalBuild' : {
        	'options': {
                'stdout': true
            },
            'command' : ['cd <%= pathToPortal %>/build', 'python build.py'].join('&&')
        },
        'copyImageFiles' : {
        	'options': {
                'stdout': true
            },
            'command' : ['cp -r <%= pathToPortal %>/src/portal/img <%= pathToPortal %>/static/img', 'cp -r <%= pathToPortal %>/src/post/img <%= pathToPortal %>/static/img', 'cp -r <%= pathToPortal %>/src/view/img <%= pathToPortal %>/static/img'].join('&&')
        },
    }
  }); // The end of grunt.initConfig

  // We've set up each task's configuration.
  // Now actually load the tasks.
  // This will do a lookup similar to node's require() function.
  grunt.loadNpmTasks('grunt-shell');
  // Register our own custom task alias.
  grunt.registerTask('prod-build', ['shell:copyLibs','shell:copyPostingSource','shell:copyViewingSource', 'shell:copyBuildFiles', 'shell:runPortalBuild', 'shell:copyImageFiles']);
  // grunt.registerTask('dev-build', ['template:development-html-template']);
};